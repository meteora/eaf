﻿using UnityEngine;
using System.Collections;

public class TestModule : MonoBehaviour {
#if UNITY_EDITOR

    public int StartMapId;
    public int EndMapId;
    public int ClearGrade;
    public bool ClearedMap;

    public int ItemId;
    public int ItemNum;
    public bool AddPurchase;
    public bool GivingItem;

    private bool isProcessing;

    void Start () {
        ClearedMap = false;
        GivingItem = false;
        ItemNum = 1;
        ClearGrade = 1;
	}
	
	void Update () {

        if(isProcessing)
        {
            ClearedMap = false;
            GivingItem = false;
        }

        if(ClearedMap)
        {
            isProcessing = true;
            ClearedMap = false;
            StartCoroutine(SetMap());
        }

        if(GivingItem)
        {
            GivingItem = false;
            GiveItem();
        }
	
	}

    IEnumerator SetMap()
    {
        for(int i=StartMapId; i < EndMapId + 1; ++i)
        {
            try
            {
                GameProcess.GetGameDataManager().SaveMapRecord(i, ClearGrade);
            }
            catch
            {
                Debug.LogError("맵 클리어 설정 오류, ID:" + i);
            }
            yield return null;
        }

        StartMapId = 0;
        EndMapId = 0;
        isProcessing = false;
    }

    void GiveItem()
    {
        try
        {
            if(ItemId == 1)
            {
                GameProcess.GetGameDataManager().UserData.AddGold(ItemNum);
            }
            else if(ItemId == 2)
            {
                GameProcess.GetGameDataManager().UserData.AddCoin(ItemNum);
            }
            else if(ItemId == 3)
            {
                if(AddPurchase) GameProcess.GetGameDataManager().UserData.AddPurcase(ItemNum * 50);
                GameProcess.GetGameDataManager().UserData.AddCash(ItemNum);
            }
            else
            {
                TableData_Item item = Kazoh.Table.TableManager.GetGameData(ItemId) as TableData_Item;
                GameProcess.GetGameDataManager().AddItem(item, ItemNum);
            }
        }
        catch
        {
            Debug.LogError("아이템 지급 오류, ID:" + ItemId);
        }
        finally
        {
            ItemId = 0;
            ItemNum = 1;
            AddPurchase = false;
        }
    }
#endif
}
