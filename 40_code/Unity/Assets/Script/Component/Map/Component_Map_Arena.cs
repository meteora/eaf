﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Kazoh.Table;

public class Component_Map_Arena : Component_Map
{
    public Component_Map_Monster Leader;
    public bool IsFightSelf;
    protected List<Component_Mob_Player> otherList;

    public override void Init(TableData_Map _data, Data_UserCharacter _cha)
    {
        try
        {
            base.Init(_data, _cha);
            SetOtherPlayer();
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    void SetOtherPlayer()
    {
        otherList = new List<Component_Mob_Player>();

        foreach (Component_Map_Object c in ObjList)
        {
            if (c is Component_Mob_Player)
            {
                Component_Mob_Player mob = c as Component_Mob_Player;
                mob.DieEvent += OnDieOther;
                otherList.Add(mob);
            }
        }

#if UNITY_EDITOR
        if(TestMode)
        {
            for(int i=0; i < otherList.Count; ++i)
            {
                otherList[i].TestInit();
            }
        }
        else
        {
            for (int i = 0; i < otherList.Count; ++i)
            {
                Data_Arena data = i > 0 ? GameProcess.GetArenaManager().Next() : GameProcess.GetArenaManager().CurData;
                otherList[i].Init(data);
            }
        }
#else
        for (int i = 0; i < otherList.Count; ++i)
        {
            Data_Arena data = i > 0 ? GameProcess.GetArenaManager().Next() : GameProcess.GetArenaManager().CurData;
            otherList[i].Init(data);
        }
#endif
    }

    void OnDieOther(Component_Mob_Player mob)
    {
        if (otherList.Contains(mob)) otherList.Remove(mob);
        if (otherList.Count == 0) OnGameClear();
        if (Leader != null && Leader == mob) KillAllOther(mob.Pos);
    }

    void KillAllOther(Vector3 _pos)
    {
        for (int i = 0; i < otherList.Count; ++i)
        {
            IAttackable target = otherList[i];
            if (target != null) target.Attacked(_pos, 1000, false, false, 1);
        }
    }
}
