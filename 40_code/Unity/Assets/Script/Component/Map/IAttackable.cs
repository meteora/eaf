﻿using UnityEngine;

public interface IAttackable {

    void Attacked(Vector3 _pos, int _atk, bool _critical, bool _knockback, int _hit);
    bool IsDie();

}
