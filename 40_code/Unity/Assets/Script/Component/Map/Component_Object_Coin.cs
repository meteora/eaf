﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

using Kazoh.Table;

public class Component_Object_Coin : Component_Map_Object, IManagedDepth
{
    public Action<int, int, Data_Reward> OpenEvent;

    public int NpcId;
    public int Range;
    public UISprite NpcSprite;
    public int SpriteNum;
    public GameObject PfCoinEffect;
    public GameObject PfItemEffect;

    public TableData_Npc Data { get; protected set; }

    private string spriteFormat;
    private UISprite shadowSprite;

    public override void Init()
    {
        base.Init();
        Data = TableManager.GetGameData(NpcId) as TableData_Npc;
        if (Data == null) throw new GameException(GameException.ErrorCode.NoGameData);

        spriteFormat = Data.SpriteName + "_{0:00}";
        NpcSprite.spriteName = string.Format(spriteFormat, 1);

        Transform shadowTransform = transform.FindChild("shadow");
        if (shadowTransform != null) shadowSprite = shadowTransform.GetComponent<UISprite>();

        isStop = false;
        StartCoroutine(PlayAnim());
        StartCoroutine(Trace());
    }

    IEnumerator Trace()
    {
        while (true)
        {
            yield return null;
            if (CheckDistant()) break;
        }
        isStop = true;
        Give();
    }

    bool isStop;
    IEnumerator PlayAnim()
    {
        int num = 0;
        WaitForSeconds delay = new WaitForSeconds(0.1f);
        while (!isStop)
        {
            NpcSprite.spriteName = string.Format(spriteFormat, num % SpriteNum + 1);
            num++;
            yield return delay;
        }
    }

    bool CheckDistant()
    {
        float dist = Mathf.Pow(map.Player.Pos.y - Pos.y, 2) + Mathf.Pow(map.Player.Pos.x - Pos.x, 2);
        if (dist > Mathf.Pow(Range, 2)) return false;

        return true;
    }
    
    void Give()
    {
        Data_Reward reward = Data_Reward.DoDrop(Data.DropList);
        if (OpenEvent != null)
        {
            OpenEvent(0, 0, reward);
        }
        if (reward.ItemRewardList.Count > 0)
        {
            TableData_Item item = TableManager.GetGameData(reward.ItemRewardList[0].ItemId) as TableData_Item;
            if (item != null)
            {
                ShowEffect(item.IconName, PfItemEffect);
            }
        }
        else if (reward.Gold > 0)
        {
            ShowEffect("coin_01", PfCoinEffect);
        }
        else if (reward.Coin > 0)
        {
            ShowEffect("coin_02", PfCoinEffect);
        }
        Hide();
    }

    //public override void Hide()
    //{
    //    BoxCollider coll = GetComponent<BoxCollider>();
    //    if (coll != null) coll.enabled = false;
    //    StartCoroutine(Despawn());
    //}

    //IEnumerator Despawn()
    //{
    //    yield return new WaitForSeconds(1);
    //    while(NpcSprite.alpha > 0)
    //    {
    //        NpcSprite.alpha -= 0.1f;
    //        yield return null;
    //    }
    //}

    void ShowEffect(string _sprite, GameObject _pf)
    {
        GameObject go = NGUITools.AddChild(transform.parent.gameObject, _pf);
        Component_Effect_Coin effect = go.GetComponent<Component_Effect_Coin>();
        if (effect != null)
        {
            go.transform.position = transform.position;
            effect.Init(_sprite);
            effect.Play();
        }
        else Destroy(go);
    }

    public void SetDepth(int _depth)
    {
        if (NpcSprite.depth != _depth)
        {
            NpcSprite.depth = _depth;
            if (shadowSprite != null) shadowSprite.depth = _depth - 1;
        }
    }

    public float GetPosY()
    {
        return Pos.y;
    }
}
