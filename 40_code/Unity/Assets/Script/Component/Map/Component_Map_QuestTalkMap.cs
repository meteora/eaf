﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Kazoh.Table;

public class Component_Map_QuestTalkMap : Component_Map
{
    private Component_Map_Dialog dialogUI;
    public Component_Map_NPC NpcWhenHasItem;
    public Component_Map_NPC NpcWhenHasNotItem;
    public int QuestItemId;
    public int NeedItemNum;
    public int NeedGold;
    public bool IsDeleteItem;
    public bool IsDeleteGold;

    private bool hasItem;

    public override void Init(TableData_Map _data, Data_UserCharacter _cha)
    {
        try
        {
            base.Init(_data, _cha);
            SetNpc();
            SetDialog();
#if UNITY_EDITOR
            if (TestMode) TestCheckQuestItem();
            else CheckQuestItem();
#else
            CheckQuestItem();
#endif
        }    
        catch(Exception e)
        {
            throw e;
        }
    }

    void SetNpc()
    {
        foreach(Component_Map_Object c in ObjList)
        {
            if(c is Component_Map_NPC)
            {
                (c as Component_Map_NPC).TalkEvent += TalkStart;
            }
        }
    }

    void SetDialog()
    {
        dialogUI = TopPanel.GetComponentInChildren<Component_Map_Dialog>();
        if (dialogUI != null)
        {
            dialogUI.Init();
            dialogUI.CloseEvent += TalkEnd;
        }
    }

    void CheckQuestItem()
    {
        if(QuestItemId > 0)
        {
            List<Slot_Item> list = GameProcess.GetGameDataManager().GetInventory().FindAll(x => !x.IsEmpty && x.ItemId == QuestItemId);
            if (list != null)
            {
                int curItemNum = 0;
                for (int i = 0; i < list.Count; ++i)
                {
                    if (list[i].Data == null) continue;
                    curItemNum += list[i].Data.Num;
                }
                if (curItemNum >= NeedItemNum) hasItem = true;
                if (hasItem && GameProcess.GetGameDataManager().UserData.Gold < NeedGold) hasItem = false;
            }
        }
        else if (GameProcess.GetGameDataManager().UserData.Gold >= NeedGold) hasItem = true;

        if (hasItem)
        {
            if(NpcWhenHasNotItem != null) NpcWhenHasNotItem.Hide();
        }
        else if(NpcWhenHasNotItem != null) NpcWhenHasItem.Hide();
    }

#if UNITY_EDITOR
    void TestCheckQuestItem()
    {
        if (HasQuestItem) NpcWhenHasNotItem.Hide();
        else NpcWhenHasItem.Hide();
    }
#endif

    protected virtual void TalkStart(Component_Map_NPC npc)
    {
        dialogUI.Show(npc.Name, npc.DialogList);
    }

    protected virtual void TalkEnd()
    {
        if (hasItem)
        {
            if(IsDeleteItem) GameProcess.GetGameDataManager().UseQuestItem(QuestItemId, NeedItemNum);
            if (IsDeleteGold) GameProcess.GetGameDataManager().UserData.AddGold(-NeedGold);
            OnGameClear();
        }
        else OnGameOver();
    }
}
