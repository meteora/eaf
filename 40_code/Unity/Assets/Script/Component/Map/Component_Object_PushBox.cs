﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

using Kazoh.Table;

public class Component_Object_PushBox : Component_Map_Object, IManagedDepth
{
    public Action ClearEvent;

    public int Range;
    public int Weight;
    public Component_Map_Cell LinkedCell;
    public Component_Map_Cell[] GoalCell;
    public UISprite NpcSprite;

    public bool IsLocatedAtGoalCell
    {
        get { return CheckClear(); }
    }
    
    private string spriteFormat;
    private UISprite shadowSprite;

    private GameEnum.Direction objDir;
    private Component_Map_Cell nextCell;
    private TweenPosition tp;

    public override void Init()
    {
        base.Init();
        tp = GetComponent<TweenPosition>();
        if (tp == null)
        {
            tp = gameObject.AddComponent<TweenPosition>();
            tp.enabled = false;
        }

        Transform shadowTransform = transform.FindChild("shadow");
        if (shadowTransform != null) shadowSprite = shadowTransform.GetComponent<UISprite>();
    }

    bool CheckDistant()
    {
        float dist = Mathf.Pow(map.Player.Pos.y - Pos.y, 2) + Mathf.Pow(map.Player.Pos.x - Pos.x, 2);
        if (dist > Mathf.Pow(Range, 2)) return false;

        return true;
    }

    bool isLock;
    void OnClick()
    {
        if (isLock) return;
        isLock = true;
        if (CheckDistant())
        {
            map.Player.OnClickObject(this);
            if(CheckAbleToMove()) Move();
        }
        else
        {
            LinkedCell.gameObject.SendMessage("OnClick", SendMessageOptions.DontRequireReceiver);
        }
        isLock = false;
    }

    void Move()
    {
        float speed = Mathf.Max(0.5f, 1.2f - (map.Player.CharacterData.Pow - Weight) * 0.2f);
        tp = TweenPosition.Begin(gameObject, speed, nextCell.Pos);
        tp.eventReceiver = gameObject;
        tp.callWhenFinished = "FinishMoving";
    }

    bool CheckAbleToMove()
    {
        if (map.Player.CharacterData.Pow < Weight) return false;

        float x = map.Player.Pos.x - Pos.x;
        float y = map.Player.Pos.y - Pos.y;
        if(Mathf.Abs(x) > Math.Abs(y))
        {
            // 플레이어가 상자 오른쪽에 있을 때.
            if(x > 0) SetNextCell(GameEnum.Direction.left);
            else SetNextCell(GameEnum.Direction.right);
        }
        else
        {
            // 플레이어가 상자 윗쪽에 있을 때.
            if(y > 0) SetNextCell(GameEnum.Direction.down);
            else SetNextCell(GameEnum.Direction.up);
        }

        return nextCell != null;
    }

    void SetNextCell(GameEnum.Direction _dir)
    {
        nextCell = map.GetCell(LinkedCell, _dir);
        if (nextCell.IsBlock) nextCell = null;
        if (nextCell != null) nextCell.IsBlock = true;
    }

    void FinishMoving()
    {
        LinkedCell.IsBlock = false;
        LinkedCell = nextCell;
        nextCell = null;
        if (CheckClear() && ClearEvent != null) ClearEvent(); 
    }

    void Spawn()
    {
        transform.localPosition = LinkedCell.Pos;
        LinkedCell.IsBlock = true;
        if (GoalCell != null && GoalCell.Length > 0)
        {
            for(int i=0; i < GoalCell.Length; ++i)
            {
                if(GoalCell[i] != null) GoalCell[i].IsBlock = false;
            }
        }
    }

    bool CheckClear()
    {
        if (GoalCell == null || GoalCell.Length == 0) return false;

        for (int i = 0; i < GoalCell.Length; ++i)
        {
            if (GoalCell[i] != null && GoalCell[i] == LinkedCell) return true;
        }

        return false; 
    }

    public override void Hide()
    {
        BoxCollider coll = GetComponent<BoxCollider>();
        if (coll != null) coll.enabled = false;
        StartCoroutine(Despawn());
    }

    IEnumerator Despawn()
    {
        yield return new WaitForSeconds(1);
        while(NpcSprite.alpha > 0)
        {
            NpcSprite.alpha -= 0.1f;
            yield return null;
        }
        LinkedCell.IsBlock = false;
    }

    public void SetDepth(int _depth)
    {
        if (NpcSprite.depth != _depth)
        {
            NpcSprite.depth = _depth;
            if (shadowSprite != null) shadowSprite.depth = _depth - 1;
        }
    }

    public float GetPosY()
    {
        return Pos.y;
    }
}
