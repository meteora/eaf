﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

using Kazoh.Table;

public class Component_Mob_Player : Component_Map_Object, IAttackable, IManagedDepth
{
    public enum State
    {
        None,
        Spawn,
        Wait,
        Move,
        Attack,
        Attacked,
        Die,
        End,
    }

    public Action<Component_Mob_Player> DieEvent;
    
    public int Range = 32;
    public bool HasShadow = true;
    public bool NoWalkSound;
    public bool IsStartedFromHide;
    public Component_Map_Cell SpawnCell;
    public UISprite NpcSprite;
    public UISprite HpBar;
    public List<UILabel> DmgLabelList;
    public GameObject PfCoinEffect;
    public GameObject PfItemEffect;

#if UNITY_EDITOR
    public int OtherChaId;
    public int OtherSAtk;
    public int OtherLAtk;
    public int OtherASpd;
    public int OtherSpd;
    public int OtherDef;
    public int OtherHp;
    public int OtherWbp;
    public int OtherCri;
#endif

    public float Speed { get; private set; }

    protected Component_Map_Cell curCell;
    protected Component_Map_Cell preCell;
    protected GameEnum.Direction curDir;

    private Data_Arena.Param param;
    private float delay = float.MinValue;
    private float attackDelay = float.MinValue;
    private float cp;
    private int hp;
    private int fireRange;
    private int spriteNum;
    private GameObject pfBullet;
    private UISprite shadowSprite;
    private int hit = 1;

    private State state;
    public State CurState
    {
        get { return state; }
        private set
        {
            if (state == value) return;
#if DEBUG_MODE || UNITY_EDITOR
            //Debug.Log(string.Format("몬스터 상태 변경 : {0} -> {1}", state, value));
#endif
            state = value;
            switch (state)
            {
                case State.None:
                    break;

                case State.Spawn:
                    Spawn();
                    break;

                case State.Wait:
                    Wait();
                    break;

                case State.Move:
                    break;

                case State.Attack:
                    break;

                case State.Attacked:
                    break;

                case State.Die:
                    Die();
                    break;

                case State.End:
                    End();
                    break;
            }
        }
    }

    private string spriteFormat;

    public void Init(Data_Arena _data)
    {
        try
        {
            if (_data == null) throw new GameException(GameException.ErrorCode.NoGameData);

            Name = _data.Name;
            param = _data.Ability;

            TableData_Character cha = TableManager.GetGameData(_data.ChaId) as TableData_Character;
            if(cha != null)
            {
                spriteFormat = cha.SpriteName + "_{0:00}";
                pfBullet = Resources.Load("Prefabs/" + cha.BulletName) as GameObject;
            }
            else
            {
                spriteFormat = "cha_0004_{0:00}";
                pfBullet = Resources.Load("Prefabs/pf_blade_01") as GameObject;
            }
            for(int i = 0; i < _data.EquipList.Count; ++i)
            {
                TableData_Item equip = TableManager.GetGameData(_data.EquipList[i].Id) as TableData_Item;
                if(equip != null && equip.SlotIdx == 1)
                {
                    pfBullet = Resources.Load("Prefabs/" + equip.BulletName) as GameObject;
                    hit = equip.Hit;
                    break;
                }
            }

            cp = GameProcess.GetGameConfig().DefaultCriPercent * 0.01f;
            hp = param.Hp;
            fireRange = 9 * 32;
            if (param.Spd > 0) Speed = 32f / param.Spd;
            else Speed = 32f;

            foreach (UILabel lb in DmgLabelList) { lb.alpha = 0f; }

            Transform shadowTransform = transform.FindChild("shadow");
            if (shadowTransform != null)
            {
                if (HasShadow)
                {
                    shadowSprite = shadowTransform.GetComponent<UISprite>();
                    if (shadowSprite != null) shadowSprite.alpha = 1f;
                }
                else
                {
                    shadowSprite = shadowTransform.GetComponent<UISprite>();
                    if (shadowSprite != null) shadowSprite.alpha = 0f;
                }
            }
            
            CurState = State.Spawn;
        }
        catch(Exception e)
        {
            throw e;
        }
    }

#if UNITY_EDITOR
    public void TestInit()
    {
        Data_Arena data = new Data_Arena(OtherChaId, "icon_item_6000", 1, "Test", 1, OtherSAtk, OtherLAtk, OtherASpd, OtherSpd, OtherDef, OtherHp, OtherWbp, OtherCri);
        Init(data);    
    }
#endif

    void LateUpdate()
    {
        if (map.IsGameStart)
        {
            ExcuteFSM();
            UpdateUI();
        }
    }

    void ExcuteFSM()
    {
        if (map.IsGameFinish) CurState = State.End;
        else if (hp == 0) CurState = State.Die;

        switch (CurState)
        {
            case State.None:
                break;

            case State.Spawn:
                CurState = State.Wait;
                break;

            case State.Wait:
                if (CheckDistant())
                {
                    curCell = preCell;
                    CurState = State.Attack;
                }
                else if (CheckFireDice() && CheckFireRange())
                {
                    CurState = State.Attack;
                }
                else if (Speed < 32f) CurState = State.Move;
                break;

            case State.Move:
                if (HasArrived())
                {
                    transform.localPosition = curCell.Pos;
                    preCell = curCell;
                    CurState = State.Wait;
                }
                else if (CheckDistant())
                {
                    curCell = preCell;
                    transform.localPosition = curCell.Pos;
                    CurState = State.Wait;
                }
                //else if (CheckFireDice() && CheckFireRange())
                //{
                //    CurState = State.Attack;
                //}
                else Move();
                break;

            case State.Attack:
                if (CheckDistant()) Attack();
                else if (CheckFireDice() && CheckFireRange()) Fire();
                else CurState = State.Wait;
                break;

            case State.Die:
                break;

            case State.End:
                break;
        }
    }

    void UpdateUI()
    {
        HpBar.fillAmount = (hp + 0f) / param.Hp;
    }

    void Spawn()
    {
        if (IsStartedFromHide)
        {
            NpcSprite.alpha = 0f;
            HpBar.alpha = 0f;
        }
        transform.localPosition = SpawnCell.Pos;
        LookAt(GameEnum.Direction.down);
        curCell = SpawnCell;
        preCell = SpawnCell;
    }

    protected virtual void Wait()
    {
        curDir = GetDirection(map.Player.Pos);
        if (curDir != GameEnum.Direction.none)
        {
            curCell = GetNextCell(curDir);
        }

#if DEBUG_MODE
        Debug.Log("Path " + curCell.Idx + "/ Dir "+curDir.ToString());
#endif
    }

    void Move()
    {
        switch (curDir)
        {
            case GameEnum.Direction.left:
                transform.localPosition += Vector3.left * Time.deltaTime * param.Spd;
                break;

            case GameEnum.Direction.right:
                transform.localPosition += Vector3.right * Time.deltaTime * param.Spd;
                break;

            case GameEnum.Direction.up:
                transform.localPosition += Vector3.up * Time.deltaTime * param.Spd;
                break;

            case GameEnum.Direction.down:
                transform.localPosition += Vector3.down * Time.deltaTime * param.Spd;
                break;
        }

        if (delay < Time.time)
        {
            delay = 0.33f * Speed;
            float audioLength = GameProcess.GetEffectLength(SOUND_EFFECT.MOVE1);
            if (audioLength > delay) delay = audioLength;
            delay = Time.time + delay;
            if (!NoWalkSound) GameProcess.PlaySound(SOUND_EFFECT.MOVE1);
            NpcSprite.spriteName = string.Format(spriteFormat, (spriteNum % 3 + (int)curDir));
            spriteNum++;
        }
    }

    void Attack()
    {
        if ((map.Player as IAttackable).IsDie()) return;
        if (NpcSprite.alpha == 0f) StartCoroutine(Appear());
        LookAt(map.Player.Pos);
        if (attackDelay < Time.time)
        {
            attackDelay = param.ASpd * 0.001f + Time.time;
            bool isCritical = IsCraticalAttack();
            GameProcess.PlaySound(SOUND_EFFECT.FIRE3);
            int atk = isCritical ? Mathf.CeilToInt(param.SAtk * cp) : param.SAtk;
            map.Player.Attacked(Pos, atk, isCritical, false, hit);
        }
    }

    IEnumerator Appear()
    {
        while (NpcSprite.alpha < 1f)
        {
            NpcSprite.alpha += 0.1f;
            yield return null;
        }

        NpcSprite.alpha = 1f;
        HpBar.alpha = 1f;
    }

    void Fire()
    {
        if ((map.Player as IAttackable).IsDie()) return;
        if (attackDelay > Time.time) return;
        if (NpcSprite.alpha == 0f) StartCoroutine(Appear());

        GameEnum.Direction dir = GetDirection(map.Player.Pos);
        LookAt(dir);
        attackDelay = param.ASpd * 0.001f + Time.time;

        GameObject go = NGUITools.AddChild(transform.parent.gameObject, pfBullet);
        if (go != null)
        {
            Component_Map_Bullet bullet = go.GetComponent<Component_Map_Bullet>();
            if (bullet != null)
            {
                bool isCritical = IsCraticalAttack();
                int atk = isCritical ? Mathf.CeilToInt(param.LAtk * cp) : param.LAtk;
                bullet.Init(map, Component_Map_Bullet.TargetType.Player, NpcSprite.depth);
                bullet.Shoot(dir, Pos, atk, isCritical);
            }
            else
            {
#if UNITY_EDITOR
                Debug.Log("탄환 없음");
#endif
            }
        }
        else
        {
#if UNITY_EDITOR
            Debug.Log("탄환 프리펩 없음");
#endif
        }
    }

    bool CheckFireDice()
    {
        if (param.SAtk > param.LAtk)
        {
            int dice = UnityEngine.Random.Range(0, param.SAtk + param.LAtk);
            if (dice < param.SAtk) return false;
        }
        return true;
    }

    bool IsCraticalAttack()
    {
        int dice = UnityEngine.Random.Range(0, 10000);
        if (dice < param.Cri) return true;
        return false;
    }

    void Die()
    {
        NpcSprite.spriteName = "die_01";
        StartCoroutine(OnDie());
    }

    IEnumerator OnDie()
    {
        yield return new WaitForSeconds(0.3f);
        SendEventAndHide();
    }

    void End()
    {

    }

    bool CheckDistant()
    {
        float dist = Mathf.Pow(map.Player.Pos.y - Pos.y, 2) + Mathf.Pow(map.Player.Pos.x - Pos.x, 2);
        if (dist > Mathf.Pow(Range, 2)) return false;

        return true;
    }

    bool CheckFireRange()
    {
        float dist = Mathf.Pow(map.Player.Pos.y - Pos.y, 2) + Mathf.Pow(map.Player.Pos.x - Pos.x, 2);
        if (dist > Mathf.Pow(fireRange, 2)) return false;
        if (!IsSameLine()) return false;
        return true;
    }

    bool IsSameLine()
    {
        Vector3 pPos = map.Player.Pos;
        Vector3 cPos = pPos - Pos;
        if (cPos.x < 0.5f && cPos.x > -0.5f) return true;
        if (cPos.y < 0.5f && cPos.y > -0.5f) return true;

        return false;
    }

    bool HasArrived()
    {
        switch (curDir)
        {
            case GameEnum.Direction.left:
                if (curCell.Pos.x < Pos.x) return false;
                break;

            case GameEnum.Direction.right:
                if (curCell.Pos.x > Pos.x) return false;
                break;

            case GameEnum.Direction.up:
                if (curCell.Pos.y > Pos.y) return false;
                break;

            case GameEnum.Direction.down:
                if (curCell.Pos.y < Pos.y) return false;
                break;
        }

        return true;
    }

    public virtual void Attacked(Vector3 _pos, int _atk, bool _critical, bool _knockback, int _hit)
    {
        if (CurState == State.Wait || CurState == State.Move || CurState == State.Attack)
        {
            if (NpcSprite.alpha == 0f) StartCoroutine(Appear());

            GameEnum.Direction dir = GetDirection(_pos);
            LookAt(dir);

            if (_knockback && _critical)
            {
                int idx = curCell.Idx;
                switch (dir)
                {
                    case GameEnum.Direction.left: idx++; break;
                    case GameEnum.Direction.right: idx--; break;
                    case GameEnum.Direction.up: idx += map.ColNum; break;
                    case GameEnum.Direction.down: idx -= map.ColNum; break;
                }
                Component_Map_Cell cell = map.GetCell(idx);
                if (!cell.IsBlock)
                {
                    if (dir == GameEnum.Direction.left) transform.localPosition += Vector3.right * 32;
                    else if (dir == GameEnum.Direction.right) transform.localPosition += Vector3.left * 32;
                    else if (dir == GameEnum.Direction.up) transform.localPosition += Vector3.down * 32;
                    else if (dir == GameEnum.Direction.down) transform.localPosition += Vector3.up * 32;
                    curCell = cell;
                }
            }

            int dmg = Mathf.Max(1, _atk - param.Def);
            hp = Mathf.Max(0, hp - dmg);

            map.Shake();
            StartCoroutine(ShowDmg(dmg, _critical, _hit));
        }
    }

    public virtual bool IsDie()
    {
        return CurState == State.Die;
    }

    void ShowDmgLabel(int dmg, bool _critical)
    {
        GameProcess.PlaySound(SOUND_EFFECT.HIT1);

        for (int i = 0; i < DmgLabelList.Count; ++i)
        {
            if (DmgLabelList[i].GetComponent<Animation>().isPlaying) continue;

            if (_critical) DmgLabelList[i].text = string.Format("[ff0000]CRITICAL\n-{0}[-]", dmg);
            else DmgLabelList[i].text = string.Format("[ff0000]-{0}[-]", dmg);
            DmgLabelList[i].GetComponent<Animation>().Play();
            break;
        }
    }

    WaitForSeconds dmgDelay = new WaitForSeconds(0.1f);
    IEnumerator ShowDmg(int dmg, bool _critical, int _hit)
    {
        if (_critical) _hit = 1;
        for (int i = 0; i < _hit; ++i)
        {
            yield return null;
            int d = dmg / (_hit - i);
            ShowDmgLabel(d, _critical);
            dmg -= d;
            yield return dmgDelay;
        }
    }

    protected GameEnum.Direction GetDirection(Vector3 _pos)
    {
        Vector3 v = Pos - _pos;
        float x = Mathf.Abs(v.x);
        float y = Mathf.Abs(v.y);

        GameEnum.Direction dir = GameEnum.Direction.none;
        if (x < 32)
        {
            if (v.y < 0) dir = GameEnum.Direction.up;
            else dir = GameEnum.Direction.down;
        }
        else if (y < 32)
        {
            if (v.x < 0) dir = GameEnum.Direction.right;
            else dir = GameEnum.Direction.left;
        }
        else if (x < y)
        {
            if (v.x < 0) dir = GameEnum.Direction.right;
            else dir = GameEnum.Direction.left;
        }
        else
        {
            if (v.y < 0) dir = GameEnum.Direction.up;
            else dir = GameEnum.Direction.down;
        }
        //if(x > y)
        //{
        //    if (v.x < 0) dir = GameEnum.Direction.right;
        //    else dir = GameEnum.Direction.left;
        //}
        //else
        //{
        //    if (v.y < 0) dir = GameEnum.Direction.up;
        //    else dir = GameEnum.Direction.down;
        //}

        return dir;
    }

    void LookAt(Vector3 _pos)
    {
        GameEnum.Direction dir = GameEnum.Direction.down;
        if (_pos.x > Pos.x) dir = GameEnum.Direction.right;
        else if (_pos.x < Pos.x) dir = GameEnum.Direction.left;
        else if (_pos.y > Pos.y) dir = GameEnum.Direction.up;

        LookAt(dir);
    }

    void LookAt(GameEnum.Direction dir)
    {
        NpcSprite.spriteName = string.Format(spriteFormat, (2 + (int)dir));
    }

    protected virtual Component_Map_Cell GetNextCell(GameEnum.Direction dir)
    {
        int idx = curCell.Idx;
        switch (dir)
        {
            case GameEnum.Direction.left: idx--; break;
            case GameEnum.Direction.right: idx++; break;
            case GameEnum.Direction.up: idx -= map.ColNum; break;
            case GameEnum.Direction.down: idx += map.ColNum; break;
        }

        Component_Map_Cell cell = map.GetCell(idx);
        if (cell.IsBlock)
        {
            switch (dir)
            {
                case GameEnum.Direction.left:
                case GameEnum.Direction.right:
                    if (map.Player.Pos.y > transform.localPosition.y) curDir = GameEnum.Direction.up;
                    else curDir = GameEnum.Direction.down;
                    break;

                case GameEnum.Direction.up:
                case GameEnum.Direction.down:
                    if (map.Player.Pos.x > transform.localPosition.x) curDir = GameEnum.Direction.right;
                    else curDir = GameEnum.Direction.left;
                    break;
            }

            idx = curCell.Idx;
            switch (curDir)
            {
                case GameEnum.Direction.left: idx--; break;
                case GameEnum.Direction.right: idx++; break;
                case GameEnum.Direction.up: idx -= map.ColNum; break;
                case GameEnum.Direction.down: idx += map.ColNum; break;
            }

            cell = map.GetCell(idx);
            if (cell == null || cell.IsBlock) cell = curCell;
        }

        return cell;
    }

    bool isLock;
    void OnClick()
    {
        if (isLock) return;
        isLock = true;
        map.Player.OnClickObject(this);
        isLock = false;
    }

    void ShowEffect(string _sprite, GameObject _pf)
    {
        GameObject go = NGUITools.AddChild(transform.parent.gameObject, _pf);
        Component_Effect_Coin effect = go.GetComponent<Component_Effect_Coin>();
        if (effect != null)
        {
            effect.FinishEvent += SendEventAndHide;
            go.transform.position = map.Player.transform.position;
            effect.Init(_sprite);
            effect.Play();
        }
        else
        {
            SendEventAndHide();
            Destroy(go);
        }
    }

    void SendEventAndHide()
    {
        transform.localPosition = new Vector3(0, 1000, 0);
        if (DieEvent != null) DieEvent(this);
        Hide();
    }

    public void SetDepth(int _depth)
    {
        if (NpcSprite.depth != _depth)
        {
            NpcSprite.depth = _depth;
            //if (shadowSprite != null) shadowSprite.depth = _depth - 1;
            if (HpBar != null) HpBar.depth = _depth + 1;
        }
    }

    public float GetPosY()
    {
        return Pos.y;
    }
}
