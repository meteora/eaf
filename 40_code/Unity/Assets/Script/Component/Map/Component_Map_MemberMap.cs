﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Kazoh.Table;

public class Component_Map_MemberMap : Component_Map
{
    private Component_Map_Dialog dialogUI;
    public Component_Map_NPC NpcWhenHasMembership;
    public Component_Map_NPC NpcWhenHasNotMembership;
    public int Membership;

    public override void Init(TableData_Map _data, Data_UserCharacter _cha)
    {
        try
        {
            base.Init(_data, _cha);
            SetNpc();
            SetDialog();
#if UNITY_EDITOR
            if (TestMode) TestCheckMembership();
            else CheckMembership();
#else
            CheckMembership();
#endif
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    void SetNpc()
    {
        foreach(Component_Map_Object c in ObjList)
        {
            if(c is Component_Map_NPC)
            {
                (c as Component_Map_NPC).TalkEvent += TalkStart;
            }
        }
    }

    void SetDialog()
    {
        dialogUI = TopPanel.GetComponentInChildren<Component_Map_Dialog>();
        if (dialogUI != null)
        {
            dialogUI.Init();
            dialogUI.CloseEvent += TalkEnd;
        }
    }

    void CheckMembership()
    {
        if(GameProcess.GetGameDataManager().GetMembership() < Membership)
        {
            NpcWhenHasMembership.Hide();
        }
        else
        {
            NpcWhenHasNotMembership.Hide();
        }
    }

#if UNITY_EDITOR
    void TestCheckMembership()
    {
        if (HasMembership) NpcWhenHasNotMembership.Hide();
        else NpcWhenHasMembership.Hide();
    }
#endif

    protected virtual void TalkStart(Component_Map_NPC npc)
    {
        dialogUI.Show(npc.Name, npc.DialogList);
    }

    protected virtual void TalkEnd()
    {
        if (GameProcess.GetGameDataManager().GetMembership() < Membership) OnGameOver();
        else OnGameClear();
    }
}
