﻿using UnityEngine;
using System.Collections;
using System;

using Kazoh.Table;

public class Component_Object_DamageField : Component_Map_Object
{
    public int NpcId;
    public int Width;
    public int Height;
    public int StartNum;
    public int SpriteNum;
    public float WaitTime;

    public TableData_Npc Data { get; protected set; }

    private UISprite npcSprite;
    private string spriteFormat;
    private float delay;
    private bool isTrace;

    ArrayList listAttackedObj = new ArrayList();

    public override void Init()
    {
        npcSprite = gameObject.GetComponentInChildren<UISprite>();
        Data = TableManager.GetGameData(NpcId) as TableData_Npc;
        if(Data != null)
        {
            delay = Data.ASpd * 0.001f;
            spriteFormat = Data.SpriteName + "_{0:00}";
            npcSprite.spriteName = string.Format(spriteFormat, StartNum);
        }

        StartCoroutine(Trace());
        StartCoroutine(PlayAnimation());
        base.Init();
    }

    IEnumerator PlayAnimation()
    {
        while(true)
        {
            yield return new WaitForSeconds(delay);

            listAttackedObj.Clear();
            isTrace = true;

            for (int i=0; i < SpriteNum; ++i)
            {
                npcSprite.spriteName = string.Format(spriteFormat, StartNum + i);
                yield return null;
            }

            yield return new WaitForSeconds(WaitTime);

            for (int i=0; i < SpriteNum; ++i)
            {
                npcSprite.spriteName = string.Format(spriteFormat, StartNum + SpriteNum - 1 - i);
                yield return null;
            }

            isTrace = false;
        }
    }

    IEnumerator Trace()
    {
        while (true)
        {
            if (isTrace)
            {
                if (!listAttackedObj.Contains(map.Player) && EnterArea(map.Player.Pos))
                {
                    OnEvent(map.Player as IAttackable);
                    listAttackedObj.Add(map.Player);
                }
                else
                {
                    for (int i = 0; i < map.ObjList.Count; ++i)
                    {
                        if (map.ObjList[i] is IAttackable && !listAttackedObj.Contains(map.ObjList[i]) && EnterArea(map.ObjList[i].Pos))
                        {
                            OnEvent(map.ObjList[i] as IAttackable);
                            listAttackedObj.Add(map.ObjList[i]);
                        }
                    }
                }               
            }

            yield return null;
        }
    }

    bool EnterArea(Vector3 _pos)
    {
        if (_pos.y > transform.localPosition.y + Height * 0.5f) return false;
        if (_pos.y < transform.localPosition.y - Height * 0.5f) return false;
        if (_pos.x > transform.localPosition.x + Width * 0.5f) return false;
        if (_pos.x < transform.localPosition.x - Width * 0.5f) return false;

        return true;
    }

    void OnEvent(IAttackable target)
    {
        if (target != null) target.Attacked(transform.localPosition, Data.SAtk, false, false, 1); 
    }
}
