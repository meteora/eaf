﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

using Kazoh.Table;

public class Component_Map_NPC_Roaming : Component_Map_NPC
{
    public List<Component_Map_Cell> PassList;
    public bool IsRoop;
    public float WaitTime;
    public bool NoWalkSound;

    protected Component_Map_Cell curCell;
    protected Component_Map_Cell preCell;
    protected GameEnum.Direction curDir;

    private float speed;
    private int passIdx;
    private bool isMoving;
    private float delay = float.MinValue;
    private int spriteNum;

    void LateUpdate()
    {
        if (map.IsGameStart && isMoving)
        {
            Move();
        }
    }

    public override void Init()
    {
        base.Init();
        speed = 32f / Data.Spd;
        curCell = LinkedCell;
        StartCoroutine(Wait());
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(WaitTime);
        isMoving = true;
    }

    public override void Talk(Component_Map_Player player)
    {
        base.Talk(player);
        isMoving = false;
    }

    private Component_Map_Cell GetNextCell()
    {
        Component_Map_Cell cell = null;
        if (PassList == null || PassList.Count == 0) return cell;
        if (passIdx == PassList.Count && IsRoop)
        {
            passIdx = 0;
        }
        if (passIdx < PassList.Count)
        {
            cell = PassList[passIdx];
            passIdx++;
        }

        return cell;
    }

    void Move()
    {
        if (HasArrived())
        {
            transform.localPosition = curCell.Pos;
            preCell = curCell;
            curCell = GetNextCell();
            if(curCell != null)
            {
                LinkedCell = curCell;
                curDir = GetDir(curCell.Pos);
            }
        }

        if(curCell == null)
        {
            Despawn();
            return;
        }

        switch (curDir)
        {
            case GameEnum.Direction.left:
                transform.localPosition += Vector3.left * Time.deltaTime * Data.Spd;
                break;

            case GameEnum.Direction.right:
                transform.localPosition += Vector3.right * Time.deltaTime * Data.Spd;
                break;

            case GameEnum.Direction.up:
                transform.localPosition += Vector3.up * Time.deltaTime * Data.Spd;
                break;

            case GameEnum.Direction.down:
                transform.localPosition += Vector3.down * Time.deltaTime * Data.Spd;
                break;
        }

        if (delay < Time.time)
        {
            delay = 0.33f * speed;
            float audioLength = GameProcess.GetEffectLength(SOUND_EFFECT.MOVE1);
            if (audioLength > delay) delay = audioLength;
            delay = Time.time + delay;
            if (!NoWalkSound) GameProcess.PlaySound(SOUND_EFFECT.MOVE1);
            NpcSprite.spriteName = string.Format(spriteFormat, (spriteNum % 3 + (int)curDir));
            spriteNum++;
        }
    }

    GameEnum.Direction GetDir(Vector3 _pos)
    {
        GameEnum.Direction dir = GameEnum.Direction.down;
        if (_pos.x > transform.localPosition.x) dir = GameEnum.Direction.right;
        if (_pos.x < transform.localPosition.x) dir = GameEnum.Direction.left;
        if (_pos.y > transform.localPosition.y) dir = GameEnum.Direction.up;

        return dir;
    }

    void Despawn()
    {
        isMoving = false;
        StartCoroutine(Play_Despawn());
    }

    IEnumerator Play_Despawn()
    {
        while(NpcSprite.alpha > 0)
        {
            NpcSprite.alpha -= 0.1f;
            yield return null;
        }

        NpcSprite.alpha = 0f;
        Hide();
    }

    bool HasArrived()
    {
        switch (curDir)
        {
            case GameEnum.Direction.left:
                if (curCell.Pos.x < Pos.x) return false;
                break;

            case GameEnum.Direction.right:
                if (curCell.Pos.x > Pos.x) return false;
                break;

            case GameEnum.Direction.up:
                if (curCell.Pos.y > Pos.y) return false;
                break;

            case GameEnum.Direction.down:
                if (curCell.Pos.y < Pos.y) return false;
                break;
        }

        return true;
    }
}
