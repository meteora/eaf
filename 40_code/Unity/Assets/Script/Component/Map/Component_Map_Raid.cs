﻿using UnityEngine;
using System.Collections;
using System;

public class Component_Map_Raid : Component_Map_MonsterMap
{
    private string key;

    public override void Init(TableData_Map _data, Data_UserCharacter _cha)
    {
        try
        {
            base.Init(_data, _cha);
#if UNITY_EDITOR
            if(TestMode)
            {
                key = string.Format("raid_{0}", BossMob.Data.Id);
            }
            else
            {
                key = string.Format("{0}_raid_{1}", GameProcess.GetGameDataManager().GetGUID(), BossMob.Data.Id);
            }
#else
            key = string.Format("{0}_raid_{1}", GameProcess.GetGameDataManager().GetGUID(), BossMob.Data.Id);
#endif
            if (EncryptedPlayerPrefs.HasKey(key) && BossMob is Component_Mob_RaidBoss)
            {
                int hp = EncryptedPlayerPrefs.GetInt(key);
                (BossMob as Component_Mob_RaidBoss).SetHp(hp);
            }
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    protected override void OnGameClear()
    {
        if (IsGameFinish) return;
        base.OnGameClear();
        if (EncryptedPlayerPrefs.HasKey(key)) EncryptedPlayerPrefs.DeleteKey(key);
    }

    protected override void OnGameOver()
    {
        if (IsGameFinish) return;
        base.OnGameOver();
        EncryptedPlayerPrefs.SetInt(key, BossMob.Hp);
    }
}
