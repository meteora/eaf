﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Kazoh.Table;

public class Component_Map_MoveBoxMap : Component_Map
{
    private List<Component_Object_PushBox> listPushBox;
    public override void Init(TableData_Map _data, Data_UserCharacter _cha)
    {
        try
        {
            base.Init(_data, _cha);
            listPushBox = new List<Component_Object_PushBox>();
            SetPushBox();
        }    
        catch(Exception e)
        {
            throw e;
        }
    }

    void SetPushBox()
    {
        foreach(Component_Map_Object c in ObjList)
        {
            if(c is Component_Object_PushBox)
            {
                (c as Component_Object_PushBox).ClearEvent += CheckClear;
                listPushBox.Add(c as Component_Object_PushBox);
            }
        }
    }

    void CheckClear()
    {
        for(int i=0; i < listPushBox.Count; ++i)
        {
            if (!listPushBox[i].IsLocatedAtGoalCell) return;
        }

        OnGameClear();
    }
}
