﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

using Kazoh.Table;

public class Component_Object_Switch : Component_Map_Object, IManagedDepth
{
    public int NpcId;
    public int Range;
    public Component_Map_Cell LinkedCell;
    public Component_Map_Cell[] LinkedCellArray;
    public UISprite NpcSprite;
    public UISprite LinkedSprite;
    public bool IsLocked;
    public bool IsOpened;

    public TableData_Npc Data { get; protected set; }

    private string spriteFormat;
    private UISprite shadowSprite;

    public override void Init()
    {
        base.Init();
        Data = TableManager.GetGameData(NpcId) as TableData_Npc;
        if (Data == null) throw new GameException(GameException.ErrorCode.NoGameData);

        spriteFormat = Data.SpriteName + "_{0:00}";
        NpcSprite.spriteName = string.Format(spriteFormat, 1);
        Transform shadowTransform = transform.FindChild("shadow");
        if (shadowTransform != null) shadowSprite = shadowTransform.GetComponent<UISprite>();
        if (LinkedSprite != null)
        {
            if (IsOpened)
            {
                LinkedSprite.alpha = 1f;
                for(int i=0; i < LinkedCellArray.Length; ++i)
                {
                    LinkedCellArray[i].IsBlock = false;
                }
            }
            else
            {
                LinkedSprite.alpha = 0f;
                for (int i = 0; i < LinkedCellArray.Length; ++i)
                {
                    LinkedCellArray[i].IsBlock = true;
                }
            }
        }
    }

    bool CheckDistant()
    {
        float dist = Mathf.Pow(map.Player.Pos.y - Pos.y, 2) + Mathf.Pow(map.Player.Pos.x - Pos.x, 2);
        if (dist > Mathf.Pow(Range, 2)) return false;

        return true;
    }

    bool isLock;
    void OnClick()
    {
        if (isLock) return;
        isLock = true;
        if (CheckDistant())
        {
            Open();
        }
        else
        {
            LinkedCell.gameObject.SendMessage("OnClick", SendMessageOptions.DontRequireReceiver);
        }
        isLock = false;
    }

    void Open()
    {
        if (!IsLocked)
        {
            IsOpened = !IsOpened;
            StopCoroutine("PlayAnim_Open");
            StopCoroutine("PlayAnim_Close");
            GameProcess.PlaySound(SOUND_EFFECT.OPEN);
            if (IsOpened) StartCoroutine("PlayAnim_Open");
            else StartCoroutine("PlayAnim_Close");
        }
    }

    void UnLock()
    {
        IsLocked = false;
    }

    IEnumerator PlayAnim_Open()
    {
        for(int i=0; i<3; ++i)
        {
            NpcSprite.spriteName = string.Format(spriteFormat, i % 3 + 1);
            yield return new WaitForSeconds(0.1f);
        }

        if (LinkedSprite != null)
        {
            while(LinkedSprite.alpha < 1)
            {
                LinkedSprite.alpha += 0.2f;
            }
            LinkedSprite.alpha = 1f;
        }
        for (int i = 0; i < LinkedCellArray.Length; ++i)
        {
            LinkedCellArray[i].IsBlock = false;
        }
    }

    IEnumerator PlayAnim_Close()
    {
        for (int i = 2; i > -1; --i)
        {
            NpcSprite.spriteName = string.Format(spriteFormat, i % 3 + 1);
            yield return new WaitForSeconds(0.1f);
        }

        if (LinkedSprite != null)
        {
            while (LinkedSprite.alpha > 0)
            {
                LinkedSprite.alpha -= 0.2f;
            }
            LinkedSprite.alpha = 0f;
        }
        for (int i = 0; i < LinkedCellArray.Length; ++i)
        {
            LinkedCellArray[i].IsBlock = true;
        }
    }

    public void SetDepth(int _depth)
    {
        if (NpcSprite.depth != _depth)
        {
            NpcSprite.depth = _depth;
            if (shadowSprite != null) shadowSprite.depth = _depth - 1;
            if (LinkedSprite != null) LinkedSprite.depth = _depth + 1;
        }
    }

    public float GetPosY()
    {
        return Pos.y;
    }
}
