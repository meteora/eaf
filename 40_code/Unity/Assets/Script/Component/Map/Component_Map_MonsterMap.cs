﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Kazoh.Table;

public class Component_Map_MonsterMap : Component_Map
{
    public Component_Map_Monster BossMob;
    protected List<Component_Map_Monster> mobList;

    public override void Init(TableData_Map _data, Data_UserCharacter _cha)
    {
        try
        {
            base.Init(_data, _cha);
            SetMob();
        }    
        catch(Exception e)
        {
            throw e;
        }
    }

    void SetMob()
    {
        mobList = new List<Component_Map_Monster>();

        foreach(Component_Map_Object c in ObjList)
        {
            if(c is Component_Map_Monster)
            {
                Component_Map_Monster mob = c as Component_Map_Monster;
                mob.DieEvent += OnDieMonster;
                mobList.Add(mob);
            }
        }
    }

    void OnDieMonster(Component_Map_Monster mob)
    {
        if (mobList.Contains(mob)) mobList.Remove(mob);
        if (mobList.Count == 0) OnGameClear();
        if (BossMob != null && BossMob == mob) KillAllMonster(mob.Pos);
    }

    void KillAllMonster(Vector3 _pos)
    {
        for(int i=0; i < mobList.Count; ++i)
        {
            IAttackable target = mobList[i];
            if(target != null) target.Attacked(_pos, 1000, false, false, 1);
        }
    }
}
