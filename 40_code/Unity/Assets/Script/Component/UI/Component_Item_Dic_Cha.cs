﻿using UnityEngine;
using System.Collections;
using System;

using Kazoh.Table;

public class Component_Item_Dic_Cha : GameComponent
{
    public UISprite IconSprite;
    public UISprite GradeSprite;
    public UILabel NoLabel;
    public UILabel NameLabel;
    public UILabel DescLabel;
    
    public UILabel[] Params;
    public UILabel[] ParamValues;

    public void Init(int _idx, TableData_DicList _data)
    {
        try
        {
            NoLabel.text = _idx.ToString();
            TableData_Character cha = TableManager.GetGameData(_data.Data) as TableData_Character;
            IconSprite.spriteName = cha.IconName;
            GradeSprite.spriteName = "grade_0" + cha.Grade;
            NameLabel.text = TableManager.GetString(cha.Str);
            DescLabel.text = string.Format("{0}\n[ffa1f9]{1}[-]", TableManager.GetString(cha.Desc), TableManager.GetString(_data.Info_1));
            
            Params[0].text = TableManager.GetString("STR_UI_POW");
            Params[1].text = TableManager.GetString("STR_UI_DEX");
            Params[2].text = TableManager.GetString("STR_UI_INT");
            Params[3].text = TableManager.GetString("STR_UI_CON");
            Params[4].text = TableManager.GetString("STR_UI_LUC");
            Params[5].text = TableManager.GetString("STR_UI_RND");
            Params[6].text = TableManager.GetString("STR_UI_SKILL");

            ParamValues[0].text = cha.Pow.ToString();
            ParamValues[1].text = cha.Dex.ToString();
            ParamValues[2].text = cha.Int.ToString();
            ParamValues[3].text = cha.Con.ToString();
            ParamValues[4].text = cha.Luc.ToString();
            ParamValues[5].text = cha.RndPow.ToString();
            ParamValues[6].text = cha.OptionList.Count > 0 ? "O" : "X";

            Show();
        }
        catch
        {
            Hide();
        }
    }
}