﻿using UnityEngine;
using System.Collections;
using System;

using Kazoh.Table;

public class Component_Item_Rank : GameComponent
{
    public UISprite ChaSprite;
    public UISprite GradeSprite;
    public UISprite IconSprite;
    public UILabel RankLabel;
    public UILabel NameLabel;
    public UILabel ScoreLabel;
    public UILabel ItemNameLabel;

    public UISprite[] ItemIcons;
    public UISprite[] ItemGrades;
    public UILabel[] ItemEnchantNums;

    public Data_Rank Data { get; private set; }

    private string[] itemNames = { "", "", "", "" };

    public void Init(int _rank, Data_Rank _data)
    {
        if(_data == null)
        {
            Hide();
            return;
        }

        ItemNameLabel.alpha = 0f;

        Data = _data;
        RankLabel.text = string.Format("{0:#0}", _rank);
        if (Data.Id == GameProcess.GetGameDataManager().GetGUID()) RankLabel.color = Color.yellow;
        else if (_rank < 4) RankLabel.color = Color.magenta;
        else RankLabel.color = Color.white;
        NameLabel.text = Data.Name;
        ScoreLabel.text = string.Format("{0:###,##0}", Data.Record);

        try
        {
            TableData_Character chaData = TableManager.GetGameData(Data.ChaId) as TableData_Character;
            if (chaData != null)
            {
                ChaSprite.spriteName = chaData.IconName;
                GradeSprite.spriteName = "grade_0" + Data.ChaGrade;
            }
            else
            {
                ChaSprite.spriteName = "icon_item_6000";
                GradeSprite.spriteName = "";
            }
        }
        catch
        {
            ChaSprite.spriteName = "icon_item_0000";
            GradeSprite.spriteName = "";
        }

        switch(_rank)
        {
            case 1: IconSprite.spriteName = "crown_03"; break;
            case 2: IconSprite.spriteName = "crown_02"; break;
            case 3: IconSprite.spriteName = "crown_01"; break;
            default: IconSprite.spriteName = ""; break;
        }

        for(int i=0; i < ItemIcons.Length; ++i)
        {
            ItemIcons[i].spriteName = "icon_item_0000";
            ItemGrades[i].alpha = 0f;
            ItemEnchantNums[i].text = "";
            itemNames[i] = "";
        }        

        for(int i=0; i<_data.EquipList.Count; ++i)
        {
            try
            {
                TableData_Item equip = TableManager.GetGameData(_data.EquipList[i].Id) as TableData_Item;
                if (equip != null)
                {
                    ItemIcons[equip.SlotIdx].spriteName = equip.IconName;
                    ItemGrades[equip.SlotIdx].spriteName = "grade_0" + equip.Grade;
                    ItemGrades[equip.SlotIdx].alpha = 1f;
                    ItemEnchantNums[equip.SlotIdx].text = "+" + _data.EquipList[i].Enchant;
                    itemNames[equip.SlotIdx] = string.Format("+{0} {1}", _data.EquipList[i].Enchant, TableManager.GetString(equip.Str));
                }
            }
            catch
            {
#if UNITY_EDITOR || DEBUG_MODE
                Debug.LogWarning("아이템 데이터 없음. ID:" + _data.EquipList[i].Id);
#endif
            }
        }

        Show();
    }

    public void Init(Data_Arena _data)
    {
        if (_data == null)
        {
            Hide();
            return;
        }

        ItemNameLabel.alpha = 0f;
        
        RankLabel.text = string.Format("{0:#0}", _data.Rank);
        if (_data.Id == GameProcess.GetGameDataManager().GetGUID()) RankLabel.color = Color.yellow;
        else if (_data.Rank < 4) RankLabel.color = Color.magenta;
        else RankLabel.color = Color.white;
        NameLabel.text = _data.Name;
        ScoreLabel.text = string.Format("{0:###,##0}", _data.Record);

        try
        {
            TableData_Character chaData = TableManager.GetGameData(_data.ChaId) as TableData_Character;
            if (chaData != null)
            {
                ChaSprite.spriteName = chaData.IconName;
                GradeSprite.spriteName = "grade_0" + _data.Grade;
            }
            else
            {
                ChaSprite.spriteName = "icon_item_6000";
                GradeSprite.spriteName = "";
            }
        }
        catch
        {
            ChaSprite.spriteName = "icon_item_0000";
            GradeSprite.spriteName = "";
        }

        switch (_data.Rank)
        {
            case 1: IconSprite.spriteName = "crown_03"; break;
            case 2: IconSprite.spriteName = "crown_02"; break;
            case 3: IconSprite.spriteName = "crown_01"; break;
            default: IconSprite.spriteName = ""; break;
        }

        for (int i = 0; i < ItemIcons.Length; ++i)
        {
            ItemIcons[i].spriteName = "icon_item_0000";
            ItemGrades[i].alpha = 0f;
            ItemEnchantNums[i].text = "";
            itemNames[i] = "";
        }

        for (int i = 0; i < _data.EquipList.Count; ++i)
        {
            try
            {
                TableData_Item equip = TableManager.GetGameData(_data.EquipList[i].Id) as TableData_Item;
                if (equip != null)
                {
                    ItemIcons[equip.SlotIdx].spriteName = equip.IconName;
                    ItemGrades[equip.SlotIdx].spriteName = "grade_0" + equip.Grade;
                    ItemGrades[equip.SlotIdx].alpha = 1f;
                    ItemEnchantNums[equip.SlotIdx].text = "+" + _data.EquipList[i].Enchant;
                    itemNames[equip.SlotIdx] = string.Format("+{0} {1}", _data.EquipList[i].Enchant, TableManager.GetString(equip.Str));
                }
            }
            catch
            {
#if UNITY_EDITOR || DEBUG_MODE
                Debug.LogWarning("아이템 데이터 없음. ID:" + _data.EquipList[i].Id);
#endif
            }
        }

        Show();
    }

    void OnClick_Item_01()
    {
        ShowItemName(0);
    }
    void OnClick_Item_02()
    {
        ShowItemName(1);
    }
    void OnClick_Item_03()
    {
        ShowItemName(2);
    }
    void OnClick_Item_04()
    {
        ShowItemName(3);
    }

    void ShowItemName(int _idx)
    {
        if (!string.IsNullOrEmpty(itemNames[_idx]))
        {
            StopAllCoroutines();
            StartCoroutine(AnimItemName(itemNames[_idx]));
        }
    }

    IEnumerator AnimItemName(string _name)
    {
        float alpha = 2f;
        ItemNameLabel.text = _name;
        ItemNameLabel.alpha = 1f;
        while (alpha > 0)
        {
            yield return null;
            alpha -= Time.deltaTime;
            ItemNameLabel.alpha = Mathf.Min(alpha, 1f);
        }
        ItemNameLabel.alpha = 0f;
    }
}