﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

using Kazoh.Table;

public class Component_UI_CouponPopup : GameComponent
{
    public UILabel TitleLabel;
    public UILabel MsgLabel;
    public UILabel CouponNumLabel;
    public UILabel YesBtnLabel;
    public UILabel NoBtnLabel;
    public UIInput[] NumInputs;
    public UIToggle MobiToggle;
    public UIToggle LootingToggle;
    public UIToggle JjiToggle;
    public UIToggle MeteoraToggle;
    public UILabel MobiLabel;
    public UILabel LootingLabel;
    public UILabel JjiLabel;
    public UILabel MeteoraLabel;

    private int selectedToggle;

    public override void Init()
    {
        base.Init();

        /* UI 텍스트 설정 */
        TitleLabel.text = TableManager.GetString("STR_TITLE_COUPON");
        MsgLabel.text = TableManager.GetString("STR_MSG_COUPON");
        CouponNumLabel.text = TableManager.GetString("STR_UI_COUPON_NUM");
        NoBtnLabel.text = TableManager.GetString("STR_UI_CANCEL");
        YesBtnLabel.text = TableManager.GetString("STR_UI_RECEIVE");

        MobiLabel.text = TableManager.GetString("STR_UI_COUPON_SERVER_1");
        LootingLabel.text = TableManager.GetString("STR_UI_COUPON_SERVER_2");
        JjiLabel.text = TableManager.GetString("STR_UI_COUPON_SERVER_3");
        MeteoraLabel.text = TableManager.GetString("STR_UI_COUPON_SERVER_4");

        InitInputs();
    }

    void InitInputs()
    {
        for (int i = 0; i < NumInputs.Length; ++i)
        {
            NumInputs[i].value = string.Empty;
        }
    }

    bool CheckInvalid()
    {
        for (int i = 0; i < NumInputs.Length; ++i)
        {
            if (NumInputs[i].value.Length != 4) return false;
        }

        return true;
    }

    string GetCouponNum()
    {
        string couponNum = "";
        for (int i = 0; i < NumInputs.Length; ++i)
        {
            couponNum += NumInputs[i].value;
        }
        return couponNum;
    }

    public void OnClick_Close()
    {
        if (gameObject.activeSelf)
        {
            GameProcess.PlaySound(SOUND_EFFECT.CLICK);
            Hide();
        }
    }

    bool isLock;
    public void OnClick_Yes()
    {
        try
        {
            if (isLock) return;
            GameProcess.PlaySound(SOUND_EFFECT.CLICK);

            if (!CheckInvalid())
            {
                GameProcess.ShowError(new GameException(GameException.ErrorCode.InvalidCouponNum));
                return;
            }

            string couponId = GetCouponNum();
            DBManager.GetCouponCond(selectedToggle, couponId, delegate (IDictionary _dict)
            {
                try
                {
                    CheckAbleToReceive(_dict, couponId);

                    if (!_dict.Contains("COND")) throw new GameException(GameException.ErrorCode.InvalidCouponId);

                    int mapId = System.Convert.ToInt32(_dict["COND"]);
                    if (mapId > 0 && GameProcess.GetGameDataManager().GetMapRecord(mapId) == 0)
                    {
                        throw new GameException(GameException.ErrorCode.NotFulfillCond);
                    }

                    DBManager.GetCouponReward(selectedToggle, couponId, delegate (IDictionary _dict2)
                    {
                        try
                        {
                            CheckAbleToReceive(_dict2, couponId);

                            int itemId = 0;
                            int num = 0;
                            if (_dict2.Contains("ITEM_ID")) itemId = Convert.ToInt32(_dict2["ITEM_ID"]);
                            if (_dict2.Contains("VALUE")) num = Convert.ToInt32(_dict2["VALUE"]);

                            string msg = string.Empty;
                            string itemName = string.Empty;
                            Data_Reward _reward = new Data_Reward();
                            if (itemId == 1)
                            {
                                itemName = "gold";
                                _reward.SetReward(num, 0);
                                GameProcess.GetGameDataManager().ExcuteReward(_reward);
                                msg = string.Format(TableManager.GetString("STR_MSG_SREWARD_GOLD"), num);
                            }
                            else if (itemId == 2)
                            {
                                itemName = "coin";
                                _reward.SetReward(0, num);
                                GameProcess.GetGameDataManager().ExcuteReward(_reward);
                                msg = string.Format(TableManager.GetString("STR_MSG_SREWARD_COIN"), num);
                            }
                            else
                            {
                                TableData_Item _item = TableManager.GetGameData(itemId) as TableData_Item;
                                if (_item == null) throw new GameException(GameException.ErrorCode.NoGameData);

                                for (int i = 0; i < num; ++i)
                                {
                                    _reward.ItemRewardList.Add(new Data_Reward.Reward(_item.Id));
                                }
                                GameProcess.GetGameDataManager().ExcuteReward(_reward);
                                itemName = _item.Name;
                                msg = string.Format(TableManager.GetString("STR_MSG_SREWARD_ITEM"), TableManager.GetString(_item.Str), num);
                            }
                            GameProcess.ShowPopup(NoticeType.OK, TableManager.GetString("STR_TITLE_SREWARD"),
                                                    msg, TableManager.GetString("STR_UI_OK"), null, NGUIText.Alignment.Left);
                            
                            /* 로그 처리 */
                            string log = GameProcess.GetGameDataManager().GetCouponLog(itemId, itemName, num);
                            DBManager.SaveLog(log, null);

                            GameProcess.GetGameDataManager().Save();
                        }
                        catch (GameException e)
                        {
                            GameProcess.ShowError(e);
                        }
                        catch (Exception e)
                        {
                            GameProcess.ShowError(new GameException(e));
                        }
                    });
                }
                catch (GameException e)
                {
                    GameProcess.ShowError(e);
                }
                catch (Exception e)
                {
                    GameProcess.ShowError(new GameException(e));
                }
            });
        }
        catch (GameException e)
        {
            GameProcess.ShowError(e);
        }
        catch (Exception e)
        {
            GameProcess.ShowError(new GameException(e));
        }
        finally
        {
            InitInputs();
            isLock = false;
        }
    }

    void CheckAbleToReceive(IDictionary _dict, string _couponId)
    {
        if (!_dict.Contains("ID") || !_couponId.Equals(System.Convert.ToString(_dict["ID"])))
        {
            throw new GameException(GameException.ErrorCode.InvalidCouponId);
        }

        int itemId = 0;
        int num = 0;
        if (_dict.Contains("ITEM_ID")) itemId = Convert.ToInt32(_dict["ITEM_ID"]);
        if (_dict.Contains("VALUE")) num = Convert.ToInt32(_dict["VALUE"]);
        if (itemId == 0)
        {
            throw new GameException(GameException.ErrorCode.InvalidParam);
        }
        else if (itemId == 1)
        {

            int maxGold = GameProcess.GetGameConfig().MaxGold;
            if (GameProcess.GetGameDataManager().UserData.Gold + num > maxGold)
                throw new GameException(GameException.ErrorCode.OverMaxGold);
        }
        else if (itemId == 2)
        {
            int maxCoin = GameProcess.GetGameConfig().MaxCoin;
            if (GameProcess.GetGameDataManager().UserData.Coin + num > maxCoin)
                throw new GameException(GameException.ErrorCode.OverMaxCoin);
        }
    }

    public override void Show()
    {
        base.Show();
        InitInputs();
    }

    public void OnChange()
    {
        if (MobiToggle.value) selectedToggle = 0;
        else if (LootingToggle.value) selectedToggle = 1;
        else if (JjiToggle.value) selectedToggle = 2;
        else if (MeteoraToggle.value) selectedToggle = 3;
    }
}
