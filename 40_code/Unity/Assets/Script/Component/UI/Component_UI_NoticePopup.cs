﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

using Kazoh.Table;

public class Component_UI_NoticePopup : GameComponent
{    
    public UILabel TitleLabel;
    public UILabel CancelBtnLabel;
    public UIScrollView ScrollView;
    public UIGrid Grid;
    public GameObject pfNoticeItem;

    private List<Component_Item_Notice> listNotice;
    private Action closeCallback;

    public override void Init()
    {
        base.Init();

        /* 스크롤 뷰 패널 뎁스 설정 */
        UIPanel panel = transform.GetComponent<UIPanel>();
        if(panel == null) panel = transform.parent.GetComponent<UIPanel>();
        if (panel != null)
        {
            ScrollView.panel.depth = panel.depth + 1;
        }        

        /* UI 텍스트 설정 */
        TitleLabel.text = TableManager.GetString("STR_TITLE_NOTICE");
        CancelBtnLabel.text = TableManager.GetString("STR_UI_CLOSE");

        /* 리스트 설정 */
        listNotice = new List<Component_Item_Notice>();
        for(int i=0; i < 10; ++i)
        {
            GameObject go = NGUITools.AddChild(Grid.gameObject, pfNoticeItem);
            Component_Item_Notice component = go.GetComponent<Component_Item_Notice>();
            if (component != null)
            {
                component.Init("","");
                listNotice.Add(component);
            }
        }
        Grid.Reposition();
    }

    public void OnClick_Close()
    {
        if (gameObject.activeSelf)
        {
            GameProcess.PlaySound(SOUND_EFFECT.CLICK);
            Hide();
            if (closeCallback != null) closeCallback();
        }
    }

    bool isLock;
    public void Show(Action _callback)
    {
        try
        {
            if (isLock) return;
            isLock = true;

            DBManager.GetNotice(delegate (string _data)
            {
                try
                {
                    closeCallback = _callback;
                    UpdateList(_data);
                }
                catch (GameException e)
                {
                    GameProcess.ShowError(e);
                }
                catch (Exception e)
                {
                    GameProcess.ShowError(new GameException(e));
                }
                finally
                {
                    isLock = false;
                }
            });
        }
        catch(GameException e)
        {
            GameProcess.ShowError(e);
        }
        catch(Exception e)
        {
            GameProcess.ShowError(new GameException(e));
        }
    }

    void UpdateList(string _data)
    {
        IList list = null;
        if (!string.IsNullOrEmpty(_data)) list = MiniJSON.Json.Deserialize(_data) as IList;
        if (list == null)
        {
            Hide();
            if (closeCallback != null) closeCallback();
            return;
        }

        int c = 0;
        foreach (IDictionary dict in list)
        {
            if (c == listNotice.Count) break;
            string _title = "";
            string _msg = "";
            if (dict.Contains("TITLE")) _title = Convert.ToString(dict["TITLE"]);
            if (dict.Contains("MSG")) _msg = Convert.ToString(dict["MSG"]);

            listNotice[c].Init(_title, _msg);
            c++;
        }
        for (int i=c; i < listNotice.Count; ++i)
        {
            listNotice[i].Init("", "");
        }

        base.Show();
        Grid.Reposition();
        ScrollView.ResetPosition();
    }
}
