﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

using Kazoh.Table;

public class Component_UI_RankPopup : GameComponent
{
    public Action RollArenaEvent;

    public UILabel TitleLabel;
    public UILabel CancelBtnLabel;
    public UILabel ScoreLabel;
    public UIButton SendButton;
    public UILabel SendBtnLabel;
    public UIButton RollButton;
    public UILabel RollBtnLabel;
    public UIScrollView ScrollView;
    public UIGrid RankGrid;
    public UIGrid ArenaGrid;
    public GameObject pfRankItem;
    public Component_UI_InputName InputUI;

    private List<Component_Item_Rank> listRank;
    private List<Component_Item_Rank> listArena;

    private bool isRolled;

    public override void Init()
    {
        base.Init();

        /* 스크롤 뷰 패널 뎁스 설정 */
        UIPanel panel = transform.parent.GetComponent<UIPanel>();
        if (panel != null)
        {
            ScrollView.panel.depth = panel.depth + 1;
        }

        /* 이니셜 입력 UI 숨김 */
        InputUI.Hide();

        /* UI 텍스트 설정 */
        CancelBtnLabel.text = TableManager.GetString("STR_UI_CLOSE");
        SendBtnLabel.text = TableManager.GetString("STR_UI_SYNC");
        RollBtnLabel.text = TableManager.GetString("STR_UI_NEW_ACCOUNT");
        SendButton.gameObject.SetActive(false);

        /* 랭크 리스트 설정 */
        listRank = new List<Component_Item_Rank>();
        for(int i=0; i < 10; ++i)
        {
            GameObject go = NGUITools.AddChild(RankGrid.gameObject, pfRankItem);
            Component_Item_Rank component = go.GetComponent<Component_Item_Rank>();
            if (component != null)
            {
                component.Init(i, null);
                listRank.Add(component);
            }
        }
        RankGrid.Reposition();

        /* 아레나 리스트 설정 */
        listArena = new List<Component_Item_Rank>();
        for (int i = 0; i < 20; ++i)
        {
            GameObject go = NGUITools.AddChild(ArenaGrid.gameObject, pfRankItem);
            Component_Item_Rank component = go.GetComponent<Component_Item_Rank>();
            if (component != null)
            {
                component.Init(i, null);
                listArena.Add(component);
            }
        }
        ArenaGrid.Reposition();
    }

    public void OnClick_Close()
    {
        if (gameObject.activeSelf)
        {
            GameProcess.PlaySound(SOUND_EFFECT.CLICK);
            Hide();
        }
    }

    public void OnClick_Sync()
    {
        if (isLock) return;
        GameProcess.PlaySound(SOUND_EFFECT.CLICK);

        int id = GameProcess.GetGameDataManager().GetGUID();
        int chaId = GameProcess.GetGameDataManager().GetCurCharacter().Data.SkinId;
        int chaGrade = GameProcess.GetGameDataManager().GetCurCharacter().Data.Grade;
        string name = GameProcess.GetGameDataManager().GetUserInitial();
        int score = GameProcess.GetGameDataManager().GetScore();
        string equip = GameProcess.GetGameDataManager().GetEquipInfoCVSOfCurCha();

        string title = TableManager.GetString("STR_TITLE_NAME");
        string msg = TableManager.GetString("STR_MSG_NAME");
        string text1 = TableManager.GetString("STR_UI_SYNC");
        string text2 = TableManager.GetString("STR_UI_CANCEL");
        InputUI.Show(title, msg, text1, text2, name, delegate (string _newName)
        {
            try
            {
                GameProcess.GetGameDataManager().SetUserInitial(_newName);
                DBManager.UpdateRankData(id, _newName, chaId, chaGrade, score, equip, delegate (string _data)
                {
                    title = TableManager.GetString("STR_TITLE_NOTICE");
                    msg = TableManager.GetString("STR_MSG_SYNCED");
                    text1 = TableManager.GetString("STR_UI_OK");
                    GameProcess.ShowPopup(NoticeType.OK, title, msg, text1, null);

                    SendButton.gameObject.SetActive(false);
                    UpdateList(_data);
                });
            }
            catch (GameException e)
            {
                GameProcess.ShowError(e);
            }
            catch (Exception e)
            {
                GameProcess.ShowError(new GameException(e));
            }
        }, null);

        isLock = false;
    }

    public void OnClick_Roll()
    {
        if (isLock) return;
        GameProcess.PlaySound(SOUND_EFFECT.CLICK);
        if (RollArenaEvent != null) RollArenaEvent();
        isLock = false;
    }

    bool isLock;
    public void Show(GameEnum.Menu _type)
    {
        try
        {
            if (isLock) return;
            isLock = true;

            // 갱신 버튼을 꺼 둔다.
            SendButton.gameObject.SetActive(false);
            RollButton.gameObject.SetActive(false);

            if(_type == GameEnum.Menu.RANK)
            {
                TitleLabel.text = TableManager.GetString("STR_TITLE_RANK");
                DBManager.GetRankData(delegate (string _data)
                {
                    try
                    {
                        UpdateList(_data);
                    }
                    catch (GameException e)
                    {
                        GameProcess.ShowError(e);
                    }
                    catch (Exception e)
                    {
                        GameProcess.ShowError(new GameException(e));
                    }
                });
            }
            else RankGrid.gameObject.SetActive(false);

            if (_type == GameEnum.Menu.ARENA)
            {
                TitleLabel.text = TableManager.GetString("STR_TITLE_ARENA");
                int score = GameProcess.GetArenaManager().Score;
                if(score < 0)
                {
                    GameProcess.GetArenaManager().SyncData(delegate ()
                    {
                        score = GameProcess.GetArenaManager().Score;
                        if(score < 0)
                        {
                            ScoreLabel.text = string.Format("{0:###,##0}", 0);
                            RollButton.gameObject.SetActive(true);
                        }
                        else
                        {
                            ScoreLabel.text = string.Format("{0:###,##0}", score);
                            RollButton.gameObject.SetActive(false);
                        }
                    });
                }
                else
                {
                    ScoreLabel.text = string.Format("{0:###,##0}", score);
                }
                GameProcess.GetArenaManager().SyncRank(delegate ()
                {
                    List<Data_Arena> list = GameProcess.GetArenaManager().GetRankList();
                    UpdateArenaList(list);
                });
            }
            else ArenaGrid.gameObject.SetActive(false);

            isLock = false;

        }
        catch(GameException e)
        {
            GameProcess.ShowError(e);
        }
        catch(Exception e)
        {
            GameProcess.ShowError(new GameException(e));
        }
    }

    void UpdateList(string _data)
    {
        IList list = null;
        if (!string.IsNullOrEmpty(_data)) list = MiniJSON.Json.Deserialize(_data) as IList;

        // 내 기록을 설정한다. 
        int id = GameProcess.GetGameDataManager().GetGUID();
        int chaId = GameProcess.GetGameDataManager().GetCurCharacter().Data.SkinId;
        int chaGrade = GameProcess.GetGameDataManager().GetCurCharacter().Data.Grade;
        string name = GameProcess.GetGameDataManager().GetUserInitial();
        int score = GameProcess.GetGameDataManager().GetScore();
        List<int> equipList = new List<int>();
        Dictionary<int, Slot_Item> dic = GameProcess.GetGameDataManager().GetCurCharacter().Data.EquipSlot;
        foreach (KeyValuePair<int, Slot_Item> item in dic)
        {
            if (item.Value == null || item.Value.IsEmpty) continue;
            equipList.Add(item.Value.ItemId);
            equipList.Add(item.Value.Data.EnchantNum);
        }
        Data_Rank myRank = new Data_Rank(id, chaId, chaGrade, name, score, equipList.ToArray());

        ScoreLabel.text = string.Format("{0:###,##0}", myRank.Record);

        List<Data_Rank> rankList = new List<Data_Rank>();
        if (list != null)
        {
            foreach (IDictionary dict in list)
            {
                rankList.Add(new Data_Rank(dict));
            }
        }

        int preRankIdx = rankList.FindIndex(x => x.Id == myRank.Id);
        int myPreScore = 0;
        if (preRankIdx > -1)
        {
            myPreScore = rankList[preRankIdx].Record;
            rankList[preRankIdx] = myRank;
        }
        else rankList.Add(myRank);

        rankList.Sort((x, y) => y.Record.CompareTo(x.Record));

        int preScore = int.MaxValue;
        int rank = 0;
        for (int i = 0; i < listRank.Count; ++i)
        {
            if (i < rankList.Count)
            {
                if(preScore > rankList[i].Record)
                {
                    preScore = rankList[i].Record;
                    rank++;
                }
                listRank[i].Init(rank, rankList[i]);
                if (myRank.Record > myPreScore) SendButton.gameObject.SetActive(true);
            }
            else listRank[i].Init(rank, null);
        }
        base.Show();
        RankGrid.gameObject.SetActive(true);
        RankGrid.Reposition();
        ScrollView.ResetPosition();
    }

    void UpdateArenaList(List<Data_Arena> list)
    {
        list.Sort((x, y) => y.Record.CompareTo(x.Record));

        for (int i = 0; i < list.Count; ++i)
        {
            listArena[i].Init(list[i]);
        }
        base.Show();
        ArenaGrid.gameObject.SetActive(true);
        ArenaGrid.Reposition();
        ScrollView.ResetPosition();
    }
}
