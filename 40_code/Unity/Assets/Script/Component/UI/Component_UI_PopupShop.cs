﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

using Kazoh.Table;

public class Component_UI_PopupShop : GameComponent
{
    public Action<GameComponent> ClickedEvent;

    public UILabel BtnLabel;
    public UIScrollView ScrollView;
    public UIGrid ExchangeGrid;
    public UIGrid RechargeGrid;
    public UIGrid CashShopGrid;
    public UIGrid SummonGrid;

    public GameObject PrefGoodsItem;
    public GameObject PrefCashItem;
    public GameObject PrefSummonItem;

    private List<Component_Item_Goods> exchangeGoodsList;
    private List<Component_Item_Goods> chargeGoodsList;
    private List<Component_Item_Goods> cashGoodsList;
    private List<Component_Item_Summon> summonList;
    private List<int> activeGoodsIdList;

    public override void Init()
    {
        base.Init();

        /* 활성화된 제품 아이디 설정 */
        activeGoodsIdList = new List<int>();
        DBManager.GetGoodsList(delegate (string _data)
        {
            IList idList = null;
            if (!string.IsNullOrEmpty(_data)) idList = MiniJSON.Json.Deserialize(_data) as IList;
            if (idList != null)
            {
                foreach (int goodsId in idList)
                {
                    activeGoodsIdList.Add(goodsId);
                }
            }
        });

        /* 스크롤 뷰 패널 뎁스 설정 */
        UIPanel panel = transform.parent.GetComponent<UIPanel>();
        if (panel != null) ScrollView.panel.depth = panel.depth + 1;

        /* 버튼 텍스트 설정 */
        BtnLabel.text = TableManager.GetString("STR_UI_CLOSE");

        /* 상품 리스트 획득 */
        List<TableData_GoodsList> list = TableManager.GetGoodsList();
        list.Sort((x, y) => x.OrderNum.CompareTo(y.OrderNum));

        /* 환전 리스트 설정 */
        exchangeGoodsList = new List<Component_Item_Goods>();
        foreach (TableData_GoodsList item in list)
        {
            if (item.Type != TableEnum.GoodsType.CashToCoin && item.Type != TableEnum.GoodsType.CashToGold) continue;
            GameObject go = NGUITools.AddChild(ExchangeGrid.gameObject, PrefGoodsItem);
            Component_Item_Goods component = go.GetComponent<Component_Item_Goods>();
            if (component != null)
            {
                component.Init(item);
                component.ClickedEvent += OnSelect;
                exchangeGoodsList.Add(component);
            }
        }
        ExchangeGrid.Reposition();

        /* 충전 리스트 설정 */
        chargeGoodsList = new List<Component_Item_Goods>();
        foreach (TableData_GoodsList item in list)
        {
            if (item.Type != TableEnum.GoodsType.CoinToKey) continue;
            GameObject go = NGUITools.AddChild(RechargeGrid.gameObject, PrefGoodsItem);
            Component_Item_Goods component = go.GetComponent<Component_Item_Goods>();
            if (component != null)
            {
                component.Init(item);
                component.ClickedEvent += OnSelect;
                chargeGoodsList.Add(component);
            }
        }
        RechargeGrid.Reposition();

        /* 캐쉬구매 리스트 설정 */
        cashGoodsList = new List<Component_Item_Goods>();
        foreach (TableData_GoodsList item in list)
        {
            if (item.Type != TableEnum.GoodsType.Cash) continue;
            GameObject go = NGUITools.AddChild(CashShopGrid.gameObject, PrefCashItem);
            Component_Item_Goods component = go.GetComponent<Component_Item_Goods>();
            if (component != null)
            {
                component.Init(item);
                component.ClickedEvent += OnSelect;
                cashGoodsList.Add(component);
            }
        }
        CashShopGrid.Reposition();

        /* 소환 리스트 설정 */
        summonList = new List<Component_Item_Summon>();
        foreach (TableData_GoodsList item in list)
        {
            if (item.Type != TableEnum.GoodsType.Summon) continue;
            GameObject go = NGUITools.AddChild(SummonGrid.gameObject, PrefSummonItem);
            Component_Item_Summon component = go.GetComponent<Component_Item_Summon>();
            if (component != null)
            {
                component.Init(item);
                component.ClickedEvent += OnSelect;
                summonList.Add(component);
            }
        }
        SummonGrid.Reposition();
    }

    bool isLock;
    public void Show(GameEnum.Menu _type)
    {
        isLock = true;

        Show();
        if (_type != GameEnum.Menu.RECHARGE) ScrollView.ResetPosition();

        if (_type == GameEnum.Menu.EXCHANGE)
        {
            for(int i=0; i < exchangeGoodsList.Count; ++i)
            {
                if (activeGoodsIdList.Contains(exchangeGoodsList[i].Data.Id)) exchangeGoodsList[i].Show();
                else exchangeGoodsList[i].Hide();
            }
            ExchangeGrid.gameObject.SetActive(true);
            ExchangeGrid.Reposition();
        }
        else ExchangeGrid.gameObject.SetActive(false);

        if (_type == GameEnum.Menu.RECHARGE)
        {
            for (int i = 0; i < chargeGoodsList.Count; ++i)
            {
                if (activeGoodsIdList.Contains(chargeGoodsList[i].Data.Id)) chargeGoodsList[i].Show();
                else chargeGoodsList[i].Hide();
            }
            RechargeGrid.gameObject.SetActive(true);
            RechargeGrid.Reposition();
        }
        else RechargeGrid.gameObject.SetActive(false);

        if (_type == GameEnum.Menu.CASHSHOP)
        {
            for(int i=0; i < cashGoodsList.Count; ++i)
            {
                if (activeGoodsIdList.Contains(cashGoodsList[i].Data.Id)) cashGoodsList[i].Show();
                else cashGoodsList[i].Hide();
                //cashGoodsList[i].Show();
            }
            CashShopGrid.gameObject.SetActive(true);
            CashShopGrid.Reposition();
        }
        else CashShopGrid.gameObject.SetActive(false);

        if (_type == GameEnum.Menu.SUMMON)
        {
            for (int i = 0; i < summonList.Count; ++i)
            {
                if (activeGoodsIdList.Contains(summonList[i].Data.Id)) summonList[i].Show();
                else summonList[i].Hide();
                //summonList[i].Show();
            }
            SummonGrid.gameObject.SetActive(true);
            SummonGrid.Reposition();
        }
        else SummonGrid.gameObject.SetActive(false);

        isLock = false;
    }

    void OnSelect(GameComponent item)
    {
        if (ClickedEvent != null) ClickedEvent(item);
    }

    public void OnClick_Close()
    {
        if (isLock) return;
        GameProcess.PlaySound(SOUND_EFFECT.CLICK);
        Hide();
    }
}
