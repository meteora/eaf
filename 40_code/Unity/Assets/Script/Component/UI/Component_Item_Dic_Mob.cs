﻿using UnityEngine;
using System.Collections;
using System;

using Kazoh.Table;

public class Component_Item_Dic_Mob : GameComponent
{
    public UISprite ChaSprite;
    public UILabel NoLabel;
    public UILabel NameLabel;
    public UILabel DropLabel;
    public UILabel SpawnLabel;

    public void Init(int _idx, TableData_DicList _data)
    {
        try
        {
            NoLabel.text = _idx.ToString();
            TableData_Npc npc = TableManager.GetGameData(_data.Data) as TableData_Npc;
            ChaSprite.spriteName = npc.SpriteName + "_02";
            ChaSprite.MakePixelPerfect();
            NameLabel.text = TableManager.GetString(npc.Str);
            DropLabel.text = TableManager.GetString(_data.Info_1);
            SpawnLabel.text = TableManager.GetString(_data.Info_2);

            Show();
        }
        catch
        {
            Hide();
        }
    }
}