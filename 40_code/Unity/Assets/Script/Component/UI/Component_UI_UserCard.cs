﻿using UnityEngine;
using System.Collections;
using System;

using Kazoh.Table;

public class Component_UI_UserCard : GameComponent
{
    public Action EditInicialEvent;

    public UISprite ChaSprite;
    public UISprite GradeSprite;
    public UISprite MemberIconSprite;
    public UILabel MembershipLabel;
    public UILabel NameLabel;
    public UILabel ScoreLabel;
    public UILabel ItemNameLabel;

    public Color[] Colors;
    public UISprite[] ItemIcons;
    public UISprite[] ItemGrades;
    public UILabel[] ItemEnchantNums;

    private string[] itemNames = { "", "", "", "" };

//    public void Init(int _membership, string _name, int _score, int _chaId, int _chaGrade, string _equipStr)
//    {
//        int membership = GameProcess.GetGameDataManager().GetMembership();
//        string name = GameProcess.GetGameDataManager().GetUserInitial();
//        int chaId = GameProcess.GetGameDataManager().GetCurCharacter().ChaId;
//        int chaGrade = GameProcess.GetGameDataManager().GetCurCharacter().Data.Grade;
//        int score = GameProcess.GetGameDataManager().GetScore();
//        string equip = GameProcess.GetGameDataManager().GetEquipInfoCVSOfCurCha();

//        ItemNameLabel.alpha = 0f;
//        Debug.Log(_equipStr);
//        MembershipLabel.text = TableManager.GetString("STR_UI_MEM_GRADE_"+_membership);
//        MembershipLabel.color = Colors[_membership];
//        switch (_membership)
//        {
//            case 1: MemberIconSprite.spriteName = "crown_02"; break;
//            case 2: MemberIconSprite.spriteName = "crown_03"; break;
//            case 3: MemberIconSprite.spriteName = "crown_04"; break;
//            case 4: MemberIconSprite.spriteName = "crown_05"; break;
//            default: MemberIconSprite.spriteName = "crown_01"; break;
//        }

//        NameLabel.text = _name;
//        ScoreLabel.text = string.Format("{0:###,##0}", _score);

//        try
//        {
//            TableData_Character chaData = TableManager.GetGameData(_chaId) as TableData_Character;
//            if (chaData != null)
//            {
//                ChaSprite.spriteName = chaData.IconName;
//                GradeSprite.spriteName = "grade_0" + _chaGrade;
//            }
//            else
//            {
//                ChaSprite.spriteName = "icon_item_6000";
//                GradeSprite.spriteName = "";
//            }
//        }
//        catch
//        {
//            ChaSprite.spriteName = "icon_item_6000";
//            GradeSprite.spriteName = "";
//        }

//        for(int i=0; i < ItemIcons.Length; ++i)
//        {
//            ItemIcons[i].spriteName = "icon_item_0000";
//            ItemGrades[i].alpha = 0f;
//            ItemEnchantNums[i].text = "";
//            itemNames[i] = "";
//        }

//        int[] itemIds = new int[4];
//        int[] enchants = new int[4];

//        for(int i=0; i<itemIds.Length; ++i)
//        {
//            try
//            {
//                TableData_Item equip = TableManager.GetGameData(itemIds[i]) as TableData_Item;
//                if (equip != null)
//                {
//                    ItemIcons[equip.SlotIdx].spriteName = equip.IconName;
//                    ItemGrades[equip.SlotIdx].spriteName = "grade_0" + equip.Grade;
//                    ItemGrades[equip.SlotIdx].alpha = 1f;
//                    ItemEnchantNums[equip.SlotIdx].text = "+" + enchants[i];
//                    itemNames[equip.SlotIdx] = string.Format("+{0} {1}", enchants[i], TableManager.GetString(equip.Str));
//                }
//            }
//            catch
//            {
//#if UNITY_EDITOR || DEBUG_MODE
//                Debug.LogWarning("아이템 데이터 없음. ID:" + itemIds[i]);
//#endif
//            }
//        }

//        Show();
//    }

    public IEnumerator SetUserInfo()
    {
        yield return null;

        int membership = GameProcess.GetGameDataManager().GetMembership();
        string name = GameProcess.GetGameDataManager().GetUserInitial();
        int chaId = GameProcess.GetGameDataManager().GetCurCharacter().ChaId;
        int chaGrade = GameProcess.GetGameDataManager().GetCurCharacter().Data.Grade;
        int score = GameProcess.GetGameDataManager().GetScore();
        string equipStr = GameProcess.GetGameDataManager().GetEquipInfoCVSOfCurCha();

        ItemNameLabel.alpha = 0f;
        Debug.Log(equipStr);
        MembershipLabel.text = TableManager.GetString("STR_UI_MEM_GRADE_" + membership);
        MembershipLabel.color = Colors[membership];
        switch (membership)
        {
            case 1: MemberIconSprite.spriteName = "card_02"; break;
            case 2: MemberIconSprite.spriteName = "card_03"; break;
            case 3: MemberIconSprite.spriteName = "card_04"; break;
            case 4: MemberIconSprite.spriteName = "card_05"; break;
            default: MemberIconSprite.spriteName = "card_01"; break;
        }

        NameLabel.text = name;
        ScoreLabel.text = string.Format("{0:###,##0}", score);

        try
        {
            TableData_Character chaData = TableManager.GetGameData(chaId) as TableData_Character;
            if (chaData != null)
            {
                ChaSprite.spriteName = chaData.IconName;
                GradeSprite.spriteName = "grade_0" + chaGrade;
            }
            else
            {
                ChaSprite.spriteName = "icon_item_6000";
                GradeSprite.spriteName = "";
            }
        }
        catch
        {
            ChaSprite.spriteName = "icon_item_6000";
            GradeSprite.spriteName = "";
        }

        for (int i = 0; i < ItemIcons.Length; ++i)
        {
            ItemIcons[i].spriteName = "icon_item_0000";
            ItemGrades[i].alpha = 0f;
            ItemEnchantNums[i].text = "";
            itemNames[i] = "";
        }

        int[] itemIds = new int[4];
        int[] enchants = new int[4];

        IList list = MiniJSON.Json.Deserialize(equipStr) as IList;
        if (list != null)
        {
            int c = 0;
            foreach (string csv in list)
            {
                string[] arr = csv.Split(',');
                if (arr.Length == 2)
                {                    
                    itemIds[c] = System.Convert.ToInt32(arr[0]);
                    enchants[c] = System.Convert.ToInt32(arr[1]);
                    c++;
                }
            }
        }

        for (int i = 0; i < itemIds.Length; ++i)
        {
            try
            {
                if (itemIds[i] == 0) continue;
                TableData_Item equip = TableManager.GetGameData(itemIds[i]) as TableData_Item;
                if (equip != null)
                {
                    ItemIcons[equip.SlotIdx].spriteName = equip.IconName;
                    ItemGrades[equip.SlotIdx].spriteName = "grade_0" + equip.Grade;
                    ItemGrades[equip.SlotIdx].alpha = 1f;
                    ItemEnchantNums[equip.SlotIdx].text = "+" + enchants[i];
                    itemNames[equip.SlotIdx] = string.Format("+{0} {1}", enchants[i], TableManager.GetString(equip.Str));
                }
            }
            catch
            {
#if UNITY_EDITOR || DEBUG_MODE
                Debug.LogWarning("아이템 데이터 없음. ID:" + itemIds[i]);
#endif
            }
        }
    }

    void OnClick_Item_01()
    {
        ShowItemName(0);
    }
    void OnClick_Item_02()
    {
        ShowItemName(1);
    }
    void OnClick_Item_03()
    {
        ShowItemName(2);
    }
    void OnClick_Item_04()
    {
        ShowItemName(3);
    }

    void OnClick_Edit()
    {
        GameProcess.PlaySound(SOUND_EFFECT.CLICK);
        if (EditInicialEvent != null) EditInicialEvent();
    }

    void ShowItemName(int _idx)
    {
        if (!string.IsNullOrEmpty(itemNames[_idx]))
        {
            StopAllCoroutines();
            StartCoroutine(AnimItemName(itemNames[_idx]));
        }
    }

    IEnumerator AnimItemName(string _name)
    {
        float alpha = 2f;
        ItemNameLabel.text = _name;
        ItemNameLabel.alpha = 1f;
        while (alpha > 0)
        {
            yield return null;
            alpha -= Time.deltaTime;
            ItemNameLabel.alpha = Mathf.Min(alpha, 1f);
        }
        ItemNameLabel.alpha = 0f;
    }
}