﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

using Kazoh.Table;

public class Component_UI_SettingPopup : GameComponent
{
    public Action SavedEvent;

    public UILabel UserInfoTitleLabel;
    public UIInput CurPwInput;
    public UIInput NewPwInput;
    public UIInput ConformPwInput;
    public UIInput EmailInput;
    public UILabel CurPwLabel;
    public UILabel NewPwLabel;
    public UILabel ConformPwLabel;
    public UILabel ChangeBtnLabel;
    public UILabel ChangeEmailBtnLabel;
    public UILabel[] GuideLabels;

    public UILabel SettingTitleLabel;
    public UILabel BgmLabel;
    public UILabel EffectLabel;
    public UILabel SaveBtnLabel;
    public UILabel CancelBtnLabel;

    public UISlider BgmSlider;
    public UISlider EffectSlider;
    public Component_UI_UserCard UserInfoUI;
    public Component_UI_InputName InputUI;

    private bool isLock;

    void Awake()
    {
        if (PlayerPrefs.HasKey("BGM_VOLUME")) BgmSlider.value = PlayerPrefs.GetFloat("BGM_VOLUME");
        if (PlayerPrefs.HasKey("EFFECT_VOLUME")) EffectSlider.value = PlayerPrefs.GetFloat("EFFECT_VOLUME");
    }

    public override void Init()
    {
        base.Init();
        InputUI.Hide();
        UserInfoUI.EditInicialEvent += ShowEditInicial;
        SetConstantUI();
    }

    public override void Show()
    {
        SetSettingPopup();
        if (EncryptedPlayerPrefs.HasKey("email"))
        {
            string email = EncryptedPlayerPrefs.GetString("email");
            EmailInput.value = email;
        }

        base.Show();
        StartCoroutine(UserInfoUI.SetUserInfo());
    }

    public void SetVolumnBgm()
    {
        if (UIProgressBar.current != null) GameProcess.Instance.SetVolumnBGM(UIProgressBar.current.value);
    }

    public void SetVolumnEffect()
    {
        if (UIProgressBar.current != null) GameProcess.Instance.SetVolumnEffect(UIProgressBar.current.value);
    }

    public void OnClick_SaveSetting()
    {
        if (!gameObject.activeSelf) return;

        GameProcess.PlaySound(SOUND_EFFECT.CLICK);
        GameSettingManager gsManager = GameProcess.GetSettingManager();
        if(gsManager != null)
        {
            float bgmVol = BgmSlider.value;
            float effectVol = EffectSlider.value;

            gsManager.SetGameSetting(bgmVol, effectVol);
        }

        OnSave();
        Hide();
    }

    public void OnClick_Close()
    {
        if (gameObject.activeSelf)
        {
            Hide();
            GameProcess.PlaySound(SOUND_EFFECT.CLICK);

            if (PlayerPrefs.HasKey("BGM_VOLUME")) GameProcess.Instance.SetVolumnBGM(PlayerPrefs.GetFloat("BGM_VOLUME"));
            if (PlayerPrefs.HasKey("EFFECT_VOLUME")) GameProcess.Instance.SetVolumnEffect(PlayerPrefs.GetFloat("EFFECT_VOLUME"));
        }
    }

    public void OnClick_ChangePw()
    {
        try
        {
            if (isLock) return;
            isLock = true;
            GameProcess.PlaySound(SOUND_EFFECT.CLICK);
            string curPw = CurPwInput.value;
            string newPw = NewPwInput.value;
            if (string.IsNullOrEmpty(curPw)) throw new GameException(GameException.ErrorCode.EmptyCurPw);
            if (string.IsNullOrEmpty(newPw)) throw new GameException(GameException.ErrorCode.EmptyNewPw);
            if (string.IsNullOrEmpty(ConformPwInput.value)) throw new GameException(GameException.ErrorCode.EmptyConformPw);
            if (curPw.Equals(newPw)) throw new GameException(GameException.ErrorCode.EqualsNewPw);
            if (!newPw.Equals(ConformPwInput.value)) throw new GameException(GameException.ErrorCode.NotEqualsNewPw);
            if (!CheckPw(newPw)) throw new GameException(GameException.ErrorCode.InvalidPwFormat);

            string title = TableManager.GetString("STR_TITLE_CHANGE_PW");
            string msg = TableManager.GetString("STR_MSG_CHANGE_PW");
            string text1 = TableManager.GetString("STR_UI_YES");
            string text2 = TableManager.GetString("STR_UI_NO");
            GameProcess.ShowPopup(NoticeType.YES_NO, title, msg, text1, text2, delegate()
            {
                int id = GameProcess.GetGameDataManager().GetGUID();
                DBManager.ChangePw(id, curPw, newPw, delegate()
                {
                    EncryptedPlayerPrefs.SetString(EncryptedPlayerPrefs.userKeys[1], newPw);
                    title = TableManager.GetString("STR_TITLE_CHANGE_PW");
                    msg = TableManager.GetString("STR_MSG_CHANGED_PW");
                    text1 = TableManager.GetString("STR_UI_OK");
                    GameProcess.ShowPopup(NoticeType.OK, title, msg, text1, null);
                    SetSettingPopup();
                });
            }, null);            
        }
        catch (GameException e)
        {
            GameProcess.ShowError(e);
        }
        catch (Exception e)
        {
#if UNITY_EDITOR
            Debug.LogError(e);
#endif
            GameProcess.ShowError(new GameException(e));
        }
        finally
        {
            isLock = false;
        }

    }

    public void OnClick_ChangeEmail()
    {
        try
        {
            if (isLock) return;
            isLock = true;
            GameProcess.PlaySound(SOUND_EFFECT.CLICK);
            string email = EmailInput.value;
            if (string.IsNullOrEmpty(email)) throw new GameException(GameException.ErrorCode.NoEmail);
            if (!CheckEmail(email)) throw new GameException(GameException.ErrorCode.InvalidEmailFormat);

            EncryptedPlayerPrefs.SetString("email", email);

            string title = TableManager.GetString("STR_TITLE_NOTICE");
            string msg = TableManager.GetString("STR_MSG_CHANGED_EMAIL");
            string text = TableManager.GetString("STR_UI_OK");
            GameProcess.ShowPopup(NoticeType.OK, title, msg, text, null);
        }
        catch (GameException e)
        {
            GameProcess.ShowError(e);
        }
        catch (Exception e)
        {
#if UNITY_EDITOR
            Debug.LogError(e);
#endif
            GameProcess.ShowError(new GameException(e));
        }
        finally
        {
            isLock = false;
        }

    }

    void SetConstantUI()
    {
        UserInfoTitleLabel.text = TableManager.GetString("STR_TITLE_USER_INFO");
        ChangeBtnLabel.text = TableManager.GetString("STR_UI_CHANGE");
        ChangeEmailBtnLabel.text = TableManager.GetString("STR_UI_CHANGE");

        SettingTitleLabel.text = TableManager.GetString("STR_TITLE_SETTING");
        BgmLabel.text = TableManager.GetString("STR_UI_BGM");
        EffectLabel.text = TableManager.GetString("STR_UI_EFFECT");
        SaveBtnLabel.text = TableManager.GetString("STR_UI_SAVE");
        CancelBtnLabel.text = TableManager.GetString("STR_UI_CLOSE");

        CurPwLabel.text = TableManager.GetString("STR_UI_CUR_PW");
        NewPwLabel.text = TableManager.GetString("STR_UI_NEW_PW");
        ConformPwLabel.text = TableManager.GetString("STR_UI_CONFORM_PW");
    }

    void SetSettingPopup()
    {
        GameSettingManager gsManager = GameProcess.GetSettingManager();
        if (gsManager != null)
        {
            BgmSlider.value = gsManager.BgmVolume;
            EffectSlider.value = gsManager.EffectVolume;
        }
        else
        {
#if UNITY_EDITOR || DEBUG_MODE
            Debug.LogWarning("GameSettingManager가 생성되지 않아 초기 값으로 설정합니다.");
#endif
            BgmSlider.value = 1;
            EffectSlider.value = 1;
        }        

        CurPwInput.value = "";
        NewPwInput.value = "";
        ConformPwInput.value = "";

        int membership = GameProcess.GetGameDataManager().GetMembership();
        if (membership < GameProcess.GetGameConfig().VipGradeArray.Length - 1)
        {
            int rest = GameProcess.GetGameConfig().VipGradeArray[membership] - GameProcess.GetGameDataManager().UserData.PurchaseAmount;
            GuideLabels[0].text = string.Format(TableManager.GetString("STR_UI_MEM_GUIDE_1" + membership), rest);
        }
        else
        {
            GuideLabels[0].text = TableManager.GetString("STR_UI_MEM_GUIDE_1" + membership);
        }
        GuideLabels[1].text = TableManager.GetString("STR_UI_MEM_GUIDE_2" + membership);
        GuideLabels[2].text = TableManager.GetString("STR_UI_MEM_GUIDE_3" + membership);
    }

    void OnSave()
    {
        if (SavedEvent != null) SavedEvent();
    }

    bool CheckPw(string _pw)
    {
        if (_pw.Length < 6) return false;
        char[] cArray = _pw.ToCharArray();
        for (int i = 0; i < cArray.Length; ++i)
        {
            if ((cArray[i] >= '0' && cArray[i] <= '9') || (cArray[i] >= 'A' && cArray[i] <= 'z')) continue;
            return false;
        }

        return true;
    }

    bool CheckEmail(string _email)
    {
        int idx_1 = _email.IndexOf("@");
        int idx_2 = _email.IndexOf(".", idx_1);

        if (idx_1 > 0 && idx_2 > 0) return true;
        return false;
    }

    void ShowEditInicial()
    {
        string name = GameProcess.GetGameDataManager().GetUserInitial();

        string title = TableManager.GetString("STR_TITLE_NAME");
        string msg = TableManager.GetString("STR_MSG_NAME");
        string text1 = TableManager.GetString("STR_UI_OK");
        string text2 = TableManager.GetString("STR_UI_CANCEL");
        InputUI.Show(title, msg, text1, text2, name, delegate (string _newName)
        {
            try
            {
                GameProcess.GetGameDataManager().SetUserInitial(_newName);
                GameProcess.GetGameDataManager().Save();
                StartCoroutine(UserInfoUI.SetUserInfo());
            }
            catch (GameException e)
            {
                GameProcess.ShowError(e);
            }
            catch (Exception e)
            {
                GameProcess.ShowError(new GameException(e));
            }
        }, null);
    }
}
