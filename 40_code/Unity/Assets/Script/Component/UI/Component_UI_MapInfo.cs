﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

using Kazoh.Table;

public class Component_UI_MapInfo : GameComponent
{
    public Action<int> ClickedEvent;
    public Action<int> ClickedEvent2;
    public Action ClickedEvent3;
    public Action<GameEnum.Menu> ClickRankEvent;
    public Action RollArenaEvent;

    public UITexture MapImg;
    public UILabel GradeLabel;
    public UISprite LevelSprite;
    public UILabel TitleLabel;
    public UILabel DescLabel;
    public UISprite UnlockSprite;
    public UIButton StartBtn;
    public UILabel StartLabel;
    public UIButton NextBtn;
    public UILabel NextLabel;
    public UIButton PrevBtn;
    public UILabel PrevLabel;
    public UITexture BgTexture;
    public UISprite BgSprite;
    public UIToggle Checkbox;
    public UILabel CheckboxLabel;
    public UISprite ClearSprite;
    public UISprite ExpSprite;
    public UISprite RewardSprite;
    public UILabel LvLabel;
    public UISprite PvpInfoSprite;
    public UISprite ChaSprite;
    public UISprite GradeSprite;
    public UILabel NameLabel;
    public UILabel ScoreLabel;
    public UIButton RankBtn;
    public UISprite RaidSprite;
    public int NextMapId { get { return nextMapId; } }

    private bool isLock;
    private int mapId;
    private bool isArena;
    private int nextMapId;
    private int prevMapId;

    public override void Init()
    {
        base.Init();

        /* 배경 설정 */
#if UNITY_EDITOR
        SystemLanguage lang = GameProcess.Instance.Language;
#else
        SystemLanguage lang = Application.systemLanguage;
#endif
        switch (lang)
        {
            case SystemLanguage.Korean: BgSprite.spriteName = "title_ko"; break;
            case SystemLanguage.Japanese: BgSprite.spriteName = "title_ja"; break;
            default: BgSprite.spriteName = "title_en"; break;
        }
    }

    public void Show(TableData_MapList _data)
    {
        GameProcess.StopBGM();

        if (_data == null)
        {
            BgTexture.alpha = 1f;
            return;
        }
        TableData_Map map = TableManager.GetGameData(_data.MapId) as TableData_Map;
        if(map == null)
        {
            BgTexture.alpha = 1f;
            return;
        }

        mapId = map.Id;
        isArena = map.Type == TableEnum.MapType.Arena;
        nextMapId = _data.NextMapId;
        prevMapId = _data.PrevMapId;

        MapImg.mainTexture = GameProcess.GetResourceManager().GetMapImg(map.ImageName);

        GradeLabel.text = string.Format("{0} : {1}", TableManager.GetString("STR_UI_GRADE_LIMITE"),GetGradeStr(map.Grade));
        if (map.Level > 0)
        {
            LevelSprite.spriteName = string.Format("icon_grade_{0:00}", map.Level);
            LevelSprite.alpha = 1f;
        }        
        else LevelSprite.alpha = 0f;
        int clearGrade = GameProcess.GetGameDataManager().GetMapRecord(mapId);
        switch(clearGrade)
        {
            case 1: ClearSprite.spriteName = "crown_01"; ClearSprite.alpha = 1f; break;
            case 2: ClearSprite.spriteName = "crown_02"; ClearSprite.alpha = 1f; break;
            case 3: ClearSprite.spriteName = "crown_03"; ClearSprite.alpha = 1f; break;
            default: ClearSprite.alpha = 0f; break;
        }
        TitleLabel.text = TableManager.GetString(map.Str);
        StartLabel.text = TableManager.GetString("STR_UI_START");
        NextLabel.text = "";
        PrevLabel.text = "";
        CheckboxLabel.text = TableManager.GetString("STR_UI_AUTO");
        isLock = GameProcess.GetGameDataManager().IsLockMap(_data);
        if (isLock)
        {
            UnlockSprite.alpha = 0.5f;
            string desc = "";
            for (int i = 0; i < _data.UnlockCondList.Count; ++i)
            {
                if (_data.UnlockCondList[i].Cond == TableEnum.UnlockType.None) break;
                switch (_data.UnlockCondList[i].Cond)
                {
                    case TableEnum.UnlockType.BCrownMapId:
                    case TableEnum.UnlockType.SCrownMapId:
                    case TableEnum.UnlockType.GCrownMapId:
                        desc += MakeUnlockCondDesc(_data.UnlockCondList[i]) + "\n";
                        break;
                    case TableEnum.UnlockType.BCrownNum:
                    case TableEnum.UnlockType.SCrownNum:
                    case TableEnum.UnlockType.GCrownNum:
                        desc += MakeUnlockCondDesc2(_data.UnlockCondList[i]);
                        break;
                    default:
                        break;
                }
            }
            DescLabel.text = desc;
        }
        else
        {
            DescLabel.text = TableManager.GetString(map.Desc);
            UnlockSprite.alpha = 0f;
        }

        if (map.RegionId == GameProcess.GetRaidManager().CurRegion())
        {
            RaidSprite.alpha = 1f;
        }
        else RaidSprite.alpha = 0f;

        if (nextMapId > 0) NextBtn.gameObject.SetActive(true);
        else NextBtn.gameObject.SetActive(false);

        if (prevMapId > 0) PrevBtn.gameObject.SetActive(true);
        else PrevBtn.gameObject.SetActive(false);

        if (!string.IsNullOrEmpty(map.Bgm))
        {
            GameProcess.PlaySound(SOUND_EFFECT.BGM, map.Bgm);
        }
        BgTexture.alpha = 0f;

        ExpSprite.fillAmount = GameProcess.GetGameDataManager().CurExpPer;
        if (GameProcess.GetGameDataManager().ExpRewardNum > 0) RewardSprite.alpha = 1f;
        else RewardSprite.alpha = 0f;

        if(GameProcess.GetGameDataManager().CurLevel < 999) LvLabel.text = "Lv. " + (GameProcess.GetGameDataManager().CurLevel + 1);
        else LvLabel.text = "Lv. Max";

        if(isArena)
        {
            DescLabel.alpha = 0f;
            PvpInfoSprite.alpha = 1f;
            RankBtn.normalSprite = "menu_26";
            if (GameProcess.GetArenaManager().Count < 2 || GameProcess.GetArenaManager().Count < map.PlayerNum)
            {
                GameProcess.GetArenaManager().SyncData(delegate ()
                {
                    Data_Arena arena = GameProcess.GetArenaManager().CurData;
                    SetPvpInfo(arena);
                });
            }
            else
            {
                Data_Arena arena = GameProcess.GetArenaManager().CurData;
                SetPvpInfo(arena);
            }
        }
        else
        {
            DescLabel.alpha = 1f;
            PvpInfoSprite.alpha = 0f;
            RankBtn.normalSprite = "menu_12";
        }

        transform.localPosition = Vector3.zero;
        base.Show();
    }
    
    public override void Hide()
    {
        base.Hide();
        transform.localPosition = hidePos;
    }

    string MakeUnlockCondDesc(TableData_MapList.UnlockCond cond)
    {
        TableData_Map condMap = TableManager.GetGameData(cond.Value) as TableData_Map;
        if (condMap == null) return string.Empty;
        string format = TableManager.GetString("STR_UI_OPEN_COND");
        switch (cond.Cond)
        {
            case TableEnum.UnlockType.BCrownMapId:
                return string.Format(format, TableManager.GetString(condMap.Str), TableManager.GetString("STR_CROWN_1"));
            case TableEnum.UnlockType.SCrownMapId:
                return string.Format(format, TableManager.GetString(condMap.Str), TableManager.GetString("STR_CROWN_2"));
            case TableEnum.UnlockType.GCrownMapId:
                return string.Format(format, TableManager.GetString(condMap.Str), TableManager.GetString("STR_CROWN_3"));
        }
        return string.Empty;
    }

    string MakeUnlockCondDesc2(TableData_MapList.UnlockCond cond)
    {
        string format = "{0} {1}/{2}";
        switch (cond.Cond)
        {
            case TableEnum.UnlockType.BCrownNum:
                return string.Format(format, TableManager.GetString("STR_CROWN_1"), GameProcess.GetGameDataManager().GetMapNum(1), cond.Value);
            case TableEnum.UnlockType.SCrownNum:
                return string.Format(format, TableManager.GetString("STR_CROWN_2"), GameProcess.GetGameDataManager().GetMapNum(2), cond.Value);
            case TableEnum.UnlockType.GCrownNum:
                return string.Format(format, TableManager.GetString("STR_CROWN_3"), GameProcess.GetGameDataManager().GetMapNum(3), cond.Value);
        }
        return string.Empty;
    }

    void SetPvpInfo(Data_Arena _data)
    {
        try
        {
            if (_data == null)
            {
                BgTexture.alpha = 1f;
                string title = TableManager.GetString("STR_TITLE_NOTICE");
                string msg = TableManager.GetString("STR_MSG_ARENA");
                string text1 = TableManager.GetString("STR_UI_YES");
                string text2 = TableManager.GetString("STR_UI_NO");
                GameProcess.ShowPopup(NoticeType.YES_NO, title, msg, text1, text2, delegate ()
                {
                    if(RollArenaEvent != null) RollArenaEvent();
                }, null);
            }
            else
            {
                GradeSprite.spriteName = "grade_0" + _data.Grade;
                NameLabel.text = _data.Name;
                int bp = _data.Ability.SAtk + _data.Ability.LAtk + _data.Ability.Def * 2;
                Data_UserCharacter my = GameProcess.GetGameDataManager().GetCurCharacter().Data;
                if (my == null) NameLabel.color = Color.white;
                else
                {
                    float x = Mathf.Clamp((bp - my.SAtk - my.LAtk - my.Def * 2) * 0.05f, -1.0f, 1.0f);
                    if (x > 0) NameLabel.color = new Color(1, 1 - x, 1 - x, 1);
                    else NameLabel.color = new Color(1 + x, 1 + x, 1, 1);
                }
                ScoreLabel.text = string.Format("{0:###,##0}", _data.Record);
                TableData_Character cha = TableManager.GetGameData(_data.ChaId) as TableData_Character;
                ChaSprite.spriteName = cha.IconName;
            }
        }
        catch(GameException e)
        {
            BgTexture.alpha = 1f;
            GameProcess.ShowError(e);
        }
        catch (Exception e)
        {
            BgTexture.alpha = 1f;
            GameProcess.ShowError(new GameException(e));
        }
    }

    bool isButtonLock;
    public void OnClick_Start()
    {
        if (isLock || isButtonLock) return;
        isButtonLock = true;
        LayoutManager.Lock();
        GameProcess.PlaySound(SOUND_EFFECT.CLICK);
        if (ClickedEvent != null) ClickedEvent(mapId);
        isButtonLock = false;
    }

    public void OnClick_Next()
    {
        if (isButtonLock) return;
        isButtonLock = true;
        GameProcess.PlaySound(SOUND_EFFECT.CLICK);
        if (ClickedEvent2 != null) ClickedEvent2(nextMapId);
        isButtonLock = false;
    }

    public void OnClick_Prev()
    {
        if (isButtonLock) return;
        isButtonLock = true;
        GameProcess.PlaySound(SOUND_EFFECT.CLICK);
        if (ClickedEvent2 != null) ClickedEvent2(prevMapId);
        isButtonLock = false;
    }

    public void OnClick_Rank()
    {
        GameProcess.PlaySound(SOUND_EFFECT.CLICK);
        if (ClickRankEvent != null)
        {
            if(isArena) ClickRankEvent(GameEnum.Menu.ARENA);
            else ClickRankEvent(GameEnum.Menu.RANK);
        }
    }

    public void OnClick_Reward()
    {
        try
        {
            if (GameProcess.GetGameDataManager().ExpRewardNum > 0)
            {
                GameProcess.PlaySound(SOUND_EFFECT.COIN);
                GameProcess.GetGameDataManager().GetExpReward();
                if (GameProcess.GetGameDataManager().ExpRewardNum > 0) RewardSprite.alpha = 1f;
                else RewardSprite.alpha = 0f;
            }
        }
        catch (GameException e)
        {
            GameProcess.ShowError(e);
        }
        catch (Exception e)
        {
            GameProcess.ShowError(new GameException(e));
        }
    }

    public void OnClick_PvpNext()
    {
        Data_Arena data = GameProcess.GetArenaManager().Next();
        if(data == null)
        {
            GameProcess.GetArenaManager().SyncData(delegate ()
            {
                data = GameProcess.GetArenaManager().Next();
                SetPvpInfo(data);
            });
        }
        else SetPvpInfo(data);
    }

    public void OnClick_PvpPrev()
    {
        Data_Arena data = GameProcess.GetArenaManager().Prev();
        if (data == null)
        {
            GameProcess.GetArenaManager().SyncData(delegate ()
            {
                data = GameProcess.GetArenaManager().Prev();
                SetPvpInfo(data);
            });
        }
        else SetPvpInfo(data);
    }

    public void OnClick_Raid()
    {
        if (isButtonLock) return;
        isButtonLock = true;
        GameProcess.PlaySound(SOUND_EFFECT.CLICK);
        if (ClickedEvent3 != null) ClickedEvent3();
        isButtonLock = false;
    }

    string GetGradeStr(int _grade)
    {
        if (_grade == 1) return "[bbbbbb]D[-]";
        if (_grade == 2) return "[3AFB16]C[-]";
        if (_grade == 3) return "[1692FB]B[-]";
        if (_grade == 4) return "[C816FB]A[-]";
        if (_grade == 5) return "[FB162D]S[-]";
        if (_grade == 6) return "[FBA916]SS[-]";
        return TableManager.GetString("STR_UI_NO_LIMITE");
    }

}
