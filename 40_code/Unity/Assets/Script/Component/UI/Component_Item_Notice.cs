﻿using UnityEngine;
using System.Collections;
using System;

using Kazoh.Table;

public class Component_Item_Notice : GameComponent
{
    public UILabel TitleLabel;
    public UILabel MsgLabel;

    public void Init(string _title, string _msg)
    {
        if(string.IsNullOrEmpty(_title))
        {
            Hide();
            return;
        }

        TitleLabel.text = _title;
        MsgLabel.text = _msg;

        Show();
    }
}