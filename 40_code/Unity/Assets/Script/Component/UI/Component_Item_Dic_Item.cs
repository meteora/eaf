﻿using UnityEngine;
using System.Collections;
using System;

using Kazoh.Table;

public class Component_Item_Dic_Item : GameComponent
{
    public UISprite IconSprite;
    public UISprite GradeSprite;
    public UILabel NoLabel;
    public UILabel NameLabel;
    public UILabel DescLabel;
    public UILabel InfoLabel;

    public UILabel[] Params;
    public UILabel[] ParamValues;

    public void Init(int _idx, TableData_DicList _data)
    {
        try
        {
            NoLabel.text = _idx.ToString();
            TableData_Item item = TableManager.GetGameData(_data.Data) as TableData_Item;
            IconSprite.spriteName = item.IconName;
            GradeSprite.spriteName = "grade_0" + item.Grade;
            NameLabel.text = TableManager.GetString(item.Str);
            DescLabel.text = TableManager.GetString(item.Desc);
            InfoLabel.text = TableManager.GetString(_data.Info_1);

            Params[0].text = TableManager.GetString("STR_UI_PARAM_01");
            Params[1].text = TableManager.GetString("STR_UI_PARAM_02");
            Params[2].text = TableManager.GetString("STR_UI_PARAM_03");
            Params[3].text = TableManager.GetString("STR_UI_PARAM_04");
            Params[4].text = TableManager.GetString("STR_UI_PARAM_05");
            Params[5].text = TableManager.GetString("STR_UI_SKILL");

            ParamValues[0].text = string.Format("{0} / {1}", item.SAtk, item.LAtk);
            if (item.SlotIdx == 1) ParamValues[1].text = string.Format("{0:0.00}", item.ASpd * 0.001f);
            else ParamValues[1].text = string.Format("{0}", item.ASpd);
            ParamValues[2].text = item.Def.ToString();
            ParamValues[3].text = item.Spd.ToString();
            ParamValues[4].text = item.Hp.ToString();
            ParamValues[5].text = item.OptionList.Count > 0 ? "O" : "X";

            Show();
        }
        catch
        {
            Hide();
        }
    }
}