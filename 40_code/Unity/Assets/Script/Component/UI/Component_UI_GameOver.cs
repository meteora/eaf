﻿using UnityEngine;
using System.Collections;
using System;

using Kazoh.Table;

public class Component_UI_GameOver : GameComponent
{
    public Action<bool> ClickedEvent;

    public UILabel GameOverLabel;
    public UILabel RetryLabel;

    private Animation anim;
    private WaitForSeconds delay;

    public override void Init()
    {
        anim = GetComponent<Animation>();
        GameOverLabel.text = TableManager.GetString("STR_TITLE_GAMEOVER");
        RetryLabel.text = TableManager.GetString("STR_UI_RETRY");
        delay = new WaitForSeconds(GameProcess.GetEffectLength(SOUND_EFFECT.GAMEOVER));
        base.Init();
    }

    public override void Show()
    {
        RetryLabel.gameObject.SetActive(false);
        transform.localPosition = Vector3.zero;
        base.Show();
        isLock = true;
        StartCoroutine(PlayAnim());
    }

    public override void Hide()
    {
        base.Hide();
        transform.localPosition = hidePos;
    }

    bool isLock = false;
    void OnClick()
    {
        if (anim.isPlaying || isLock) return;
        GameProcess.PlaySound(SOUND_EFFECT.CLICK);
        Hide();
        if (ClickedEvent != null) ClickedEvent(false);
    }

    IEnumerator PlayAnim()
    {
        yield return null;

        GameProcess.PlaySound(SOUND_EFFECT.GAMEOVER);
        if (anim != null)
        {
            anim.Play();
        }

        yield return delay;
        RetryLabel.gameObject.SetActive(true);
        isLock = false;
    }
}
