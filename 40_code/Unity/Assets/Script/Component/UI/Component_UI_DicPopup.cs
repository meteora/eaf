﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

using Kazoh.Table;

public class Component_UI_DicPopup : GameComponent
{
    public UILabel TitleLabel;
    public UILabel CancelBtnLabel;
    public UIScrollView ScrollView;
    public UIGrid Grid_1;
    public UIGrid Grid_2;
    public UIGrid Grid_3;
    public UIToggle Tab_1;
    public UIToggle Tab_2;
    public UIToggle Tab_3;
    public UILabel ChaLabel;
    public UILabel MobLabel;
    public UILabel ItemLabel;
    public GameObject pfChaItem;
    public GameObject pfMobItem;
    public GameObject pfBossItem;
    public GameObject pfItemItem;

    private List<Component_Item_Dic_Cha> listCha;
    private List<Component_Item_Dic_Mob> listMob;
    private List<Component_Item_Dic_Item> listItem;

    private bool isReady;

    public override void Init()
    {
        base.Init();

        /* 스크롤 뷰 패널 뎁스 설정 */
        UIPanel panel = transform.GetComponent<UIPanel>();
        if(panel == null) panel = transform.parent.GetComponent<UIPanel>();
        if (panel != null) ScrollView.panel.depth = panel.depth + 1;

        /* 사전 리스트 설정 */
        listCha = new List<Component_Item_Dic_Cha>();
        listMob = new List<Component_Item_Dic_Mob>();
        listItem = new List<Component_Item_Dic_Item>();

        List<TableData_DicList> list = TableManager.GetDicList();
        List<TableData_DicList> chaDatas = new List<TableData_DicList>();
        List<TableData_DicList> mobDatas = new List<TableData_DicList>();
        List<TableData_DicList> itemDatas = new List<TableData_DicList>();

        foreach (TableData_DicList item in list)
        {
            switch (item.Type)
            {
                case TableEnum.DicType.Cha: chaDatas.Add(item); break;
                case TableEnum.DicType.Mob: 
                case TableEnum.DicType.Boss: mobDatas.Add(item); break;
                case TableEnum.DicType.Item: itemDatas.Add(item); break;
            }
        }
        
        foreach(TableData_DicList item in chaDatas)
        {
            GameObject go = NGUITools.AddChild(Grid_1.gameObject, pfChaItem);
            Component_Item_Dic_Cha component = go.GetComponent<Component_Item_Dic_Cha>();
            if (component != null)
            {
                component.Init(listCha.Count + 1, item);
                listCha.Add(component);
            }
        }

        foreach(TableData_DicList item in mobDatas)
        {
            GameObject prefab = item.Type == TableEnum.DicType.Mob ? pfMobItem : pfBossItem;
            GameObject go = NGUITools.AddChild(Grid_2.gameObject, prefab);
            Component_Item_Dic_Mob component = go.GetComponent<Component_Item_Dic_Mob>();
            if (component != null)
            {
                component.Init(listMob.Count + 1, item);
                listMob.Add(component);
            }
        }

        itemDatas.Sort((x, y) => x.Data.CompareTo(y.Data));
        foreach(TableData_DicList item in itemDatas)
        {
            GameObject go = NGUITools.AddChild(Grid_3.gameObject, pfItemItem);
            Component_Item_Dic_Item component = go.GetComponent<Component_Item_Dic_Item>();
            if (component != null)
            {
                component.Init(listItem.Count + 1, item);
                listItem.Add(component);
            }
        }

        /* UI 텍스트 설정 */
        TitleLabel.text = TableManager.GetString("STR_MENU_23");
        CancelBtnLabel.text = TableManager.GetString("STR_UI_CLOSE");
        ChaLabel.text = TableManager.GetString("STR_UI_DIC_01");
        MobLabel.text = TableManager.GetString("STR_UI_DIC_02");
        ItemLabel.text = TableManager.GetString("STR_UI_DIC_03");

        Grid_1.Reposition();
        Grid_2.Reposition();
        Grid_3.Reposition();

        isReady = true;
        Tab_1.value = true;
        Change();
    }

    public void OnClick_Close()
    {
        if (gameObject.activeSelf)
        {
            GameProcess.PlaySound(SOUND_EFFECT.CLICK);
            Hide();
        }
    }

    public void Change()
    {
        if (!isReady) return;
        
        if (Tab_2.value)
        {
            Grid_2.gameObject.SetActive(true);
            Grid_2.Reposition();
            Grid_1.gameObject.SetActive(false);
            Grid_3.gameObject.SetActive(false);
        }
        else if (Tab_3.value)
        {
            Grid_3.gameObject.SetActive(true);
            Grid_3.Reposition();
            Grid_1.gameObject.SetActive(false);
            Grid_2.gameObject.SetActive(false);
        }
        else
        {
            Grid_1.gameObject.SetActive(true);
            Grid_1.Reposition();
            Grid_2.gameObject.SetActive(false);
            Grid_3.gameObject.SetActive(false);
        }
        ScrollView.ResetPosition();
    }
}
