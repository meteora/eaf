﻿using UnityEngine;
using System.Collections;

public class GameComponent : MonoBehaviour {

    public virtual void Init()
    {

    }

    public virtual void Show()
    {
        if (!gameObject.activeSelf) gameObject.SetActive(true);
    }

    protected Vector3 hidePos = new Vector3(1000f, 0f, 0f);
    public virtual void Hide()
    {
        if (gameObject.activeSelf) gameObject.SetActive(false);
    }
}
