﻿using UnityEngine;
using System.Collections;

public class AutoDisable : MonoBehaviour {

	// Use this for initialization
	void Start () {
        gameObject.SetActive(false);	
	}
}
