﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System;

using Kazoh.Table;

public class LayoutManager_UI_LogIn : LayoutManager {

    public Component_UI_LogIn LogInUI;

    private string keyId;
    private string keyPw;

    protected override IEnumerator Loading()
    {
        yield return base.Loading();

        if (GameProcess.CheckLooting())
        {
            GameProcess.ShowError(new GameException(GameException.ErrorCode.NotAllowedDevice));
            yield break;
        }

        /* 키 설정 */
        keyId = EncryptedPlayerPrefs.userKeys[0];
        keyPw = EncryptedPlayerPrefs.userKeys[1];

        /* 배경음 적용 */
        GameProcess.PlaySound(SOUND_EFFECT.BGM, "bgm_01");

        /* 로그인 UI 초기화 */
        LogInUI.Init(keyId, keyPw);
        LogInUI.SignInEvent += SignIn;
        LogInUI.CreateEvent += CreateAccount;
        LogInUI.FindEvent += FindPw;
        OnFinishLoading();
    }

    void SignIn(string _email, string _pw)
    {
        try
        {
            DBManager.SignIn(_email, _pw, delegate (int _guid, string _timeStr)
            {
                EncryptedPlayerPrefs.SetString(keyId, _email);
                EncryptedPlayerPrefs.SetString(keyPw, _pw);
                GameProcess.ShowLoading();
                StartCoroutine(LoadingDate(_guid, _email, _timeStr, false));
            });
        }
        catch (GameException e)
        {
            GameProcess.ShowError(e);
        }
        catch (Exception e)
        {
            GameProcess.ShowError(new GameException(e));
        }
    }

    void CreateAccount(string _email, string _pw)
    {
        try
        {
            DBManager.CreateAccount(_email, _pw, delegate (int _guid, string _timeStr)
            {
                GameProcess.ShowLoading();
                EncryptedPlayerPrefs.SetString(keyId, _email);
                EncryptedPlayerPrefs.SetString(keyPw, _pw);
                StartCoroutine(LoadingDate(_guid, _email, _timeStr, true));
            });
        }
        catch (GameException e)
        {
            GameProcess.ShowError(e);
        }
        catch (Exception e)
        {
            GameProcess.ShowError(new GameException(e));
        }
    }

    void FindPw(string _email)
    {
        try
        {
            string title = TableManager.GetString("STR_TITLE_FIND_PW");
            string msg = TableManager.GetString("STR_MSG_FIND_PW");
            string text1 = TableManager.GetString("STR_UI_YES");
            string text2 = TableManager.GetString("STR_UI_NO");

            GameProcess.ShowPopup(NoticeType.YES_NO, title, msg, text1,text2, delegate ()
            {
                string tempPw = GameProcess.GetGameDataManager().GetNewPw();

                title = TableManager.GetString("STR_MAIL_SUB");
                msg = string.Format(TableManager.GetString("STR_MAIL_MSG"),tempPw);

                DBManager.FindPw(_email, tempPw, title, msg, delegate ()
                {
                    title = TableManager.GetString("STR_TITLE_MAIL_PW");
                    msg = TableManager.GetString("STR_MSG_MAIL_PW");
                    text1 = TableManager.GetString("STR_UI_OK");
                    GameProcess.ShowPopup(NoticeType.OK, title, msg, text1, null);
                });

            }, null);
        }
        catch (GameException e)
        {
            GameProcess.ShowError(e);
        }
        catch (Exception e)
        {
            GameProcess.ShowError(new GameException(e));
        }
    }

    IEnumerator LoadingDate(int _guid, string _email, string _timeStr, bool isSignUp)
    {
        yield return null;

        /* 테이블 데이터 로딩 */
        try
        {
            if(isSignUp)
            {
                GameProcess.GetGameDataManager().SignUp(_guid, _email, _timeStr, delegate ()
                {
                    GameProcess.GetMissionManager().Load();
                    GameProcess.GetRaidManager().Load();

                    if (GameProcess.GetGameDataManager().HasNoCharacter) LoadScene("CharacterScene");
                    else LoadScene("GameScene");
                });
            }
            else
            {
                GameProcess.GetGameDataManager().SignIn(_guid, _email, _timeStr, delegate ()
                {
                    GameProcess.GetMissionManager().Load();
                    GameProcess.GetRaidManager().Load();

                    if (GameProcess.GetGameDataManager().HasNoCharacter) LoadScene("CharacterScene");
                    else LoadScene("GameScene");
                });
            }
        }
        catch (GameException e)
        {
            GameProcess.ShowError(e);
        }
        catch (Exception e)
        {
            GameProcess.ShowError(new GameException(e));
        }
    }

    //IEnumerator CheckTimeOut()
    //{
    //    float _waitTime = 30f;
    //    while (_waitTime > 0)
    //    {
    //        yield return null;
    //       _waitTime -= Time.deltaTime;
    //    }

    //    GameProcess.ShowError(new GameException(GameException.ErrorCode.TimeOut));
    //}

    protected override void Quit()
    {
        base.Quit();
    }
}
