﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;

using Kazoh.Table;

public class Data_UserCharacter
{
    public TableData_Character Data { get; private set; }
    public int EnchantNum { get; private set; }
    public int Grade { get; private set; }

    public int OptionSAtk { get; private set; }
    public int OptionLAtk { get; private set; }
    public int OptionASpd { get; private set; }
    public int OptionSpd { get; private set; }
    public int OptionDef { get; private set; }
    public int OptionHp { get; private set; }

    public int SkinId { get; private set; }

    public Dictionary<int, Slot_Item> EquipSlot;
    public List<int> OptionList;

    private int curHp;
    public int CurHp
    {
        get
        {
            if (curHp > MaxHp) curHp = MaxHp;
            return curHp;
        }
        set
        {
            curHp = value;
            if (curHp < 0) curHp = 0;
        }
    }

    public int DataId { get { return Data == null ? 0 : Data.Id; } }

    public int Pow { get { return Data == null ? 0 : Data.Pow + EnchantNum + GetEquipPow(); } }
    public int Dex { get { return Data == null ? 0 : Data.Dex + EnchantNum + GetEquipDex(); } }
    public int Int { get { return Data == null ? 0 : Data.Int + EnchantNum + GetEquipInt(); } }
    public int Con { get { return Data == null ? 0 : Data.Con + EnchantNum + GetEquipCon(); } }
    public int Luc { get { return Data == null ? 0 : Data.Luc + EnchantNum + GetEquipLuc(); } }

    public int SAtk { get { return GetSAtk(); } }
    public int LAtk { get { return GetLAtk(); } }
    public int ASpd { get { return GetASpd(); } }
    public int Spd { get { return GetSpd(); } }
    public int Def { get { return GetDef(); } }
    public int MaxHp { get { return GetMaxHp(); } }
    public int Critical { get { return GetCritical(); } }
    public float CriticalPercent { get { return GetCriticalPercent(); } }
    public int Wbp { get { return GetWeaponBlockingPoint(); } }
    public int Hit { get { return GetHit(); } }
    public string ChaName { get { return Data == null ? "" : TableManager.GetString(Data.Str); } }
    public string ChaDesc { get { return Data == null ? "" : TableManager.GetString(Data.Desc); } }
    public bool CanEnchant { get { return EnchantNum < 5; } }
    public bool CanDownGrade { get { return Grade > 1; } }
    public string SpriteName
    {
        get
        {
            try
            {
                return (TableManager.GetGameData(SkinId) as TableData_Character).SpriteName;
            }
            catch
            {
                return "";
            }
        }
    }

    public string IconName
    {
        get
        {
            try
            {
                return (TableManager.GetGameData(SkinId) as TableData_Character).IconName;
            }
            catch
            {
                return "";
            }
        }
    }

    public string BulletName
    {
        get
        {
            if( EquipSlot.ContainsKey(1) && 
                EquipSlot[1] != null && 
                EquipSlot[1].Data != null && 
                EquipSlot[1].Data.Data != null && 
                !string.IsNullOrEmpty(EquipSlot[1].Data.Data.BulletName))
            {
                return EquipSlot[1].Data.Data.BulletName;
            }
            else if(Data != null && !string.IsNullOrEmpty(Data.BulletName))
            {
                return Data.BulletName;
            }

            return "pf_bullet_01";            
        }
    }

    public Data_UserCharacter()
    {
        EquipSlot = new Dictionary<int, Slot_Item>();
        for(int i=0; i < 4; ++i)
        {
            EquipSlot.Add(i, null);
        }
        OptionList = new List<int>();
    }

    public Data_UserCharacter(TableData_Character _data) : this()
    {
        Data = _data;
        Grade = _data.Grade;
        CurHp = MaxHp;

        SkinId = _data.Id;

        if (_data.OptionList != null && _data.OptionList.Count > 0)
        {
            for (int i = 0; i < 1; ++i)
            {
                int dice = UnityEngine.Random.Range(0, _data.OptionList.Count);
                string optionName = _data.OptionList[dice];
                TableData_Option option = TableManager.GetGameData(optionName) as TableData_Option;
                OptionList.Add(option.Id);
                SetOptionEffect(option);
            }
        }
    }

    public Data_UserCharacter(IDictionary dict) : this()
    {
        int id = 0;
        if (dict.Contains("ID")) id = System.Convert.ToInt32(dict["ID"]);
        if (id == 0) return;

        Data = TableManager.GetGameData(id) as TableData_Character;
        if (dict.Contains("ENCHANT")) EnchantNum = Convert.ToInt32(dict["ENCHANT"]);
        if (dict.Contains("GRADE")) Grade = Convert.ToInt32(dict["GRADE"]);
        if (dict.Contains("HP")) CurHp = Convert.ToInt32(dict["HP"]);
        if (dict.Contains("SKIN")) SkinId = Convert.ToInt32(dict["SKIN"]);
        else SkinId = Data.Id;

        IList list = dict["EQUIP_LIST"] as IList;
        if(list != null)
        {
            foreach(int slotIdx in list)
            {
                try
                {
                    Slot_Item _item = GameProcess.GetGameDataManager().GetInventory(slotIdx);
                    _item.Equip(true);
                    EquipSlot[_item.EquipSlotId] = _item;
                }
                catch(Exception e)
                {
                    Debug.LogWarning("장비 장착 설정 에러: " + e.Message);
                }
            }
        }

        list = dict["OPTION_LIST"] as IList;
        if (list != null)
        {
            foreach (int optionId in list)
            {
                TableData_Option option = TableManager.GetGameData(optionId) as TableData_Option;
                if(option != null)
                {
                    OptionList.Add(option.Id);
                    SetOptionEffect(option);
                }
            }
        }
    }

    public Dictionary<string, object> GetHash()
    {
        Dictionary<string, object> hash = new Dictionary<string, object>();
        hash.Add("ID", DataId);
        hash.Add("ENCHANT", EnchantNum);
        hash.Add("GRADE", Grade);
        hash.Add("HP", CurHp);
        hash.Add("SKIN", SkinId);

        ArrayList list = new ArrayList();
        List<Slot_Item> inven = GameProcess.GetGameDataManager().GetInventory();
        foreach (KeyValuePair<int,Slot_Item> equip in EquipSlot)
        {
            if(equip.Value != null && inven.Count > 0 && inven.IndexOf(equip.Value) > -1)
            {
                list.Add(inven.IndexOf(equip.Value));
            }
        }
        hash.Add("EQUIP_LIST", list);

        ArrayList options = new ArrayList();
        foreach (int optionId in OptionList)
        {
            if (optionId > 0)
            {
                options.Add(optionId);
            }
        }
        hash.Add("OPTION_LIST", options);

        return hash;
    }

    public string GetCVS(int _slotId)
    {
        int[] optionIds = new int[3];
        for (int i = 0; i < optionIds.Length; ++i)
        {
            if (i < OptionList.Count) optionIds[i] = OptionList[i];
            else optionIds[i] = 0;
        }

        int[] equipIds = new int[4];
        List<Slot_Item> inven = GameProcess.GetGameDataManager().GetInventory();
        foreach (KeyValuePair<int, Slot_Item> equip in EquipSlot)
        {
            if (equip.Value != null && inven.Count > 0 && inven.IndexOf(equip.Value) > -1)
            {
                equipIds[equip.Key] = inven.IndexOf(equip.Value);
            }
            else equipIds[equip.Key] = -1;
        }


        StringBuilder stb = new StringBuilder();
        stb.Append(_slotId);
        stb.Append(",");
        stb.Append(DataId);
        stb.Append(",");
        stb.Append(EnchantNum);
        stb.Append(",");
        stb.Append(Grade);
        stb.Append(",");
        stb.Append(CurHp);
        stb.Append(",");
        stb.Append(SkinId);
        stb.Append(",");
        stb.Append(optionIds[0]);
        stb.Append(",");
        //stb.Append(optionIds[1]);
        //stb.Append(",");
        //stb.Append(optionIds[2]);
        stb.Append(equipIds[0]);
        stb.Append(",");
        stb.Append(equipIds[1]);
        stb.Append(",");
        stb.Append(equipIds[2]);
        stb.Append(",");
        stb.Append(equipIds[3]);

        return stb.ToString();
    }

    public static Dictionary<string, object> GetHashFromCSV(string _csv)
    {
        try
        {
            string[] arr = _csv.Split(',');
            TableData_Character _data = TableManager.GetGameData(Convert.ToInt32(arr[1])) as TableData_Character;
            Dictionary<string, object> hash = new Dictionary<string, object>();
            hash.Add("ID", Convert.ToInt32(arr[1]));
            hash.Add("ENCHANT", Convert.ToInt32(arr[2]));
            hash.Add("GRADE", Convert.ToInt32(arr[3]));
            hash.Add("HP", Convert.ToInt32(arr[4]));
            hash.Add("SKIN", Convert.ToInt32(arr[5]));

            ArrayList options = new ArrayList();
            for (int i = 0; i < 1; ++i)
            {
                if (Convert.ToInt32(arr[6 + i]) > 0) options.Add(Convert.ToInt32(arr[6 + i]));
            }
            hash.Add("OPTION_LIST", options);

            ArrayList equips = new ArrayList();
            for (int i = 0; i < 4; ++i)
            {
                if (Convert.ToInt32(arr[7 + i]) < 0) continue;
                equips.Add(Convert.ToInt32(arr[7 + i]));
            }
            hash.Add("EQUIP_LIST", equips);

            return hash;
        }
        catch
        {
            return new Dictionary<string, object>();
        }
    }

    public string Enchant()
    {
        EnchantNum++;
        string str = TableManager.GetString("STR_MSG_ENCHANTED");
        return str;
    }

    public string DownGrade()
    {
        Grade--;
        string str = TableManager.GetString("STR_MSG_DGRADED");
        return str;
    }

    public void ChangeSkin(int _id)
    {
        try
        {
            TableData_Character target = TableManager.GetGameData(_id) as TableData_Character;
            SkinId = target.Id;
        }
        catch(Exception e)
        {
            throw e;
        }
    }

#if UNITY_EDITOR
    public void SetParamForTest(int satk, int latk, int aspd, int spd, int def, int hp)
    {
        OptionSAtk = satk;
        OptionLAtk = latk;
        OptionASpd = aspd;
        OptionSpd = spd;
        OptionDef = def;
        OptionHp = hp;
        CurHp = MaxHp;
    }
#endif

    int GetSAtk()
    {
        /* 장비 공격력 * ((Pow - 9) / 3 + 1) + 추가 공격력 */
        int value = 0;
        int equip = 0;
        int added = 0;
        if(Data != null)
        {
            added = OptionSAtk;
            for (int i = 0; i < EquipSlot.Count; ++i)
            {
                if (EquipSlot[i] != null && EquipSlot[i].IsEquipped)
                {
                    if (EquipSlot[i].Data.Data.SlotIdx == 1) equip = EquipSlot[i].Data.SAtk;
                    else added += EquipSlot[i].Data.SAtk;
                    added += EquipSlot[i].Data.OptionSAtk;
                }
            }

            equip = Mathf.Max(1, equip);
            value = equip * (Mathf.Max((Pow - 9),0) / 3 + 1) + added;
        }
        return value;
    }

    int GetLAtk()
    {
        /* 장비 공격력 * ((Int - 9) / 3 + 1) + 추가 공격력 */
        int value = 0;
        int equip = 0;
        int added = 0;
        if (Data != null)
        {
            added = OptionLAtk;
            for (int i = 0; i < EquipSlot.Count; ++i)
            {
                if (EquipSlot[i] != null && EquipSlot[i].IsEquipped)
                {
                    if (EquipSlot[i].Data.Data.SlotIdx == 1) equip = EquipSlot[i].Data.LAtk;
                    else added += EquipSlot[i].Data.LAtk;
                    added += EquipSlot[i].Data.OptionLAtk;
                }
            }

            equip = Mathf.Max(1, equip);
            value = equip * (Mathf.Max((Int - 9),0) / 3 + 1) + added;
        }
        return value;
    }

    int GetASpd()
    {
        /* 무기 공속 * Max((100 - Dex / 3) * 0.01, 0.7) - 추가 공속 */
        int value = 0;
        int equip = GameProcess.GetGameConfig().DefaultASpd;
        int added = 0;
        if (Data != null)
        {
            added = OptionASpd;
            for (int i = 0; i < EquipSlot.Count; ++i)
            {
                if (EquipSlot[i] != null && EquipSlot[i].IsEquipped)
                {
                    if(EquipSlot[i].Data.Data.SlotIdx == 1) equip = EquipSlot[i].Data.ASpd;
                    else added += EquipSlot[i].Data.ASpd;
                    added += EquipSlot[i].Data.OptionASpd;
                }
            }

            value = Mathf.FloorToInt(equip * Mathf.Max((100 - Dex / 3) * 0.01f, 0.7f)) - added;
        }
        return Mathf.Max(value, GameProcess.GetGameConfig().MaxASpd);
    }

    int GetSpd()
    {
        /* 기본 이속 + 장비 이속 + Dex가중치 + 추가 이속 */
        int value = 0;
        int equip = 0;
        int added = 0;
        if (Data != null)
        {
            added = OptionSpd;
            for (int i = 0; i < EquipSlot.Count; ++i)
            {
                if (EquipSlot[i] != null && EquipSlot[i].IsEquipped)
                {
                    equip += EquipSlot[i].Data.Spd;
                    added += EquipSlot[i].Data.OptionSpd;
                }
            }

            value = GameProcess.GetGameConfig().DefaultSpd + (Dex / 3) * GameProcess.GetGameConfig().SpdPerDex + equip + added;
        }
        return Mathf.Min(value, GameProcess.GetGameConfig().MaxSpd);
    }

    int GetDef()
    {
        /* 장비 방어력 + 추가 방어력 */
        int value = 0;
        int equip = 0;
        int added = 0;
        if (Data != null)
        {
            added = OptionDef;
            for (int i = 0; i < EquipSlot.Count; ++i)
            {
                if (EquipSlot[i] != null && EquipSlot[i].IsEquipped)
                {
                    equip += EquipSlot[i].Data.Def;
                    added += EquipSlot[i].Data.OptionDef;
                }
            }

            value = equip + added;

        }
        return value;
    }

    int GetMaxHp()
    {
        /* 10 * (Con-5) + 추가 Hp */
        int value = 0;
        int equip = 0;
        int added = 0;
        if (Data != null)
        {
            added = OptionHp;
            for (int i = 0; i < EquipSlot.Count; ++i)
            {
                if (EquipSlot[i] != null && EquipSlot[i].IsEquipped)
                {
                    equip += EquipSlot[i].Data.Hp;
                    added += EquipSlot[i].Data.OptionHp;
                }
            }

            value = GameProcess.GetGameConfig().HpPerCon * Mathf.Max((Con-5),0) + equip + added;
        }
        return value;
    }

    int GetCritical()
    {
        /* 기본 크리티컬 + Luc 가중치 + 추가 크리티컬 */
        int value = 0;
        int added = 0;
        if (Data != null)
        {
            //added = OptionWbp;
            for (int i = 0; i < EquipSlot.Count; ++i)
            {
                if (EquipSlot[i] != null && EquipSlot[i].IsEquipped)
                {
                    added += EquipSlot[i].Data.Cri;
                    //added += EquipSlot[i].Data.OptionCri;
                }
            }
            value = GameProcess.GetGameConfig().DefaultCri + (Luc / 3) * GameProcess.GetGameConfig().CriPerLuc + added;
        }
        return Mathf.Min(GameProcess.GetGameConfig().MaxCritical, value);
    }

    float GetCriticalPercent()
    {
        int value = GameProcess.GetGameConfig().DefaultCriPercent;
        return value * 0.01f;
    }

    int GetWeaponBlockingPoint()
    {
        int value = 0;
        int equip = 0;
        int added = 0;
        if (Data != null)
        {
            //added = OptionWbp;
            for (int i = 0; i < EquipSlot.Count; ++i)
            {
                if (EquipSlot[i] != null && EquipSlot[i].IsEquipped)
                {
                    if (EquipSlot[i].Data.Data.SlotIdx == 1) equip = EquipSlot[i].Data.Wbp;
                    else added += EquipSlot[i].Data.Wbp;
                    //added += EquipSlot[i].Data.OptionWbp;
                }
            }

            if(equip > 0) value = GameProcess.GetGameConfig().BlockPerDex * Mathf.Max((Dex - 9),0) + equip + added;
        }
        return value;
    }

    int GetHit()
    {
        if (EquipSlot == null || !EquipSlot.ContainsKey(1) || EquipSlot[1] == null || EquipSlot[1].IsEmpty) return 1;
        return EquipSlot[1].Hit;
    }

    int GetEquipPow()
    {
        int value = 0;
        for(int i=0; i<EquipSlot.Count; ++i)
        {
            if(EquipSlot[i] != null && EquipSlot[i].IsEquipped) value += EquipSlot[i].Data.Pow;
        }

        return value;
    }

    int GetEquipDex()
    {
        int value = 0;
        for (int i = 0; i < EquipSlot.Count; ++i)
        {
            if (EquipSlot[i] != null && EquipSlot[i].IsEquipped) value += EquipSlot[i].Data.Dex;
        }

        return value;
    }

    int GetEquipInt()
    {
        int value = 0;
        for (int i = 0; i < EquipSlot.Count; ++i)
        {
            if (EquipSlot[i] != null && EquipSlot[i].IsEquipped) value += EquipSlot[i].Data.Int;
        }

        return value;
    }

    int GetEquipCon()
    {
        int value = 0;
        for (int i = 0; i < EquipSlot.Count; ++i)
        {
            if (EquipSlot[i] != null && EquipSlot[i].IsEquipped) value += EquipSlot[i].Data.Con;
        }

        return value;
    }

    int GetEquipLuc()
    {
        int value = 0;
        for (int i = 0; i < EquipSlot.Count; ++i)
        {
            if (EquipSlot[i] != null && EquipSlot[i].IsEquipped) value += EquipSlot[i].Data.Luc;
        }

        return value;
    }

    void SetOptionEffect(TableData_Option option)
    {
        if(option != null)
        {
            switch(option.Type)
            {
                case TableEnum.OptionType.SATK:
                    OptionSAtk += option.Value;
                    break;
                case TableEnum.OptionType.LATK:
                    OptionLAtk += option.Value;
                    break;
                case TableEnum.OptionType.DEF:
                    OptionDef += option.Value;
                    break;
                case TableEnum.OptionType.ASPD:
                    OptionASpd += option.Value;
                    break;
                case TableEnum.OptionType.SPD:
                    OptionSpd += option.Value;
                    break;
                case TableEnum.OptionType.HP:
                    OptionHp += option.Value;
                    break;
            }
        }
    }
}
