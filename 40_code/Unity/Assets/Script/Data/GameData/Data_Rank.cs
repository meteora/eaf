﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Data_Rank
{
    public struct Equip
    {
        public int Id;
        public int Enchant;

        public Equip(int _id, int _enchant)
        {
            Id = _id;
            Enchant = _enchant;
        }
    }

    public int Id { get; private set; }
    public int ChaId { get; private set; }
    public int ChaGrade { get; private set; }
    public string Name { get; private set; }
    public int Record { get; private set; }
    public List<Equip> EquipList;

    public Data_Rank()
    {
        Id = 0;
        Record = 0;
        ChaId = 0;
        ChaGrade = 0;
        Name = "";
        EquipList = new List<Equip>();
    }

    public Data_Rank(int _id, int _chaId, int _chaGrade, string _name, int _record, int[] _equips) : this()
    {
        Id = _id;
        Record = _record;
        ChaId = _chaId;
        ChaGrade = _chaGrade;
        Name = _name;
        if(_equips.Length % 2 == 0)
        {
            for(int i=0; i < _equips.Length; i+=2)
            {
                EquipList.Add(new Equip(_equips[i], _equips[i + 1]));
            }
        }
    }

    public Data_Rank(IDictionary dict) : this()
    {
        if (dict.Contains("ID")) Id = System.Convert.ToInt32(dict["ID"]);
        if (dict.Contains("SCORE")) Record = System.Convert.ToInt32(dict["SCORE"]);
        if (dict.Contains("CHA_ID")) ChaId = System.Convert.ToInt32(dict["CHA_ID"]);
        if (dict.Contains("CHA_GRADE")) ChaGrade = System.Convert.ToInt32(dict["CHA_GRADE"]);
        if (dict.Contains("NAME")) Name = System.Convert.ToString(dict["NAME"]);
        if (dict.Contains("EQUIP"))
        {
            IList list = MiniJSON.Json.Deserialize(System.Convert.ToString(dict["EQUIP"])) as IList;
            if (list != null)
            {
                foreach (string csv in list)
                {
                    string[] arr = csv.Split(',');
                    if (arr.Length == 2)
                    {
                        int _id = System.Convert.ToInt32(arr[0]);
                        int _enchant = System.Convert.ToInt32(arr[1]);
                        EquipList.Add(new Equip(_id, _enchant));
                    }
                }
            }
        }
    }
}
