﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Data_Arena
{
    public struct Equip
    {
        public int Id;
        public int Enchant;

        public Equip(int _id, int _enchant)
        {
            Id = _id;
            Enchant = _enchant;
        }
    }

    public struct Param
    {
        public int SAtk;
        public int LAtk;
        public int ASpd;
        public int Spd;
        public int Def;
        public int Hp;
        public int Wbp;
        public int Cri;

        public Param(int _satk, int _latk, int _aspd, int _spd, int _def, int _hp, int _wbp, int _cri)
        {
            SAtk = _satk;
            LAtk = _latk;
            ASpd = _aspd;
            Spd = _spd;
            Def = _def;
            Hp = _hp;
            Wbp = _wbp;
            Cri = _cri;
        }

        public Param(IList _list)
        {
            SAtk = (int)_list[0];
            LAtk = (int)_list[1];
            ASpd = (int)_list[2];
            Spd = (int)_list[3];
            Def = (int)_list[4];
            Hp = (int)_list[5];
            Wbp = (int)_list[6];
            Cri = (int)_list[7];
        }
    }

    public int Id { get; private set; }
    public int ChaId { get; private set; }
    public int Grade { get; private set; }
    public string Name { get; private set; }
    public int Record { get; private set; }
    public int Rank { get; private set; }
    public Param Ability;
    public List<Equip> EquipList;

    public Data_Arena()
    {
        Id = 0;
        Record = 0;
        ChaId = 10000;
        Grade = 0;
        Name = "";
        Rank = 0;
        Ability = new Param();
        EquipList = new List<Equip>();
    }

    public Data_Arena(int _chaId, string _icon, int _grade, string _name, int _rank, int _satk, int _latk, int _aspd, int _spd, int _def, int _hp, int _wbp, int _cri)
    {
        Id = 0;
        Record = 0;
        ChaId = _chaId;
        Grade = _grade;
        Name = _name;
        Rank = _rank;
        Ability = new Param(_satk, _latk, _aspd, _spd, _def, _hp, _wbp, _cri);
        EquipList = new List<Equip>();
    }

    public Data_Arena(IDictionary dict) : this()
    {
        if (dict.Contains("ID")) Id = System.Convert.ToInt32(dict["ID"]);
        if (dict.Contains("SCORE")) Record = System.Convert.ToInt32(dict["SCORE"]);
        if (dict.Contains("RANK")) Rank = System.Convert.ToInt32(dict["RANK"]);
        if (dict.Contains("CHA_INFO"))
        {
            IList list = MiniJSON.Json.Deserialize(System.Convert.ToString(dict["CHA_INFO"])) as IList;
            if(list.Count == 2)
            {
                ChaId = (int)list[0];
                Grade = (int)list[1];
            }
        }
        if (dict.Contains("ABILITY"))
        {
            IList list = MiniJSON.Json.Deserialize(System.Convert.ToString(dict["ABILITY"])) as IList;
            if (list.Count == 8) Ability = new Param(list);
        }
        if (dict.Contains("NAME")) Name = System.Convert.ToString(dict["NAME"]);
        if (dict.Contains("EQUIP"))
        {
            IList list = MiniJSON.Json.Deserialize(System.Convert.ToString(dict["EQUIP"])) as IList;
            if (list != null)
            {
                foreach (string csv in list)
                {
                    string[] arr = csv.Split(',');
                    if (arr.Length == 2)
                    {
                        int _id = System.Convert.ToInt32(arr[0]);
                        int _enchant = System.Convert.ToInt32(arr[1]);
                        EquipList.Add(new Equip(_id, _enchant));
                    }
                }
            }
        }
    }
}
