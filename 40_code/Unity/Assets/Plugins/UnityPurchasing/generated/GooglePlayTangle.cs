#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("KuEOCIuAY4Be0Dv0Ao1PTS2J60UWpCcEFisgLwygbqDRKycnJyMmJRy9eWQXjgUTBPOnafohA81KpeioeU+47szRHGgsoEXSyp66lylljyqkJykmFqQnLCSkJycmmoAZja38muyZl+AIxv8AEm9ELSwXXRnKs6H/iOvVJ35EGgf8A0uymtFibw+icsP5YQakx1ZygB6lgVSeFyzTXN7vQ40cSQKrZ6Uw7PKaLSPh7zHfuCIqmNohpWCy/sTVvzmtMArul+xCz5/L4VJwx02gjcfuAPYUiJg1HE3X4ndNRkO/Ev7DtLf8FeHz7PG5essQFTD39u1Z7H7lfPeznV0OpQ8EKnN2u+JcOeS/ueNULlxv+9jwm1dp5bmb19uQmzZS8yQlJyYn");
        private static int[] order = new int[] { 4,4,11,6,4,11,10,11,8,9,12,12,12,13,14 };
        private static int key = 38;

        public static byte[] Data() {
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
