#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("kJeUkZWWk/+yqJaQlZeVnJeUkZXchcTW1tDIwNaFxMbGwNXRxMvGwNLSi8TV1cnAi8bKyIrE1dXJwMbEqjiYVo7sjb9tW2sQHKt8+7lzbpiVtKOm8KGvtq/k1dXJwIXsy8aLlPwCoKzZsuXztLvRdhIuhp7iBnDKgUdOdBLVeqrgRIJvVMjdSEIQsrLg27rpzvUz5Cxh0ceutSbkIpYvJNXJwIXmwNfRzMPMxsTRzMrLheTQx8nAhdbRxMvBxNfBhdHA18jWhcSWk/+Vx5SulayjpvCho7an8PaUtiWxjnXM4jHTrFtRziiL5QNS4uja7H3TOpaxwATSMWyIp6akpaQGJ6S6NH674vVOoEj73CGITpMH8unwScIqrRGFUm4JiYXK1ROapJUpEuZqlSehHpUnpgYFpqekp6ekp5Woo6wNeduHkG+AcHyqc85xB4GGtFIECczDzMbE0czKy4Xk0NHNytfM0dyU0czDzMbE0cCFx9yFxMvchdXE19EOBtQ34vbwZAqK5BZdXkbVaEMG6XyT2mQi8HwCPByX5159cNQ72wT3J6Slo6yPI+0jUsbBoKSVJFeVj6Pa5A09XHRvwzmBzrR1Bh5Bvo9mutHNytfM0dyUs5Wxo6bwoaa2qOTVoaO2p/D2lLaVtKOm8KGvtq/k1dWjlaqjpvC4tqSkWqGglaakpFqVuGXGltJSn6KJ805/qoSrfx/WvOoQ35UnpNOVq6Om8LiqpKRaoaGmp6Soo6yPI+0jUqikpKCgpaYnpKSl+RtR1j5Ld8GqbtzqkX0Hm1zdWs5trY6jpKCgoqeks7vN0dHV1p+KitIq1iTFY77+rIo3F13h7VXFnTuwUKClpiekqqWVJ6SvpyekpKVBNAysg5WBo6bwoa62uOTV1cnAhebA19H3wMnMxMvGwIXKy4XRzczWhcbA14XEy8GFxsDX0czDzMbE0czKy4XV1cnAhffKytGF5uSVu7KolZOVkZeFysOF0c3AhdHNwMuFxNXVyczGxKJJ2JwmLvaFdp1hFBo/6q/OWo5ZhebklSekh5Woo6yPI+0jUqikpKSjpvC4q6GzobGOdcziMdOsW1HOKMvBhcbKy8HM0czKy9aFysOF0NbAbLzXUPircNr6PleAph/wKuj4qFSYg8KFL5bPUqgnantOBopc9s/+wYmFxsDX0czDzMbE0cCF1crJzMbcLrwse1zuyVCiDoeVp029m131rHbJwIXsy8aLlIOVgaOm8KGutrjk1RCfCFGqq6U3rhSEs4vRcJmofsezuiAmIL48mOKSVww+5SuJcRQ1t33XxMbRzMbAhdbRxNHAyMDL0daLlbOVsaOm8KGmtqjk1dXJwIX3ysrRipUkZqOtjqOkoKCip6eVJBO/JBaL5QNS4ujarfuVuqOm8LiGob2Vs637lSektKOm8LiFoSekrZUnpKGVkzzpiN0SSCk+eVbSPlfTd9KV6mSPI+0jUqikpKCgpZXHlK6VrKOm8MGQhrDusPy4FjFSUzk7avUfZP31FJX9Sf+hlynNFiq4e8DWWsL7wBkwO9+pAeIu/nGzkpZuYaroa7HMdBK+GDbngbePYqq4E+g5+8Zt7iWy9Q8vcH9BWXWsopIV0NCE");
        private static int[] order = new int[] { 7,40,36,22,9,52,53,25,9,42,42,32,27,15,21,25,54,32,39,24,58,34,32,52,36,29,57,28,47,50,35,55,34,42,38,45,53,41,51,50,45,43,53,56,44,54,51,52,49,51,52,56,55,53,56,56,58,57,58,59,60 };
        private static int key = 165;

        public static byte[] Data() {
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
