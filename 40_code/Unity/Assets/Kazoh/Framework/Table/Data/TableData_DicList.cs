﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class TableData_DicList : TableData
{
    readonly public TableEnum.DicType Type;
    readonly public int Data;
    readonly public string Info_1;
    readonly public string Info_2;
    
    public TableData_DicList(IDictionary dict)
        : base(dict)
    {
        if (dict.Contains("TYPE")) Type = (TableEnum.DicType)Enum.Parse(typeof(TableEnum.DicType), Convert.ToString(dict["TYPE"]), true);
        if (dict.Contains("DATA")) Data = Convert.ToInt32(dict["DATA"]);
        if (dict.Contains("INFO_1")) Info_1 = Convert.ToString(dict["INFO_1"]);
        if (dict.Contains("INFO_2")) Info_2 = Convert.ToString(dict["INFO_2"]);
    }
}
