﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class TableData_Exp : TableData
{
    readonly public int Exp;

    public TableData_Exp(IDictionary dict)
        : base(dict)
    {
        if (dict.Contains("EXP")) Exp = Convert.ToInt32(dict["EXP"]);
    }
}
