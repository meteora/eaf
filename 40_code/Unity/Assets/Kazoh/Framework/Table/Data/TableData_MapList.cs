﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class TableData_MapList : TableData
{
    public struct UnlockCond
    {
        readonly public TableEnum.UnlockType Cond;
        readonly public int Value;

        public UnlockCond(IDictionary dict)
        {
            Cond = TableEnum.UnlockType.None;
            Value = 0;

            if (dict != null)
            {
                if (dict.Contains("COND")) Cond = (TableEnum.UnlockType)Enum.Parse(typeof(TableEnum.UnlockType), Convert.ToString(dict["COND"]), true);
                if (dict.Contains("VALUE")) Value = Convert.ToInt32(dict["VALUE"]);
            }
        }
    }
    readonly public int MapId;
    readonly public int NextMapId;
    readonly public int PrevMapId;
    readonly public bool IsRaid;
    public List<UnlockCond> UnlockCondList;

    public TableData_MapList() : base()
	{
        MapId = 0;
        NextMapId = 0;
        PrevMapId = 0;
        UnlockCondList = new List<UnlockCond>();
	}

    public TableData_MapList(IDictionary dict)
        : base(dict)
    {
        if (dict.Contains("MAP_ID")) MapId = Convert.ToInt32(dict["MAP_ID"]);
        if (dict.Contains("NEXT_MAP_ID")) NextMapId = Convert.ToInt32(dict["NEXT_MAP_ID"]);
        if (dict.Contains("PREV_MAP_ID")) PrevMapId = Convert.ToInt32(dict["PREV_MAP_ID"]);
        if (dict.Contains("IS_RAID")) IsRaid = Convert.ToBoolean(dict["IS_RAID"]);

        UnlockCondList = new List<UnlockCond>();
        IList list = dict["COND_ARRAY"] as IList;
        foreach (IDictionary dict2 in list)
        {
            UnlockCond data = new UnlockCond(dict2);
            UnlockCondList.Add(data);
        }
    }
}
