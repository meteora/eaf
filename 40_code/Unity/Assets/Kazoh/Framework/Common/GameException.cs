﻿using System;
using System.Collections;

using Kazoh.Table;

public class GameException : Exception
{
    public enum ErrorCode
    {
        // 치명적 오류, 시스템 종료됨.
        Unknown = 0,
        UnknownServerError = 1,
        NoNetwork = 2,
        NotAllowedDevice = 3,
        TimeOut = 4,
        NoMatchBuild = 5,
        ServerShutDown = 100,
        ClosedTest = 101,
        InvalidAPICode = 401,
        InvalidToken = 403,
        BlockedAccount = 501,
        FailBlock = 502,
        Kick = 503,
        CanNotInitTableManager = 1000,
        CanNotLoadTable = 1001,
        FailToGetGameData = 1100,
        FailToAddData = 1101,
        FailToGetGameConfig = 1102,
        InvalidConfigVer = 1103,

        // 캐릭터 및 아이템 관련 오류.
        ChaSlotIsFull = 2000,
        InvalidParam = 2001,
        NotEnoughCash = 2002,
        NotEnoughCoin = 2003,
        NotEnoughKey = 2004,
        NotEnoughGold = 2005,
        OverMaxGold = 2006,
        OverMaxCoin = 2007,
        OverMaxKey = 2008,
        OverMaxCash = 2009,
        OverMaxInventory = 2010,
        OverMaxCharacter = 2011,
        OverMaxSalePrice = 2012,
        CharacterSlotIsEmpty = 2100,
        CanNotSelectCharacter = 2101,
        CanNotDeleteCharacter = 2102,
        CanNotEnchantCharacter = 2103,
        CanNotDGradeCharacter = 2104,
        CanNotHeal = 2105,
        CanNotDeleteDefaultCharacter = 2106,
        CanNotExtractSkin = 2107,
        NoCurCharacter = 2108,
        ItemSlotIsEmpty = 2200,
        CanNotSellItem = 2201,
        NotEnoughItem = 2202,
        CanNotUseItem = 2203,
        CanNotEquipItem = 2204,
        CanNotEnchantItem = 2205,
        CanNotEnchantEquippedItem = 2206,
        CanNotStackItem = 2207,
        CanNotSellEquippedItem = 2208,
        CanNotUseMembershipService = 2209,
        OnlySilverMembership = 2210,
        OnlyGoldMembership = 2211,
        OnlyDiaMembership = 2212,
        OnlyPlatinumMembership = 2213,
        OverPurchaseLimit = 2214,
        NotEnoughArenaItem = 2215,

        // 맵 관련 오류.
        NoMapPrefabs = 5000,
        HpIsZero = 5001,
        OverGrade = 5002,
        NotEnoughItemSlot = 5003,
        NoStartPoint = 5100,
        NoPlayer = 5101,
        NoTimer = 5102,
        InvalidMapPrefabs = 5103,

        // IAP 관련 오류.
        FailInitialization = 7000,
        NotInitializedPurchaser = 7001,
        NotPurchasingProduct = 7002,
        FailRestorePurchases = 7003,

        // 서버 관련 오류.
        InvalidIdOrPassward = 8000,
        DuplicatedId = 8001,
        CanNotConnectServer = 8002,
        NotExistId = 8003,
        ExistId = 8004,
        CanNotGetRankData = 8005,
        InvalidPassward = 8006,
        NoSavedDataOnServer = 8007,
        OverMaxRow = 8008,
        FailToUpdateData = 8009,
        InvalidCouponId = 8100,
        NotFulfillCond = 8101,
        ExpiredCoupon = 8102,
        NotExistData = 8201,
        ExistUserId = 8202,

        // 공용 오류.
        NoGameData = 9000,
        NoId = 9001,
        NoPw = 9002,
        InvalidIdFormat = 9003,
        InvalidPwFormat = 9004,
        NoService = 9005,
        UserDataIsNull = 9006,
        EmptyCurPw = 9007,
        EmptyNewPw = 9008,
        EmptyConformPw = 9009,
        EqualsNewPw = 9010,
        NotEqualsNewPw = 9011,
        InvalidInicial = 9013,
        InvalidCouponNum = 9014,
        NoEmail = 9015,
        InvalidEmailFormat = 9016,
    }

    public ErrorCode Code;
    public readonly string Msg;
    public readonly string Trace;
    public bool IsCritical
    {
        get
        {
            if ((int)Code < 2000) return true;
            return false;
        }
    }

    public GameException()
    {
        Code = ErrorCode.Unknown;
        Msg = GetMsg(Code);
    }

    public GameException(ErrorCode _code)
    {
        Code = _code;
        Msg = GetMsg(Code);
    }

    public GameException(ErrorCode _code, string _message) : base(_message)
    {
        Code = _code;
        Msg = _message;
    }

    public GameException(Exception e) : base(e.Message)
    {
        Code = ErrorCode.Unknown;
        Msg = Message;
        Trace = e.StackTrace;
    }

    public GameException(string message)
        : base(message)
    {
    }

    public GameException(string message, Exception inner)
        : base(message, inner)
    {
    }

    string GetMsg(ErrorCode _code)
    {
        return TableManager.GetString("STR_ERROR_" + (int)_code);
    }
}
