﻿using UnityEngine;
using System.Collections;
using System;

public class RaidManager {

    private int[] regions;

    public RaidManager()
    {
        regions = new int[7];
    }

    public void Load()
    {
        try
        {
            DBManager.GetRaidInfo(delegate (string _data)
            {
                SetRaidData(_data);
            });
        }
        catch(Exception e)
        {
            Debug.LogError(e);
        }
    }

    void SetRaidData(string _data)
    {
        try
        {
            if (string.IsNullOrEmpty(_data))
            {
#if UNITY_EDITOR || DEBUG_MODE
                Debug.LogWarning("레이드 정보 없음.");
#endif
                return;
            }

            IList data = MiniJSON.Json.Deserialize(System.Convert.ToString(_data)) as IList;
            if (data == null) throw new GameException(GameException.ErrorCode.InvalidParam);

            foreach (IDictionary dict in data)
            {
                if (dict.Contains("DAY") && dict.Contains("REGION"))
                {
                    int idx = System.Convert.ToInt32(dict["DAY"]);
                    int region = System.Convert.ToInt32(dict["REGION"]);
                    if (idx < 0 || idx >= regions.Length) continue;
                    regions[idx] = region;
                }
            }
        }            
        catch
        {
            Debug.LogWarning("레이드 정보가 형식에 맞지 않습니다.");
        }
    }

    public int CurRegion()
    {
        int day = (int)DateTime.Now.DayOfWeek;
        if (day < regions.Length) return regions[day];
        return 0;
    }
}
