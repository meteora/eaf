﻿using UnityEngine;
using System.Collections;
using System.Text;

public class DataCheckManager
{
    private int gold;
    private int coin;
    private int cash;
    private int purchase;

    readonly private int GOLD;
    readonly private int COIN;
    readonly private int CASH;
    readonly private int PURCHASE;
    readonly private int GUID;
    readonly private string USER_ID;
    readonly private string PW;

    readonly public int DeltaGold;
    readonly public int DeltaCoin;
    readonly public int DeltaCash;
    readonly public int TotalGold;
    readonly public int TotalPurchase;

    private bool ignore;

    public DataCheckManager(Data_User _data, int _guid, string _userId, string _pw)
    {
        GUID = _guid;
        USER_ID = _userId;
        PW = _pw;
        GOLD = gold = _data.Gold;
        COIN = coin = _data.Coin;
        CASH = cash = _data.Cash;
        PURCHASE = purchase = _data.PurchaseAmount;

        DeltaGold = GameProcess.GetGameConfig().DeltaGold;
        DeltaCoin = GameProcess.GetGameConfig().DeltaCoin;
        DeltaCash = GameProcess.GetGameConfig().DeltaCash;
        TotalGold = GameProcess.GetGameConfig().TotalGold;
        TotalPurchase = GameProcess.GetGameConfig().TotalPurchase;

        _data.ChangedEvent += Check;
    }

    public bool CheckData(TableData_GoodsList _data)
    {
        int code = 0;
        StringBuilder sb = new StringBuilder();
        if (_data != null && _data.Price < 0)
        {
            code = 1;
            sb.Append("제품 데이터 변조(");
            sb.Append(_data.Id);
            sb.Append(",");
            sb.Append(_data.Price);
            sb.Append(") ");
        }

        if (code == 1)
        {
            DBManager.SendLog(GUID, USER_ID, sb.ToString(), null);
            DBManager.BlockId(GUID, PW);
            GameProcess.ShowError(new GameException(GameException.ErrorCode.Kick));
            return false;
        }

        return true;
    }

    public void CheckReciept(string _id, string _reciept)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("영수증 변조(");
        sb.Append(_id);
        sb.Append(",");
        sb.Append(_reciept);
        sb.Append(") ");
        DBManager.SendLog(GUID, USER_ID, sb.ToString(), null);
        //DBManager.BlockId(GUID, PW);
        //GameProcess.ShowError(new GameException(GameException.ErrorCode.Kick));
    }

    void Check(Data_User _data)
    {
        int dGold = _data.Gold - gold;
        int dCoin = _data.Coin - coin;
        int dCash = _data.Cash - cash;
        int dPurchase = _data.PurchaseAmount - purchase;

        int tGold = _data.Gold - GOLD;
        //int tCoin = _data.Coin - COIN;
        //int tCash = _data.Cash - CASH;
        int tPurchase = _data.PurchaseAmount - PURCHASE;

        int code = 0;
        StringBuilder sb = new StringBuilder();
        if(dGold > DeltaGold)
        {
            code = 1;
            sb.Append("골드변화량 초과(");
            sb.Append(gold);
            sb.Append("->");
            sb.Append(_data.Gold);
            sb.Append(", ");
            sb.Append(dGold);
            sb.Append(") ");
        }
        if(dCoin > DeltaCoin)
        {
            code = 1;
            sb.Append("코인변화량 초과(");
            sb.Append(coin);
            sb.Append("->");
            sb.Append(_data.Coin);
            sb.Append(", ");
            sb.Append(dCoin);
            sb.Append(") ");
        }
        if (dCash > DeltaCash)
        {
            code = 1;
            sb.Append("캐쉬변화량 초과(");
            sb.Append(cash);
            sb.Append("->");
            sb.Append(_data.Cash);
            sb.Append(", ");
            sb.Append(dCash);
            sb.Append(") ");
        }
        if (dCash > 0 && dPurchase == 0)
        {
            code = 1;
            sb.Append("구매없이 캐쉬획득(");
            sb.Append(cash);
            sb.Append("->");
            sb.Append(_data.Cash);
            sb.Append(", ");
            sb.Append(dCash);
            sb.Append(") ");
        }

        gold = _data.Gold;
        coin = _data.Coin;
        cash = _data.Cash;
        purchase = _data.PurchaseAmount;

        if (code == 1)
        {
            DBManager.SendLog(GUID, USER_ID, sb.ToString(), null);
            DBManager.BlockId(GUID, PW);
            GameProcess.ShowError(new GameException(GameException.ErrorCode.Kick));
            return;
        }

        if (ignore) return;

        if (tGold > TotalGold)
        {
            code = 2;
            sb.Append("골드총변화량 초과(");
            sb.Append(GOLD);
            sb.Append("->");
            sb.Append(_data.Gold);
            sb.Append(", ");
            sb.Append(tGold);
            sb.Append(") ");
        }
        if (tPurchase > TotalPurchase)
        {
            code = 2;
            sb.Append("구매총변화량 초과(");
            sb.Append(tPurchase);
            sb.Append(") ");
        }

        if (code == 2)
        {
            DBManager.SendLog(GUID, USER_ID, sb.ToString(), delegate()
            {
                ignore = true;
            });
        }
    }
}
