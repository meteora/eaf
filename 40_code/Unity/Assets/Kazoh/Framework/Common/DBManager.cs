﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class DBManager {

    public enum FuncCode
    {
        NONE = 0,
        CREATE_ACCOUNT = 1,
        SIGN_IN = 2,
        CHANGE_PW = 3,
        FIND_PW = 4,
        BLOCK = 5,
        SELECT_USER_DATA = 11,
        UPDATE_USER_DATA = 12,
        NEW_USER_DATA = 13,
        CHECK_LOG_SERVER = 21,
        SAVE_LOG = 22,
        GET_CONFIG = 31,
        GET_SERVER_STATE = 41,
        SAVE_ERROR_LOG = 51,
        SELECT_RANK = 61,
        UPDATE_RANK = 62,
        SELECT_USER_CHA = 71,
        UPDATE_USER_CHA = 72,
        SELECT_USER_INVEN = 81,
        UPDATE_USER_INVEN = 82,
        SELECT_USER_MAP = 91,
        UPDATE_USER_MAP = 92,
        SELECT_NOTICE = 101,
        SELECT_REWARD = 201,
        GET_COUPON_REWARD = 211,
        GET_COUPON_COND = 212,
        SELECT_GOODS = 221,
        SELECT_INVEN = 231,
        UPDATE_INVEN = 232,
        ADD_ARENA = 241,
        GET_ARENA = 242,
        WIN_ARENA = 243,
        LOSE_ARENA = 244,
        GET_ARENA_RANK = 245,
        SEND_LOG = 250,
        GET_RAID_INFO = 261,
    }

    private HttpManager httpManager;

    readonly private string URL_ACCOUNT;
    readonly private string URL_USER_DATA;
    readonly private string URL_LOG;
    readonly private string URL_CONFIG;
    readonly private string URL_SERVER_CHECK;
    readonly private string URL_ERROR_LOG;
    readonly private string URL_RANK;
    readonly private string URL_USER_CHA;
    readonly private string URL_USER_INVEN;
    readonly private string URL_USER_MAP;
    readonly private string URL_NOTICE;
    readonly private string URL_REWARD;
    readonly private string URL_COUPON_1;
    readonly private string URL_COUPON_2;
    readonly private string URL_COUPON_3;
    readonly private string URL_COUPON_4;
    readonly private string URL_GOODS;
    readonly private string URL_ARENA;
    readonly private string URL_ALOG;
    readonly private string URL_RAID;

    private static DBManager instance;
    public static DBManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new DBManager();
                instance.httpManager = GameProcess.GetHttpManager();
            }

            return instance;
        }
    }

    private DBManager()
    {
        switch(GameProcess.Instance.TargetServer)
        {
            case GameProcess.Server.DEV:
                URL_ACCOUNT = "https://script.google.com/macros/s/AKfycbx7ALLUBytLFxUGJmFzC6i4M8iwJaYxwChzxylZ6QIo6wA6eA/exec";
                URL_USER_DATA = "https://script.google.com/macros/s/AKfycbx2WcgVg61bWMeI-y7JcqMDZFh3E4HEF4TnmXw-in-EEamXcA/exec";
                URL_LOG = "https://script.google.com/macros/s/AKfycbxMynRM8V0QZYEsCgHZCAkr4OOKfY1YkrgDpYliH7SUr_HYY7A/exec";
                URL_CONFIG = "https://script.google.com/macros/s/AKfycbzzWKD1bfzwGJu3-9tMmfSAxOhs4L4yDuUtf4QZvsjMY9rOop8/exec";
                URL_SERVER_CHECK = "https://script.google.com/macros/s/AKfycbzXB04VJ8oWcdL6CRwdMPRiZfC42Pk-WW9KbmsN9CcVO-sfXA/exec";
                URL_ERROR_LOG = "https://script.google.com/macros/s/AKfycbx6KCBTuufwIliEWCkQxm1m0YksXovtv7x1Q4rD3dsDLe5eMG8/exec";
                URL_RANK = "https://script.google.com/macros/s/AKfycbwe_WuY1zHO9LNpjTMqaSJWs87kOb7ckjUYrgPXWleTD9WjezA/exec";
                URL_USER_CHA = "https://script.google.com/macros/s/AKfycbwz-mcxap2Mk7HEsxzPynFR0t45tr7rJR5G1NzwXi6DLfBxMw/exec";
                URL_USER_INVEN = "https://script.google.com/macros/s/AKfycbybfN9_pT2kKwDtcNXwQKbNbjFx7-HQltr7iVP9n2j4sHHr1g/exec";
                URL_USER_MAP = "https://script.google.com/macros/s/AKfycbwKzly2JIBMZH_iC6XojDWr12v-oW8hmVN_TJr_76hL05NHjQ/exec";
                URL_NOTICE = "https://script.google.com/macros/s/AKfycbwdqsdfBMKe1TFgsX_wflJgs76yEBlPu2sKtbLShNd-KxWSIwA/exec";
                URL_REWARD = "https://script.google.com/macros/s/AKfycbz8ZuHfpug0bBO5iA5EnKYehlJ8zlZ-PPfAe5UgMDx89qaaClU/exec";
                URL_COUPON_1 = "https://script.google.com/macros/s/AKfycbzOAFrgMdVsCjbzNbiiiLd632f-B9XkuHveefXtVRl27sToeyk/exec";
                URL_COUPON_2 = "https://script.google.com/macros/s/AKfycbzOAFrgMdVsCjbzNbiiiLd632f-B9XkuHveefXtVRl27sToeyk/exec";
                URL_COUPON_3 = "https://script.google.com/macros/s/AKfycbzOAFrgMdVsCjbzNbiiiLd632f-B9XkuHveefXtVRl27sToeyk/exec";
                URL_COUPON_4 = "https://script.google.com/macros/s/AKfycbzOAFrgMdVsCjbzNbiiiLd632f-B9XkuHveefXtVRl27sToeyk/exec";
                URL_GOODS = "https://script.google.com/macros/s/AKfycbxboeZoz6CmnLPgCAUObBFIG7K0PaCkS4ms00_Z5QHNQ1uzjA/exec";
                URL_ARENA = "https://script.google.com/macros/s/AKfycbzEpXAnD8xESh4Vpum4qrwMU_1AmQeQhG6ZJqGVskfUoRJC0Q/exec";
                URL_ALOG = "https://script.google.com/macros/s/AKfycbxUrKdfTIrLzr0oBiPIS4l1pOBBx-3nj36AqJ0BjpIvA486Jes/exec";
                URL_RAID = "https://script.google.com/macros/s/AKfycbz0ke-NVnSXoKHRLU2Vgd79Tbj3nycrKFmSHEtS7W44mF90Vw/exec";
                break;
            case GameProcess.Server.TEST:
                URL_ACCOUNT = "https://script.google.com/macros/s/AKfycbwX8UhLmtDvN1KLy13_YbA_yYDuhxOitfRkCORU7ZT1TID-mmg/exec";
                URL_USER_DATA = "https://script.google.com/macros/s/AKfycbwfEUmrd799lW5hR_J18Olormn-h3PnEaoFFcVUDVm7lQXQhkw/exec";
#if UNITY_EDITOR || UNITY_ANDROID
                URL_LOG = "https://script.google.com/macros/s/AKfycbz34W1yk3CWL4y8_wUInAOqzq0l80lqJS9M1jAEXPKoO1BrFXs/exec";
#else
                URL_LOG = "https://script.google.com/macros/s/AKfycbzhd9n-9elZ1FsMKLMUtTH2geo9MEM5OtZurTX3Ckw5-hK14aM/exec";
#endif
                URL_CONFIG = "https://script.google.com/macros/s/AKfycbzA3lXzT7l-9TebXIovk-K4MTd2mlfn_sXs4GdS-AFYJTqFQWU/exec";
                URL_SERVER_CHECK = "https://script.google.com/macros/s/AKfycby6_J_VJXWzaVpUWeuKuLY1_ulhRusK5erFFI9Yzhh8IGsdug/exec";
                URL_ERROR_LOG = "https://script.google.com/macros/s/AKfycbxOtCIkkjkTu_b6pDfCLiTKIVariBc5R4qCb7w7OH3xrr0CPME/exec";
                URL_RANK = "https://script.google.com/macros/s/AKfycbxl6pU7kI3t0L49-FPBCfuQuBBH69AMbrUacvBcXbJDp0o3TQ/exec";
                URL_USER_CHA = "https://script.google.com/macros/s/AKfycbxHc3R7cN7xn3SxS1FyvAHEVdVkcHxD63S9lJtj-Z-SVFG_AXU/exec";
                URL_USER_INVEN = "https://script.google.com/macros/s/AKfycbyYb3t499gCVQGbfxRvJY1VcnD0pfBg03Zd6rU6Q5fDljUiPrs/exec";
                URL_USER_MAP = "https://script.google.com/macros/s/AKfycbxs8eRY5A6CJNq-hWju6DVYNM3NrdOvkdUDmU2Lvv_zPtjUvA/exec";
                URL_NOTICE = "https://script.google.com/macros/s/AKfycbxGyyZPSwgUNyahkmLXdY9QO2jTcLSDeXwLCdlsAm_mSx_mybw/exec";
                URL_REWARD = "https://script.google.com/macros/s/AKfycby1muJR6tmq0H3wNsnk2xUHH0r27uwwVfYwPH1QsmLSYmlkoOs/exec";
                URL_COUPON_1 = "https://script.google.com/macros/s/AKfycbwC5hkFy6tYAL3LXmorXiERUpTf426KBNpzswI0JYdSUtayh_U/exec";
                URL_COUPON_2 = "https://script.google.com/macros/s/AKfycbwC5hkFy6tYAL3LXmorXiERUpTf426KBNpzswI0JYdSUtayh_U/exec";
                URL_COUPON_3 = "https://script.google.com/macros/s/AKfycbwC5hkFy6tYAL3LXmorXiERUpTf426KBNpzswI0JYdSUtayh_U/exec";
                URL_COUPON_4 = "https://script.google.com/macros/s/AKfycbwC5hkFy6tYAL3LXmorXiERUpTf426KBNpzswI0JYdSUtayh_U/exec";
                URL_GOODS = "https://script.google.com/macros/s/AKfycbzQ9ywNlxcxnorGRqsTC13HmsszKotRSUMW8NNN5Au_9YLRwzA/exec";
                URL_ARENA = "";
                URL_ALOG = "";
                URL_RAID = "";
                break;
            case GameProcess.Server.LIVE:
                URL_ACCOUNT = "https://script.google.com/macros/s/AKfycbzxD4JPBHzBPPyC4IX09jVS7KdFdeWVj0cwSN7sYeQlIs7nIg/exec";
                URL_USER_DATA = "https://script.google.com/macros/s/AKfycbyHcz4UNn6TsfnIvEblEFJCkycL6jehl0hZyjhV1cgJ6FYA4wY/exec";
#if UNITY_EDITOR || UNITY_ANDROID
                URL_LOG = "https://script.google.com/macros/s/AKfycbyFHa6dzreiHhkfpNKq4Z4ac4CJnWYc-wg1kdKns0yIwUb8PrE/exec";
#else
                URL_LOG = "https://script.google.com/macros/s/AKfycbyLAKBvXPqzeo8vMsMlLpGwKacQkYuKuw9Kq6MOEIMHzR27Lw/exec";
#endif
                URL_CONFIG = "https://script.google.com/macros/s/AKfycbxdcJI3rMNv_55bEUf75Bpeu60A3XhQkgXAppu_wlqW409m6w/exec";
                URL_SERVER_CHECK = "https://script.google.com/macros/s/AKfycbyXHYLCNM6rynSiuM5JJozAnCQI-fDQ--Dnetvm7z27oAK6Og/exec";
                URL_ERROR_LOG = "https://script.google.com/macros/s/AKfycbw8sHytkQ6RexTwncFp1xSFqzYWA_WOfXfwbHuDBIGe_fUPHA/exec";
                URL_RANK = "https://script.google.com/macros/s/AKfycbyr-r9ezPy1gTyjX8GUEaaSHqgE6qZ8NrrsN7NwLTctgB3cDSQ/exec";
                URL_USER_CHA = "https://script.google.com/macros/s/AKfycbzJK7fZSkq8vGbETS5cGwi_LEBL7_xXJjqXwgq4GWlBVUMdvTI/exec";
                URL_USER_INVEN = "https://script.google.com/macros/s/AKfycbwiEwbcQPYkYg11jIJy1mpVG_N8AU30WsdsjjpAOpqXrIbZ0Ts/exec";
                URL_USER_MAP = "https://script.google.com/macros/s/AKfycbxF4wOUhQ2TEb-UdfXuKBpWNH3Nixxenb3EWgSAZh1WvgIXSw/exec";
                URL_NOTICE = "https://script.google.com/macros/s/AKfycbxa26DVekIMYQzHNzmFUhuPlXvQrTUWOtpGZcC3pXCvSkHl/exec";
                URL_REWARD = "https://script.google.com/macros/s/AKfycbybIqBOUIuymi_Fuiv_OfLheYTmcLghlv1BCcADijg4PzEZmSU/exec";
                URL_COUPON_1 = "https://script.google.com/macros/s/AKfycbxM8yfHFQ3y3Hhdi9ZsCsdnDBnyMkMcqvTsOz7k9Wjo2RbQ0_w/exec";
                URL_COUPON_2 = "https://script.google.com/macros/s/AKfycbwg5c1jOB9r1aoY1Pzm3d457XmkSFxtgHZajEVgc8hNgvgS9Q/exec";
                URL_COUPON_3 = "https://script.google.com/macros/s/AKfycbwCsOM8D4cjJNNw1XCoIkpY9jKub9CHvKOZlZMynq1stABBy7w/exec";
                URL_COUPON_4 = "https://script.google.com/macros/s/AKfycbxHbP-0nhb2CPhNDb85BYscrfoIW24VytbHbesziXCePtx0C6w/exec";
                URL_GOODS = "https://script.google.com/macros/s/AKfycbz5jZvI-hLVGUaFJ1qFRIKbEmdTS8_EU4hhuYz9s7IBHfXckg/exec";
                URL_ARENA = "https://script.google.com/macros/s/AKfycbxXuu9rqdTQl_5tK3AXZuxxejdloDadJvRtFq5lXTo3nSQC3us/exec";
                URL_ALOG = "https://script.google.com/macros/s/AKfycbxHn-yRyoDV3N--rDC4GEdUtlyqZHanxSohQGPFknCe52yuzg/exec";
                URL_RAID = "https://script.google.com/macros/s/AKfycbw6Rz9EmuYUHtn9iR3U2zaKO9HtssczUtJTJg86-y3ogQQLNg/exec";
                break;
        }
    }

    public static void SignIn(string _email, string _pw, Action<int,string> _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);

        string pw = EncryptedPlayerPrefs.Md5(_pw);
        string _url = string.Format("{0}?CODE={1}&EMAIL={2}&PW={3}&KEY={4}", 
            Instance.URL_ACCOUNT, (int)FuncCode.SIGN_IN, _email, pw, GameProcess.GetGameDataManager().GetToken());

        Instance.httpManager.CallRequest(_url, true, delegate (WWW www)
        {
            IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
            if (CheckResponse(dict))
            {
                int guid = 0;
                string timeStr = "";

                if (dict.Contains("guid")) guid = Convert.ToInt32(dict["guid"]);
                if (dict.Contains("tick")) timeStr = Convert.ToString(dict["tick"]);

                if (_callback != null) _callback(guid, timeStr);
            }
            else
            {
#if UNITY_EDITOR || DEBUG_MODE
                Debug.LogError(www.text);
#endif
            }
        });
    }

    public static void CreateAccount(string _email, string _pw, Action<int,string> _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);
        SystemLanguage lang = SystemLanguage.Korean;
#if UNITY_EDITOR
        lang = GameProcess.Instance.Language;
#else
        lang = Application.systemLanguage;
#endif
        string pw = EncryptedPlayerPrefs.Md5(_pw);
        string _url = string.Format("{0}?CODE={1}&EMAIL={2}&PW={3}&KEY={4}&LANG={5}",
            Instance.URL_ACCOUNT,(int)FuncCode.CREATE_ACCOUNT,_email,pw, GameProcess.GetGameDataManager().GetToken(),lang.ToString());
        Instance.httpManager.CallRequest(_url, true, delegate (WWW www)
         {
             IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
             if(CheckResponse(dict))
             {
                 int guid = 0;
                 string timeStr = "";

                 if (dict.Contains("guid")) guid = Convert.ToInt32(dict["guid"]);
                 if (dict.Contains("tick")) timeStr = Convert.ToString(dict["tick"]);

                 if (_callback != null) _callback(guid, timeStr);
             }
             else
             {
#if UNITY_EDITOR || DEBUG_MODE
                 Debug.LogError(www.text);
#endif
             }
         });
    }

    public static void ChangePw(int _id, string _pw, string _pw2, Action _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);

        string pw = EncryptedPlayerPrefs.Md5(_pw);
        string pw2 = EncryptedPlayerPrefs.Md5(_pw2);
        string _url = string.Format("{0}?CODE={1}&ID={2}&PW={3}&PW2={4}&KEY={5}", 
            Instance.URL_ACCOUNT, (int)FuncCode.CHANGE_PW, _id, pw, pw2, GameProcess.GetGameDataManager().GetToken());
        Instance.httpManager.CallRequest(_url, true, delegate (WWW www)
        {
            IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
            if (CheckResponse(dict))
            {
                if (_callback != null) _callback();
            }
            else
            {
#if UNITY_EDITOR || DEBUG_MODE
                Debug.LogError(www.text);
#endif
            }
        });
    }

    public static void FindPw(string _email, string _pw, string _title, string _msg, Action _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);

        string pw = EncryptedPlayerPrefs.Md5(_pw);
        _title = WWW.EscapeURL(_title);
        _msg = WWW.EscapeURL(_msg);
        string _url = string.Format("{0}?CODE={1}&EMAIL={2}&PW={3}&TITLE={4}&MSG={5}&KEY={6}", 
            Instance.URL_ACCOUNT, (int)FuncCode.FIND_PW, _email, pw, _title, _msg, GameProcess.GetGameDataManager().GetToken());
        Debug.Log(_url);
        Instance.httpManager.CallRequest(_url, true, delegate (WWW www)
        {
            IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
            if (CheckResponse(dict))
            {
                if (_callback != null) _callback();
            }
            else
            {
#if UNITY_EDITOR || DEBUG_MODE
                Debug.LogError(www.text);
#endif
            }
        });
    }

    public static void BlockId(int _id, string _pw)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);

        string pw = EncryptedPlayerPrefs.Md5(_pw);
        string _url = string.Format("{0}?CODE={1}&ID={2}&PW={3}&KEY={4}",
            Instance.URL_ACCOUNT, (int)FuncCode.BLOCK, _id, pw, GameProcess.GetGameDataManager().GetToken());
        Instance.httpManager.CallRequest(_url, true, delegate (WWW www)
        {
            IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
            if (!CheckResponse(dict))
            {
#if UNITY_EDITOR || DEBUG_MODE
                Debug.LogError(www.text);
#endif
            }
        });
    }

    public static void SelectData(string _guid, Action<string, string> _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);

        string _url = string.Format("{0}?CODE={1}&ID={2}&KEY={3}", 
            Instance.URL_USER_DATA, (int)FuncCode.SELECT_USER_DATA, _guid, GameProcess.GetGameDataManager().GetToken());
        Instance.httpManager.CallRequest(_url, true, delegate (WWW www)
        {
            IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
            if (CheckResponse(dict))
            {
                string data = "";
                string timeStr = "";

                if (dict.Contains("data")) data = Convert.ToString(dict["data"]);
                if (dict.Contains("tick")) timeStr = Convert.ToString(dict["tick"]);

                if (_callback != null) _callback(data, timeStr);
            }
            else
            {
#if UNITY_EDITOR || DEBUG_MODE
                Debug.LogError(www.text);
#endif
            }
        });
    }

    public static void UpdateData(string _guid, string _data, Action<string> _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);

        _data = WWW.EscapeURL(_data);
        string _url = string.Format("{0}?CODE={1}&ID={2}&DATA={3}&KEY={4}", 
            Instance.URL_USER_DATA, (int)FuncCode.UPDATE_USER_DATA, _guid, _data, GameProcess.GetGameDataManager().GetToken());
        Instance.httpManager.CallRequest(_url, false, delegate (WWW www)
        {
            IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
            if (CheckResponse(dict))
            {
                string timeStr = "";                
                if (dict.Contains("tick")) timeStr = Convert.ToString(dict["tick"]);

                if (_callback != null) _callback(timeStr);
            }
            else
            {
                if (_callback != null) _callback("FAIL");
#if UNITY_EDITOR || DEBUG_MODE
                Debug.LogError(www.text);
#endif
            }
        });
    }

    public static void CheckLogServer(Action<string> _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);

        string _url = string.Format("{0}?CODE={1}&KEY={2}", 
            Instance.URL_LOG, (int)FuncCode.CHECK_LOG_SERVER, GameProcess.GetGameDataManager().GetToken());
        Instance.httpManager.CallRequest(_url, true, delegate (WWW www)
        {
            IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
            if (CheckResponse(dict))
            {
                string timeStr = "";
                if (dict.Contains("tick")) timeStr = Convert.ToString(dict["tick"]);

                if (_callback != null) _callback(timeStr);
            }
            else
            {
#if UNITY_EDITOR || DEBUG_MODE
                Debug.LogError(www.text);
#endif
            }
        });
    }

    public static void SaveLog(string _data, Action _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);
        SystemLanguage lang = SystemLanguage.Korean;
#if UNITY_EDITOR
        lang = GameProcess.Instance.Language;
#else
        lang = Application.systemLanguage;
#endif

        _data = WWW.EscapeURL(_data);
        string _url = string.Format("{0}?CODE={1}&LOG={2}&KEY={3}&LANG={4}", 
            Instance.URL_LOG, (int)FuncCode.SAVE_LOG, _data, GameProcess.GetGameDataManager().GetToken(),lang.ToString());
        Instance.httpManager.CallRequest(_url, true, delegate (WWW www)
        {
            IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
            if (CheckResponse(dict))
            {
                if (_callback != null) _callback();
            }
            else
            {
#if UNITY_EDITOR || DEBUG_MODE
                Debug.LogError(www.text);
#endif
            }
        });
    }

    public static void GetConfig(Action<string> _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);
        string _url = string.Format("{0}?CODE={1}&KEY={2}", 
            Instance.URL_CONFIG, (int)FuncCode.GET_CONFIG, GameProcess.GetGameDataManager().GetToken());
        Instance.httpManager.CallRequest(_url, true, delegate (WWW www)
        {
            IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
            if (CheckResponse(dict))
            {
                if (_callback != null) _callback(www.text);
            }
            else
            {
#if UNITY_EDITOR || DEBUG_MODE
                Debug.LogError(www.text);
#endif
            }
        });
    }

    public static void GetServerState(Action<int> _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);
        string _url = string.Format("{0}?CODE={1}&KEY={2}", 
            Instance.URL_SERVER_CHECK, (int)FuncCode.GET_SERVER_STATE, GameProcess.GetGameDataManager().GetToken());
        Instance.httpManager.CallRequest(_url, false, delegate (WWW www)
        {
            IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
            if (CheckResponse(dict))
            {
                int state = -1;
                if (dict.Contains("state")) state = Convert.ToInt32(dict["state"]);
                if (_callback != null) _callback(state);
            }
            else
            {
#if UNITY_EDITOR || DEBUG_MODE
                Debug.LogError(www.text);
#endif
            }
        });
    }

    public static void SaveErrorLog(string _data)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);

        _data = WWW.EscapeURL(_data);
        string _url = string.Format("{0}?CODE={1}&LOG={2}&KEY={3}", 
            Instance.URL_ERROR_LOG, (int)FuncCode.SAVE_ERROR_LOG, _data, GameProcess.GetGameDataManager().GetToken());
        Instance.httpManager.CallRequest(_url, true, null);
    }

    public static void GetRankData(Action<string> _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);

        string _url = string.Format("{0}?CODE={1}&KEY={2}", 
            Instance.URL_RANK, (int)FuncCode.SELECT_RANK, GameProcess.GetGameDataManager().GetToken());
        Instance.httpManager.CallRequest(_url, true, delegate (WWW www)
        {
            IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
            if (CheckResponse(dict))
            {
                string data = "";
                if (dict.Contains("data")) data = Convert.ToString(dict["data"]);

                if (_callback != null) _callback(data);
            }
            else
            {
#if UNITY_EDITOR || DEBUG_MODE
                Debug.LogError(www.text);
#endif
            }
        });
    }

    public static void UpdateRankData(int _id, string _name, int _chaId, int _chaGrade, int _score, string _equip, Action<string> _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);

        _equip = WWW.EscapeURL(_equip);
        string _url = string.Format("{0}?CODE={1}&ID={2}&NAME={3}&CHA_ID={4}&CHA_GRADE={5}&SCORE={6}&EQUIP={7}&KEY={8}", 
            Instance.URL_RANK, (int)FuncCode.UPDATE_RANK, _id, _name, _chaId, _chaGrade, _score, _equip, GameProcess.GetGameDataManager().GetToken());
        Instance.httpManager.CallRequest(_url, true, delegate (WWW www)
        {
            Debug.Log(www.text);
            IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
            if (CheckResponse(dict))
            {
                string data = "";
                if (dict.Contains("data")) data = Convert.ToString(dict["data"]);

                if (_callback != null) _callback(data);
            }
            else
            {
#if UNITY_EDITOR || DEBUG_MODE
                Debug.LogError(www.text);
#endif
            }
        });
    }

    public static void LoadData(string _guid, Action<string> _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);
        LoadMapData(_guid, delegate (string _data)
        {
            if (_callback != null) _callback(_data);
        });
    }

    public static void LoadChaData(string _guid, Action<string> _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);

        string _url = string.Format("{0}?CODE={1}&ID={2}&KEY={3}", Instance.URL_USER_CHA, (int)FuncCode.SELECT_USER_CHA, _guid, GameProcess.GetGameDataManager().GetToken());
        Instance.httpManager.CallRequest(_url, true, delegate (WWW www)
        {
            IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
            if (CheckResponse(dict))
            {
                string data = "";
                if (dict.Contains("data")) data = Convert.ToString(dict["data"]);
                if (_callback != null) _callback(data);
            }
            else
            {
#if UNITY_EDITOR || DEBUG_MODE
                Debug.LogError(www.text);
#endif
            }
        });
    }

    public static void LoadInvenData(string _guid, Action<string> _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);

        string _url = string.Format("{0}?CODE={1}&ID={2}&KEY={3}", 
            Instance.URL_USER_INVEN, (int)FuncCode.SELECT_USER_INVEN, _guid, GameProcess.GetGameDataManager().GetToken());
        Instance.httpManager.CallRequest(_url, true, delegate (WWW www)
        {
            IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
            if (CheckResponse(dict))
            {
                string data = "";
                if (dict.Contains("data")) data = Convert.ToString(dict["data"]);

                if (_callback != null) _callback(data);
            }
            else
            {
#if UNITY_EDITOR || DEBUG_MODE
                Debug.LogError(www.text);
#endif
            }
        });
    }

    public static void LoadMapData(string _guid, Action<string> _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);

        string _url = string.Format("{0}?CODE={1}&ID={2}&KEY={3}",
            Instance.URL_USER_MAP, (int)FuncCode.SELECT_USER_MAP, _guid, GameProcess.GetGameDataManager().GetToken());
        Instance.httpManager.CallRequest(_url, true, delegate (WWW www)
        {
            IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
            if (CheckResponse(dict))
            {
                string data = "";
                if (dict.Contains("data")) data = Convert.ToString(dict["data"]);

                if (_callback != null) _callback(data);
            }
            else
            {
#if UNITY_EDITOR || DEBUG_MODE
                Debug.LogError(www.text);
#endif
            }
        });
    }

    public static void SendData(string _guid, string _mapData, Action _callback)
    {
        SendMapData(_guid, _mapData, delegate ()
        {
            if (_callback != null) _callback();
        });
    }

    public static void SendChaData(string _guid, string _data, Action _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);
        _data = WWW.EscapeURL(_data);
        string _url = string.Format("{0}?CODE={1}&ID={2}&DATA={3}&KEY={4}",
                                    Instance.URL_USER_CHA,
                                    (int)FuncCode.UPDATE_USER_CHA,
                                    _guid,
                                    _data,
                                    GameProcess.GetGameDataManager().GetToken());
        Instance.httpManager.CallRequest(_url, false, delegate (WWW www)
        {
            IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
            if (CheckResponse(dict))
            {
                if (_callback != null) _callback();
            }
            else
            {
#if UNITY_EDITOR || DEBUG_MODE
                Debug.LogError(www.text);
#endif
            }
        });
    }

    public static void SendInvenData(string _guid, string _data, Action _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);
        _data = WWW.EscapeURL(_data);
        string _url = string.Format("{0}?CODE={1}&ID={2}&DATA={3}&KEY={4}",
                                        Instance.URL_USER_INVEN,
                                        (int)FuncCode.UPDATE_USER_INVEN,
                                        _guid,
                                        _data,
                                        GameProcess.GetGameDataManager().GetToken());
        Instance.httpManager.CallRequest(_url, false, delegate (WWW www)
        {
            IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
            if (CheckResponse(dict))
            {
                if (_callback != null) _callback();
            }
            else
            {
#if UNITY_EDITOR || DEBUG_MODE
                Debug.LogError(www.text);
#endif
            }
        });
    }

    public static void SendMapData(string _guid, string _data, Action _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);
        _data = WWW.EscapeURL(_data);
        string _url = string.Format("{0}?CODE={1}&ID={2}&DATA={3}&KEY={4}",
                                    Instance.URL_USER_MAP,
                                    (int)FuncCode.UPDATE_USER_MAP,
                                    _guid,
                                    _data,
                                    GameProcess.GetGameDataManager().GetToken());
        Instance.httpManager.CallRequest(_url, true, delegate (WWW www)
        {
            IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
            if (CheckResponse(dict))
            {
                if (_callback != null) _callback();
            }
            else
            {
#if UNITY_EDITOR || DEBUG_MODE
                Debug.LogError(www.text);
#endif
            }
        });
    }

    public static void GetNotice(Action<string> _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);

        string _url = string.Format("{0}?CODE={1}&OS={2}&KEY={3}",
            Instance.URL_NOTICE, (int)FuncCode.SELECT_NOTICE, (int)GameProcess.OS, GameProcess.GetGameDataManager().GetToken());
        Instance.httpManager.CallRequest(_url, true, delegate (WWW www)
        {
            IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
            if (CheckResponse(dict))
            {
                string data = "";
                if (dict.Contains("data")) data = Convert.ToString(dict["data"]);

                if (_callback != null) _callback(data);
            }
        });
    }

    public static void GetReward(int _guid, int _rewarded, Action<string> _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);

        string _url = string.Format("{0}?CODE={1}&ID={2}&REWARDED={3}&KEY={4}",
            Instance.URL_REWARD, (int)FuncCode.SELECT_REWARD, _guid, _rewarded, GameProcess.GetGameDataManager().GetToken());
        Instance.httpManager.CallRequest(_url, true, delegate (WWW www)
        {
            IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
            if (CheckResponse(dict))
            {
                string data = "";
                if (dict.Contains("data")) data = Convert.ToString(dict["data"]);

                if (_callback != null) _callback(data);
            }
        });
    }

    public static void GetCouponCond(int _serverId, string _id, Action<IDictionary> _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);

        string serverUrl = "";
        switch (_serverId)
        {
            case 0: serverUrl = Instance.URL_COUPON_1; break;
            case 1: serverUrl = Instance.URL_COUPON_2; break;
            case 2: serverUrl = Instance.URL_COUPON_3; break;
            case 3: serverUrl = Instance.URL_COUPON_4; break;
        }

        string _url = string.Format("{0}?CODE={1}&ID={2}&KEY={3}",
            serverUrl, (int)FuncCode.GET_COUPON_COND, _id, GameProcess.GetGameDataManager().GetToken());
        Instance.httpManager.CallRequest(_url, true, delegate (WWW www)
        {
            IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
            if (CheckResponse(dict))
            {
                if (_callback != null) _callback(dict);
            }
            else
            {
#if UNITY_EDITOR || DEBUG_MODE
                Debug.LogError(www.text);
#endif
            }
        });
    }

    public static void GetCouponReward(int _serverId, string _id, Action<IDictionary> _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);

        string serverUrl = "";
        switch(_serverId)
        {
            case 0: serverUrl = Instance.URL_COUPON_1; break;
            case 1: serverUrl = Instance.URL_COUPON_2; break;
            case 2: serverUrl = Instance.URL_COUPON_3; break;
            case 3: serverUrl = Instance.URL_COUPON_4; break;
        }

        string _url = string.Format("{0}?CODE={1}&ID={2}&KEY={3}",
            serverUrl, (int)FuncCode.GET_COUPON_REWARD, _id, GameProcess.GetGameDataManager().GetToken());
        Instance.httpManager.CallRequest(_url, true, delegate (WWW www)
        {
            IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
            if (CheckResponse(dict))
            {
                if (_callback != null) _callback(dict);
            }
            else
            {
#if UNITY_EDITOR || DEBUG_MODE
                Debug.LogError(www.text);
#endif
            }
        });
    }

    public static void GetGoodsList(Action<string> _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);

        string _url = string.Format("{0}?CODE={1}&KEY={2}",
            Instance.URL_GOODS, (int)FuncCode.SELECT_GOODS, GameProcess.GetGameDataManager().GetToken());
        Instance.httpManager.CallRequest(_url, true, delegate (WWW www)
        {
            IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
            if (CheckResponse(dict))
            {
                string data = "";
                if (dict.Contains("data")) data = Convert.ToString(dict["data"]);

                if (_callback != null) _callback(data);
            }
        });
    }

    public static void RollArena(int _id, string _name, string _chaInfo, string _param, string _equip, Action _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);

        _equip = WWW.EscapeURL(_equip);
        string _url = string.Format("{0}?CODE={1}&ID={2}&NAME={3}&CHA_INFO={4}&ABILITY={5}&EQUIP={6}&KEY={7}",
            Instance.URL_ARENA, (int)FuncCode.ADD_ARENA, _id, _name, _chaInfo, _param, _equip, GameProcess.GetGameDataManager().GetToken());

        Instance.httpManager.CallRequest(_url, true, delegate (WWW www)
        {
            IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
            if (CheckResponse(dict))
            {
                if (_callback != null) _callback();
            }
            else
            {
#if UNITY_EDITOR || DEBUG_MODE
                Debug.LogError(www.text);
#endif
            }
        });
    }

    public static void GetArenaData(int _id, Action<string> _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);

        string _url = string.Format("{0}?CODE={1}&ID={2}&KEY={3}",
            Instance.URL_ARENA, (int)FuncCode.GET_ARENA, _id, GameProcess.GetGameDataManager().GetToken());
        Instance.httpManager.CallRequest(_url, true, delegate (WWW www)
        {
            IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
            if (CheckResponse(dict))
            {
                string data = "";
                if (dict.Contains("data")) data = Convert.ToString(dict["data"]);

                if (_callback != null) _callback(data);
            }
            else
            {
#if UNITY_EDITOR || DEBUG_MODE
                Debug.LogError(www.text);
#endif
            }
        });
    }

    public static void SendWinArena(int _id, string _name, string _chaInfo, string _param, string _equip, Action<int> _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);

        _equip = WWW.EscapeURL(_equip);
        string _url = string.Format("{0}?CODE={1}&ID={2}&NAME={3}&CHA_INFO={4}&ABILITY={5}&EQUIP={6}&KEY={7}",
            Instance.URL_ARENA, (int)FuncCode.WIN_ARENA, _id, _name, _chaInfo, _param, _equip, GameProcess.GetGameDataManager().GetToken());
        Instance.httpManager.CallRequest(_url, false, delegate (WWW www)
        {
            Debug.Log(www.text);
            IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
            if (CheckResponse(dict))
            {
                int score = 0;
                if (dict.Contains("data")) score = Convert.ToInt32(dict["data"]);

                if (_callback != null) _callback(score);
            }
            else
            {
#if UNITY_EDITOR || DEBUG_MODE
                Debug.LogError(www.text);
#endif
            }
        });
    }

    public static void SendLoseArena(int _id, string _name, string _chaInfo, string _param, string _equip, Action<int> _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);

        _equip = WWW.EscapeURL(_equip);
        string _url = string.Format("{0}?CODE={1}&ID={2}&NAME={3}&CHA_INFO={4}&ABILITY={5}&EQUIP={6}&KEY={7}",
            Instance.URL_ARENA, (int)FuncCode.LOSE_ARENA, _id, _name, _chaInfo, _param, _equip, GameProcess.GetGameDataManager().GetToken());
        Instance.httpManager.CallRequest(_url, false, delegate (WWW www)
        {
            Debug.Log(www.text);
            IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
            if (CheckResponse(dict))
            {
                int score = 0;
                if (dict.Contains("data")) score = Convert.ToInt32(dict["data"]);

                if (_callback != null) _callback(score);
            }
            else
            {
#if UNITY_EDITOR || DEBUG_MODE
                Debug.LogError(www.text);
#endif
            }
        });
    }

    public static void GetArenaRank(Action<string> _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);

        string _url = string.Format("{0}?CODE={1}&KEY={2}",
            Instance.URL_ARENA, (int)FuncCode.GET_ARENA_RANK, GameProcess.GetGameDataManager().GetToken());
        Instance.httpManager.CallRequest(_url, true, delegate (WWW www)
        {
            IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
            if (CheckResponse(dict))
            {
                string data = "";
                if (dict.Contains("data")) data = Convert.ToString(dict["data"]);

                if (_callback != null) _callback(data);
            }
            else
            {
#if UNITY_EDITOR || DEBUG_MODE
                Debug.LogError(www.text);
#endif
            }
        });
    }

    public static void SendLog(int _id, string _userId, string _data, Action _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);

        _data = WWW.EscapeURL(_data);
        string _url = string.Format("{0}?CODE={1}&ID={2}&USER_ID={3}&LOG={4}&KEY={5}",
            Instance.URL_ALOG, (int)FuncCode.SEND_LOG, _id, _userId, _data, GameProcess.GetGameDataManager().GetToken());
        Instance.httpManager.CallRequest(_url, false, delegate (WWW www)
        {
            IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
            if (CheckResponse(dict))
            {
                if (_callback != null) _callback();
            }
            else
            { 
#if UNITY_EDITOR || DEBUG_MODE
                Debug.LogError(www.text);
#endif
            }
        });
    }

    public static void GetRaidInfo(Action<string> _callback)
    {
        if (!CheckNetwork()) throw new GameException(GameException.ErrorCode.NoNetwork);

        string _url = string.Format("{0}?CODE={1}&KEY={2}",
            Instance.URL_RAID, (int)FuncCode.GET_RAID_INFO, GameProcess.GetGameDataManager().GetToken());
        Instance.httpManager.CallRequest(_url, false, delegate (WWW www)
        {
            IDictionary dict = MiniJSON.Json.Deserialize(www.text) as IDictionary;
            if (CheckResponse(dict))
            {
                string data = "";
                if (dict.Contains("data")) data = Convert.ToString(dict["data"]);

                if (_callback != null) _callback(data);
            }
        });
    }

    static bool CheckResponse(IDictionary _dict, bool _ignore = false)
    {
        if (_dict == null)
        {
            if(!_ignore) GameProcess.ShowError(new GameException(GameException.ErrorCode.UnknownServerError));
            return false;
        }

        if (!_dict.Contains("result"))
        {
            if (!_ignore) GameProcess.ShowError(new GameException(GameException.ErrorCode.UnknownServerError));
            return false;
        }

        if (_dict["result"].Equals("fail") || _dict["result"].Equals("error"))
        {
            if (!_ignore)
            {
                GameException.ErrorCode errCode = (GameException.ErrorCode)Convert.ToInt32(_dict["code"]);
                GameProcess.ShowError(new GameException(errCode));
            }
            return false;
        }

        return true;
    }

    static bool CheckNetwork()
    {
        switch(Application.internetReachability)
        {
            case NetworkReachability.NotReachable: return false;
            default: return true;
        }
    }
}
