﻿using System;
using System.Collections;
using System.Diagnostics;

public class CheckerLootingForAndroid
{
    ProcessStartInfo info;

    public CheckerLootingForAndroid()
    {
        info = new System.Diagnostics.ProcessStartInfo("cmd.exe");
        info.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
        info.CreateNoWindow = true;
        info.UseShellExecute = false;
        info.RedirectStandardOutput = true;
        info.RedirectStandardInput = true;
        info.RedirectStandardError = true;
    }

    public bool CheckLooting()
    {
        bool isLootedPhone = false;
        Process cmd = new Process();
        try
        {
            cmd.StartInfo = info;
            cmd.Start();
            cmd.StandardInput.Write(@"su"+Environment.NewLine);
            cmd.StandardInput.Close();
            string result = cmd.StandardError.ReadToEnd();
            if (result.Equals(string.Empty)) isLootedPhone = true;
        }
        catch (Exception e)
        {
#if UNITY_EDITOR || DEBUG_MODE
            UnityEngine.Debug.LogError(e);
#endif
            isLootedPhone = false;
        }
        finally
        {
            cmd.Close();
        }

        return isLootedPhone;
    }
}
