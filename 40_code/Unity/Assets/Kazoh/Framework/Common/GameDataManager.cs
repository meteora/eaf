﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;

using Kazoh.Table;
using MiniJSON;

public class GameDataManager
{
    public Action ChangedCurCharacterEvent;
    public Action ChangeUserMapListEvent;
    public Action AddedInventorySizeEvent;
    public Action<Data_User> ChangedChaSlotEvent;
    public Action NewMapEvent;
    public Action NewChaEvent;
    public Action NewItemEvent;

    public Data_User UserData { get; private set; }
    public bool HasNoCharacter
    {
        get
        {
            if (chaSlotList == null) return true;
            for(int i=0; i < chaSlotList.Count; ++i)
            {
                if (!chaSlotList[i].IsEmpty) return false;
            }
            return true;
        }
    }

    public Slot_Character CurCharacterSlot
    {
        get
        {
            return chaSlotList[curChaIdx];
        }
    }

    public int CurCharacterSlotId
    {
        get
        {
            return curChaIdx;
        }
    }
    public int CurExp { get; private set; }
    public int CurLevel { get; private set; }
    public int CurMaxExp
    {
        get
        {
            if (CurLevel >= exps.Length) return exps[exps.Length - 1];
            return exps[CurLevel];
        }
    }
    public float CurExpPer { get { return CurExp / (CurMaxExp + 0f); } }
    public int ExpRewardNum { get { return UserData.ExpReward; } }
    public bool IsPrimium { get; private set; }
    private int[] exps;

    private int curChaIdx;
    private List<Slot_Character> chaSlotList;
    private List<Slot_Item> itemSlotList;
    private List<Slot_Item> tempItemSlotList;
    private List<Data_UserMap> listUserMap;

    private List<int> listClearMission;
    private List<int> listIapHistory;
    private DateTime logInTime;
    private string userId;
    private int guid;

    private string keyUserInfo;
    private string keyUserCha;
    private string keyUserInven;
    private string keyUserMap;

    private int lastPlayedMapId;

    private DataCheckManager cm;

    public void Init()
    {
#if PRIMIUM
        IsPrimium = true;
#endif
#if DEBUG_MODE || UNITY_EDITOR
        Debug.Log("[GameDataManager] 게임 데이터 매니저 초기화!!!");
        if (IsPrimium) Debug.Log("프리미엄 버전");
        else Debug.Log("일반 버전");
#endif
    }

    Action loadingCompleteEvent;
    public void SignIn(int _guid = 0, string _email = "", string _timeStr = "", Action _callback = null)
    {
        SetGuid(_guid, _email);
        Load(_timeStr, _callback);
    }

    public void SignUp(int _guid = 0, string _email = "", string _timeStr = "", Action _callback = null)
    {
        if (EncryptedPlayerPrefs.HasKey(keyUserInfo)) EncryptedPlayerPrefs.DeleteKey(keyUserInfo);
        if (EncryptedPlayerPrefs.HasKey(keyUserInven)) EncryptedPlayerPrefs.DeleteKey(keyUserInven);
        if (EncryptedPlayerPrefs.HasKey(keyUserCha)) EncryptedPlayerPrefs.DeleteKey(keyUserCha);
        if (EncryptedPlayerPrefs.HasKey(keyUserMap)) EncryptedPlayerPrefs.DeleteKey(keyUserMap);
        SignIn(_guid, _email, _timeStr, _callback);
    }

    void Load(string _timeStr, Action _callback)
    {
        try
        {
            loadingCompleteEvent = _callback;
            
            SetLogInTime(_timeStr);

            UpdateUserInfo();
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    void SetGuid(int _guid, string _email)
    {
        guid = _guid;
        userId = EncryptedPlayerPrefs.Md5(_guid + _email);
        
        keyUserInfo = userId +  EncryptedPlayerPrefs.userKeys[2];
        keyUserCha = userId + EncryptedPlayerPrefs.userKeys[3];
        keyUserInven = userId + EncryptedPlayerPrefs.userKeys[4];
        keyUserMap = userId + EncryptedPlayerPrefs.userKeys[5];
    }

    void UpdateUserInfo()
    {
        if (EncryptedPlayerPrefs.HasKey(keyUserInfo))
        {
            DBManager.UpdateData(userId, EncryptedPlayerPrefs.GetString(keyUserInfo), delegate (string _time)
            {
                if(!_time.Equals("FAIL")) EncryptedPlayerPrefs.DeleteKey(keyUserInfo);
                SelectUserInfo();
            });
        }
        else
        {
            SelectUserInfo();
        }
    }

    Dictionary<string, IDictionary> dicData = new Dictionary<string, IDictionary>();
    IList listMap = null;
    void SelectUserInfo()
    {
        DBManager.SelectData(userId, delegate (string _data, string _time)
        {
            if (!string.IsNullOrEmpty(_data))
            {
                dicData.Add("USER_INFO", Json.Deserialize(_data) as IDictionary);
                SelectInvenData();
            }
            else SetGameData();
        });
    }

    void SelectInvenData()
    {
        DBManager.LoadInvenData(userId, delegate (string _data)
        {
            if (!string.IsNullOrEmpty(_data))
            {
                Dictionary<string, object> dicItem = new Dictionary<string, object>();
                IList list = MiniJSON.Json.Deserialize(_data) as IList;
                foreach (string csv in list)
                {
                    if (!string.IsNullOrEmpty(csv))
                    {
                        string[] arr = csv.Split(',');
                        if (arr.Length == 6 && Convert.ToInt32(arr[0]) < 100) dicItem.Add(arr[0], Data_UserItem.GetHashFromCSV(csv));
                    }
                }
                string _json = Json.Serialize(dicItem);
                dicData.Add("INVENTORY", Json.Deserialize(_json) as IDictionary);
            }

            SelectChaData();

        });
    }

    void SelectChaData()
    {
        DBManager.LoadChaData(userId, delegate (string _data)
        {
            if (!string.IsNullOrEmpty(_data))
            {
                Dictionary<string, object> dicCha = new Dictionary<string, object>();
                IList list = MiniJSON.Json.Deserialize(_data) as IList;
                foreach (string csv in list)
                {
                    if (!string.IsNullOrEmpty(csv))
                    {
                        string[] arr = csv.Split(',');
                        if (arr.Length == 11) dicCha.Add(arr[0], Data_UserCharacter.GetHashFromCSV(csv));
                    }
                }
                string _json = Json.Serialize(dicCha);
                dicData.Add("CHARACTER", Json.Deserialize(_json) as IDictionary);
            }
            else if (EncryptedPlayerPrefs.HasKey(keyUserCha))
            {
                string _json = EncryptedPlayerPrefs.GetString(keyUserCha);
                if (!string.IsNullOrEmpty(_json)) dicData.Add("CHARACTER", Json.Deserialize(_json) as IDictionary);
            }

            SelectMapData();

        });
    }

    void SelectMapData()
    {
        if (EncryptedPlayerPrefs.HasKey(keyUserMap))
        {
            string _json = EncryptedPlayerPrefs.GetString(keyUserMap);
            if (!string.IsNullOrEmpty(_json)) listMap = Json.Deserialize(_json) as IList;
        }
        SetGameData();
    }

    void SetGameData()
    {
        try
        {
            /* 유저 데이터 세팅 */
            if (dicData.ContainsKey("USER_INFO")) SetUserData(dicData["USER_INFO"]);
            else SetUserData(null);

            /* 유저 아이템 데이터 세팅 */
            if (dicData.ContainsKey("INVENTORY")) SetUserItemData(UserData.InventorySlotNum, dicData["INVENTORY"]);
            else SetUserItemData(UserData.InventorySlotNum, null);

            /* 유저 캐릭터 데이터 세팅 */
            if (dicData.ContainsKey("CHARACTER")) SetUserChaData(UserData.ChaSlotNum, dicData["CHARACTER"]);
            else SetUserChaData(UserData.ChaSlotNum, null);

            /* 유저 맵 데이터 세팅 */
            SetUserMapData(listMap);

            /* 유저 미션 데이터 세팅 */
            SetMissionData();

            /* 구매제한 상품 구매 내역 세팅 */
            if (dicData.ContainsKey("USER_INFO")) SetIapHistory(dicData["USER_INFO"]);
            else SetIapHistory(null);

            /* 마지막 플레이 맵 세팅 */
            if (dicData.ContainsKey("USER_INFO")) SetLastMapId(dicData["USER_INFO"]);

            /* 경험치 세팅 */
            SetExp();

            string keyPw = EncryptedPlayerPrefs.userKeys[1];
            if (EncryptedPlayerPrefs.HasKey(keyPw))
            {
                cm = new DataCheckManager(UserData, guid, userId, EncryptedPlayerPrefs.GetString(keyPw));
            }

#if UNITY_EDITOR || DEBUG_MODE
            Debug.Log("게임 데이터 로딩 완료!!!");
#endif

            if (loadingCompleteEvent != null) loadingCompleteEvent();
        }
        catch (GameException e)
        {
            GameProcess.ShowError(e);
        }
        catch (Exception e)
        {
            GameProcess.ShowError(new GameException(e));
        }
    }

    public void Save()
    {
        try
        {
            if (UserData == null)
            {
#if UNITY_EDITOR
                Debug.LogError("[GameDataManager] 유저 데이터가 생성되지 않아 데이터를 저장할 수 없습니다.");
#endif
                return;
            }

            ArrayList listMap = new ArrayList();
            foreach (Data_UserMap map in listUserMap)
            {
                listMap.Add(map.GetHash());
            }
            EncryptedPlayerPrefs.SetString(keyUserMap, Json.Serialize(listMap));

            Dictionary<string, object> hash = new Dictionary<string, object>();
            hash.Add("USER_DATA", UserData.GetHash());
            hash.Add("IAP_HISTORY", listIapHistory.ToArray());
            hash.Add("LAST_MAP", lastPlayedMapId);
            hash.Add("CUR_CHA", curChaIdx);
            hash.Add("BUILD", IsPrimium ? 1 : 0);

            string data = Json.Serialize(hash);
            EncryptedPlayerPrefs.SetString(keyUserInfo, data);

            DBManager.UpdateData(userId, data, delegate (string _timeStr)
            {
                if (!_timeStr.Equals("FAIL")) EncryptedPlayerPrefs.DeleteKey(keyUserInfo);
#if UNITY_EDITOR
                Debug.Log("SAVE: " + data);
#endif
            });

            ArrayList itemArray = new ArrayList();
            for (int i = 0; i < itemSlotList.Count; ++i)
            {
                if (itemSlotList[i].IsEmpty) continue;
                itemArray.Add(itemSlotList[i].Data.GetCVS(itemSlotList[i].SlotId));
            }

            string itemData = Json.Serialize(itemArray);
            DBManager.SendInvenData(userId, itemData, delegate ()
            {
#if UNITY_EDITOR
                Debug.Log("SAVE: " + itemData);
#endif
            });

            ArrayList chaArray = new ArrayList();
            for (int i = 0; i < chaSlotList.Count; ++i)
            {
                if (chaSlotList[i].IsEmpty) continue;
                chaArray.Add(chaSlotList[i].Data.GetCVS(i));
            }

            string chaData = Json.Serialize(chaArray);
            DBManager.SendChaData(userId, chaData, delegate ()
            {
                if (EncryptedPlayerPrefs.HasKey(keyUserCha)) EncryptedPlayerPrefs.DeleteKey(keyUserCha);
#if UNITY_EDITOR
                Debug.Log("SAVE: " + chaData);
#endif
            });
        }
        catch (GameException e)
        {
            GameProcess.ShowError(e);
        }
        catch (Exception e)
        {
            GameProcess.ShowError(new GameException(e));
        }
    }

    #region // Test
    public void TestSetUserData(int _gold, int _coin, int _key, int _cash, int _slot, int _inven)
    {
        UserData = new Data_User(_gold, _coin, _key, _cash, _slot, _inven);
        Debug.Log("게임 데이터 매니저 초기화!!!");
    }

    public void TestMapClear(int _id, int _record)
    {
        listUserMap.Add(new Data_UserMap(_id, _record, false));
    }
    #endregion // Test

    #region // Set
    void SetUserData(IDictionary dict)
    {
        /* 유저 데이터 세팅 */
        if (dict == null || !dict.Contains("USER_DATA"))
        {
            UserData = new Data_User();
        }
        else
        {
            IDictionary dict2 = dict["USER_DATA"] as IDictionary;
            if (dict2 == null) UserData = new Data_User();
            else UserData = new Data_User(dict2);
        }

        if (dict != null && dict.Contains("CUR_CHA")) curChaIdx = Convert.ToInt32(dict["CUR_CHA"]);

        int build = 0;
        if (dict != null && dict.Contains("BUILD")) build = Convert.ToInt32(dict["BUILD"]);
        if (IsPrimium)
        {
            if (build == 0)
            {
                UserData.AddCoin(1000);
                UserData.AddGold(10000000);
            }
        }
        else if (build == 1)
        {
            throw new GameException(GameException.ErrorCode.NoMatchBuild);
        }

#if UNITY_EDITOR || DEBUG_MODE
        Debug.Log("[GDM] 유저 데이터 세팅 완료!!");
#endif
    }

    void SetUserChaData(int _slotNum, IDictionary dict)
    {
        chaSlotList = new List<Slot_Character>();
        for (int i = 0; i < _slotNum; ++i)
        {
            chaSlotList.Add(new Slot_Character());
            chaSlotList[i].ChangedEvent += OnChangeChaSlot;
        }

        if (dict != null)
        {
            for (int i = 0; i < chaSlotList.Count; ++i)
            {
                if (dict.Contains("" + i))
                {
                    IDictionary dict2 = dict["" + i] as IDictionary;
                    if (dict2 != null) chaSlotList[i].Put(dict2);
                }
            }
        }

        if (CurCharacterSlot.IsEmpty) curChaIdx = 0;

#if UNITY_EDITOR || DEBUG_MODE
        Debug.Log("[GDM] 유저 캐릭터 데이터 세팅 완료!!");
#endif
    }

    void SetUserItemData(int _slotNum, IDictionary dict)
    {
        tempItemSlotList = new List<Slot_Item>();
        itemSlotList = new List<Slot_Item>();
        for (int i = 0; i < _slotNum; ++i)
        {
            itemSlotList.Add(new Slot_Item(i));
        }

        if (dict != null)
        {
            for (int i = 0; i < itemSlotList.Count; ++i)
            {
                if (dict.Contains("" + i))
                {
                    IDictionary dict3 = dict["" + i] as IDictionary;
                    if (dict3 != null) itemSlotList[i].Put(dict3);
                }
            }
        }

#if UNITY_EDITOR || DEBUG_MODE
        Debug.Log("[GDM] 유저 아이템 데이터 세팅 완료!!");
#endif
    }

    void SetUserMapData(IDictionary dict)
    {
        listUserMap = new List<Data_UserMap>();
        if (dict != null && dict.Contains("MAP_LIST"))
        {
            IList list = dict["MAP_LIST"] as IList;
            foreach (IDictionary dict2 in list)
            {
                Data_UserMap map = new Data_UserMap(dict2);
                listUserMap.Add(map);
            }
        }

#if UNITY_EDITOR
        Debug.Log("[GDM] 유저 맵 데이터 세팅 완료!!");
#endif
    }

    void SetUserMapData(IList list)
    {
        listUserMap = new List<Data_UserMap>();
        if (list != null)
        {
            foreach (IDictionary dict in list)
            {
                Data_UserMap map = new Data_UserMap(dict);
                listUserMap.Add(map);
            }
        }

#if UNITY_EDITOR || DEBUG_MODE
        Debug.Log("[GDM] 유저 맵 데이터 세팅 완료!!");
#endif
    }

    void SetMissionData()
    {
        listClearMission = new List<int>();
        if(listUserMap != null)
        {
            List<TableData_Mission> ml = TableManager.GetAllMission();
            foreach(TableData_Mission mission in ml)
            {
                Data_UserMap map = listUserMap.Find(x => x.DataId == mission.MapId);
                if(map != null && map.Record > 0) listClearMission.Add(mission.Id);
            }
        }

#if UNITY_EDITOR || DEBUG_MODE
        Debug.Log("[GDM] 유저 미션 데이터 세팅 완료!!");
#endif
    }

    void SetIapHistory(IDictionary dict)
    {
        listIapHistory = new List<int>();
        if (dict != null && dict.Contains("IAP_HISTORY"))
        {
            IList list = dict["IAP_HISTORY"] as IList;
            foreach (int productId in list)
            {
                listIapHistory.Add(productId);
            }
        }

#if UNITY_EDITOR || DEBUG_MODE
        Debug.Log("[GDM] 구매제한 상품 구매 내역 세팅 완료!!");
#endif
    }

    void SetLastMapId(IDictionary dict)
    {
        if (dict != null && dict.Contains("LAST_MAP"))
        {
            lastPlayedMapId = Convert.ToInt32(dict["LAST_MAP"]);
        }

#if UNITY_EDITOR || DEBUG_MODE
        Debug.Log("[GDM] 마지막 플레이 맵 ID 세팅 완료!!");
#endif
    }

    void SetExp()
    {
        List<TableData_Exp> expList = TableManager.GetExpList();
        exps = new int[expList.Count];
        for (int i = 0; i < expList.Count; ++i)
        {
            exps[expList[i].Id - 1] = expList[i].Exp;
        }
        CurExp = UserData.Exp;
        while(CurExp > CurMaxExp)
        {
            CurExp -= CurMaxExp;
            CurLevel++;
        }
    }
#endregion // Set


#region // Character

    public List<Slot_Character> GetCharacterSlot()
    {
        List<Slot_Character> list = new List<Slot_Character>();
        list.AddRange(chaSlotList);
        return list;
    }

    public Slot_Character GetCurCharacter()
    {
        if (chaSlotList != null) return CurCharacterSlot;
        return new Slot_Character();
    }

    public int GetCharacterNum()
    {
        if (chaSlotList != null) return chaSlotList.FindAll(x => x.IsEmpty == false).Count;
        return 0;
    }

    public bool HasNewCharacter()
    {
        Slot_Character slot = chaSlotList.Find(x => x.IsNew);
        return slot == null ? false : true;
    }

    public void ConformCharacter()
    {
        for(int i=0; i < chaSlotList.Count; ++i)
        {
            chaSlotList[i].SetNew();
        }
    }

    public void SelectCharacter(Slot_Character _slot)
    {
        if (_slot == null)
        {
            throw new GameException(GameException.ErrorCode.InvalidParam);
        }
        else if (_slot.IsEmpty)
        {
            throw new GameException(GameException.ErrorCode.CanNotSelectCharacter);
        }
        else
        {
            try
            {
                int idx = chaSlotList.IndexOf(_slot);
                if (idx < 0) throw new GameException(GameException.ErrorCode.InvalidParam);
                else
                {
                    curChaIdx = idx;
                    OnChangeCurCharacter();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }

    public void AddCharacter(int _id, Slot_Character _slot = null)
    {
        if (_slot == null || !_slot.IsEmpty)
        {
            _slot = chaSlotList.Find(x => x.IsEmpty);
        }

        if (_slot == null)
        {
            throw new GameException(GameException.ErrorCode.ChaSlotIsFull);
        }
        else
        {
            TableData_Character data = TableManager.GetGameData(_id) as TableData_Character;
            _slot.Put(data);
            _slot.SetNew();
        }
    }

    public TableData_Character[] SummonCharater(TableData_GoodsList _data, Slot_Character _slot = null)
    {
        if (UserData.Coin < _data.Price) throw new GameException(GameException.ErrorCode.NotEnoughCoin);
        if (_data.Num < 1) throw new GameException(GameException.ErrorCode.InvalidParam);

        List<Slot_Character> slotList = new List<Slot_Character>();
        if (_slot != null && _slot.IsEmpty) slotList.Add(_slot);
        for(int i=0; i < chaSlotList.Count; ++i)
        {
            if (slotList.Count == _data.Num) break;
            if (slotList.Contains(chaSlotList[i])) continue;
            if (chaSlotList[i].IsEmpty) slotList.Add(chaSlotList[i]);
        }

        if (slotList.Count < _data.Num)
        {
            throw new GameException(GameException.ErrorCode.ChaSlotIsFull);
        }
        else
        {
            try
            {
                TableData_Character[] data = new TableData_Character[slotList.Count];
                for(int i=0; i<data.Length; ++i)
                {
                    data[i] = Summon(_data);
                    slotList[i].Put(data[i]);
                }
                UserData.AddCoin((-1) * _data.Price);
                OnNewCharacter();
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }

    TableData_Character Summon(TableData_GoodsList _data)
    {
        List<TableData_Character> list = new List<TableData_Character>();
        for (int i = 0; i < _data.SummonList.Count; ++i)
        {
            TableData_Character cha = TableManager.GetGameData(_data.SummonList[i]) as TableData_Character;
            if (cha != null) list.Add(cha);
        }

        if (list.Count == 0) throw new GameException(GameException.ErrorCode.InvalidParam);

        int dice = UnityEngine.Random.Range(0, 10000);
        dice %= list.Count;
        return list[dice];
    }

    public void DeleteCharacter(Slot_Character _slot)
    {
        try
        {
            if(_slot == CurCharacterSlot) throw new GameException(GameException.ErrorCode.CanNotDeleteCharacter);
            _slot.Discard();
            OnChangeChaSlot();
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    public string EnchantCharacter(Slot_Character _slot)
    {
        try
        {
            int cost = GameProcess.GetGameConfig().CostEnchantCha;
            if (UserData.Gold < cost) throw new GameException(GameException.ErrorCode.NotEnoughGold);
            string str = _slot.Enchant();
            UserData.AddGold(-cost);
            OnChangeChaSlot();
            OnChangeCurCharacter();
            return str;
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    public string DownGradeCharacter(Slot_Character _slot)
    {
        try
        {
            int cost = GameProcess.GetGameConfig().CostDownGradeCha;
            if (UserData.Coin < cost) throw new GameException(GameException.ErrorCode.NotEnoughCoin);
            string str = _slot.DownGrade();
            UserData.AddCoin(-cost);
            OnChangeChaSlot();
            OnChangeCurCharacter();
            return str;
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    public void AddHp(Slot_Character _cha, int value)
    {
        try
        {
            _cha.AddHp(value);
            OnChangeChaSlot();
            OnChangeCurCharacter();
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    public void ChangeChaSkin(Slot_Character _slot)
    {
        try
        {
            CurCharacterSlot.Data.ChangeSkin(_slot.Data.SkinId);
            _slot.Discard();
            OnChangeChaSlot();
            OnChangeCurCharacter();
        }
        catch (Exception e)
        {
            throw e;
        }
    }
#endregion // Character

#region // Item
    public List<Slot_Item> GetInventory(bool _isSort = false)
    {
        List<Slot_Item> list = new List<Slot_Item>();
        list.AddRange(itemSlotList);
        if (_isSort) list.Sort(delegate (Slot_Item x, Slot_Item y)
         {
             if (x.IsEmpty && y.IsEmpty) return x.SlotId.CompareTo(y.SlotId);
             else if (x.IsEmpty) return 1;
             else if (y.IsEmpty) return -1;

             if (x.IsEquipped && y.IsEquipped)
             {
                 if (x.Data.Data.SlotIdx == y.Data.Data.SlotIdx) return x.ItemId.CompareTo(y.ItemId);
                 else return x.Data.Data.SlotIdx.CompareTo(y.Data.Data.SlotIdx);
             }
             else if (x.IsEquipped) return -1;
             else if (y.IsEquipped) return 1;

             if (x.Data.Data.SlotIdx == y.Data.Data.SlotIdx) return x.ItemId.CompareTo(y.ItemId);
             else return x.Data.Data.SlotIdx.CompareTo(y.Data.Data.SlotIdx);
         });

        return list;
    }

    public Slot_Item GetInventory(int _idx)
    {
        if (itemSlotList != null && _idx < itemSlotList.Count) return itemSlotList[_idx];
        return null;
    }

    public bool HasNewItem()
    {
        Slot_Item slot = itemSlotList.Find(x => x.IsNew);
        return slot == null ? false : true;
    }

    public void CheckNewItem()
    {
        OnNewItem();
    }

    public int GetEmptyItemSlotNum()
    {
        return itemSlotList.FindAll(x => x.IsEmpty).Count;
    }

    public void BuyItem(TableData_SaleList _data)
    {
        try
        {
            TableData_Item _item = TableManager.GetGameData(_data.ItemName) as TableData_Item;
            if (_item == null)
            {
                throw new GameException(GameException.ErrorCode.NoGameData);
            }

            int num = _data.Amount;

            /* 슬롯 여분 확인 */
            if(itemSlotList.Find(x=>x.IsEmpty) == null)
            {
                if (!_item.IsStack) throw new GameException(GameException.ErrorCode.NotEnoughItemSlot);
                List<Slot_Item> list = itemSlotList.FindAll(x=> x.ItemId == _item.Id && x.RestCount > 0);
                if (list == null) throw new GameException(GameException.ErrorCode.NotEnoughItemSlot);
                int sum = 0;
                for(int i=0; i<list.Count; ++i)
                {
                    sum += list[i].RestCount;
                }
                if (sum < num) throw new GameException(GameException.ErrorCode.NotEnoughItemSlot);
            }

            /* 비용 확인 */
            if (_data.Type == TableEnum.MoneyType.Coin)
            {
                int price = num * _item.CoinPrice;
                if (UserData.Coin < price) throw new GameException(GameException.ErrorCode.NotEnoughCoin);

                AddItem(_item, num);
                UserData.AddCoin(-price);
            }
            else
            {
                int price = num * _item.Price;
                if (UserData.Gold < price) throw new GameException(GameException.ErrorCode.NotEnoughGold);

                AddItem(_item, num);
                UserData.AddGold(-price);
            }
            OnNewItem();
        }
        catch (Exception e)
        {
            throw e;
        }

    }

    /// <summary>
    /// 주의!! 여러번 호출될 수 있으므로 해당 함수 안에서는 Save()함수를 호출하지 않는다.
    /// </summary>
    /// <param name="_data"></param>
    /// <param name="num"></param>
    public void AddItem(TableData_Item _data, int num)
    {
        if (_data.IsStack)
        {
            List<Slot_Item> list = itemSlotList.FindAll(x => x.ItemId == _data.Id && x.RestCount > 0);
            if (list != null)
            {
                for(int i=0; i<list.Count; ++i)
                {
                    int n = Mathf.Min(num, list[i].RestCount);
                    list[i].Put(n);
                    num -= n;
                }
            }
        }
        if (num > 0)
        {
            Slot_Item slot = itemSlotList.Find(x => x.IsEmpty);
            if (slot != null)
            {
                slot.Put(_data, num);
            }
            else
            {
                AddItemToTempList(_data, num);
            }
        }
    }

    public void AddItem(Data_Reward.Reward _data)
    {
        TableData_Item _item = TableManager.GetGameData(_data.ItemId) as TableData_Item;
        if (_item != null) AddItem(_item, _data.Num);
        else throw new GameException(GameException.ErrorCode.NoGameData);
    }

    void AddItemToTempList(TableData_Item _data, int num)
    {
        if (_data.IsStack)
        {
            List<Slot_Item> list = tempItemSlotList.FindAll(x => x.ItemId == _data.Id && x.RestCount > 0);
            if (list != null)
            {
                for (int i = 0; i < list.Count; ++i)
                {
                    int n = Mathf.Min(num, list[i].RestCount);
                    list[i].Put(n);
                    num -= n;
                }
            }
        }
        if (num > 0)
        {
            Slot_Item slot = new Slot_Item(-1);
            slot.Put(_data, num);
            tempItemSlotList.Add(slot);
        }
    }

    public void SellItem(Slot_Item _slot, int num)
    {
        try
        {
            int gold = _slot.Data.Data.SalePrice * num;
            if (gold > GameProcess.GetGameConfig().DeltaGold) throw new GameException(GameException.ErrorCode.OverMaxSalePrice);
            int max = GameProcess.GetGameConfig().MaxGold;
            if (UserData.Gold + gold > max) throw new GameException(GameException.ErrorCode.OverMaxGold);
            _slot.SellItem(num);
            UserData.AddGold(gold);
            MoveItemFromTempList();
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    public void EquipItem(Slot_Item _item, Slot_Character _cha)
    {
        try
        {
            _cha.Equip(_item);
            OnChangeChaSlot();
            OnChangeCurCharacter();
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    public void DismountItem(Slot_Item _item)
    {
        try
        {
            List<Slot_Character> list = chaSlotList;
            for (int i = 0; i < list.Count; ++i)
            {
                if (list[i].IsEmpty) continue;
                if (!list[i].Data.EquipSlot.ContainsKey(_item.EquipSlotId)) continue;
                if (list[i].Data.EquipSlot[_item.EquipSlotId] == null) continue;
                if (list[i].Data.EquipSlot[_item.EquipSlotId] == _item)
                {
                    list[i].Dismount(_item.EquipSlotId);
                    _item.Equip(false);
                }
            }
            OnChangeChaSlot();
            OnChangeCurCharacter();
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    public void EnchantItem(Slot_Item _slot)
    {
        try
        {
            int cost = GameProcess.GetGameConfig().CostEnchantItem;
            if (UserData.Gold < cost) throw new GameException(GameException.ErrorCode.NotEnoughGold);

            _slot.Enchant();
            UserData.AddGold(-cost);
            if (itemSlotList.Find(x => x.IsEmpty) != null) MoveItemFromTempList();
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    void MoveItemFromTempList()
    {
        Stack<Slot_Item> stack = new Stack<Slot_Item>(tempItemSlotList);
        while(stack.Count > 0)
        {
            Slot_Item slot = stack.Pop();
            Data_UserItem item = slot.Data;
            int num = slot.Data.Num;

            if (item.Data.IsStack)
            {
                List<Slot_Item> list = itemSlotList.FindAll(x => x.ItemId == slot.ItemId && x.RestCount > 0);
                if (list != null)
                {
                    for (int i = 0; i < list.Count; ++i)
                    {
                        int n = Mathf.Min(num, list[i].RestCount);
                        list[i].Put(n);
                        num -= n;
                    }
                }
            }
            if (num > 0)
            {
                Slot_Item itemSlot = itemSlotList.Find(x => x.IsEmpty);
                if (itemSlot != null)
                {
                    itemSlot.Put(item.Data, num);
                    tempItemSlotList.Remove(slot);
                }
                else
                {
                    slot.UseItem(slot.Data.Num - num);
                }
            }
        }
    }

    public void UseItem(Slot_Item _slot, Slot_Character target)
    {
        try
        {
            if (_slot == null || _slot.IsEmpty || _slot.Data.Data == null) throw new GameException(GameException.ErrorCode.ItemSlotIsEmpty);

            string title = TableManager.GetString("STR_UI_USE");
            string msg = "";
            string text1 = "";
            switch (_slot.Data.Data.EffectType)
            {
                case TableEnum.EffectType.ADD_HP:
                    if (target == null || target.IsEmpty) throw new GameException(GameException.ErrorCode.CharacterSlotIsEmpty);
                    if (target.Data.CurHp == target.Data.MaxHp)
                    {
                        throw new GameException(GameException.ErrorCode.CanNotHeal);
                    }
                    int value = _slot.Data.Data.EffectValue;
                    GameProcess.GetGameDataManager().AddHp(target, value);
                    GameProcess.PlaySound(SOUND_EFFECT.HEAL);
                    break;

                case TableEnum.EffectType.ADD_CHARACTER_SLOT:
                    GameProcess.GetGameDataManager().AddCharacterSlot(true);

                    msg = TableManager.GetString("STR_MSG_USE_3");
                    text1 = TableManager.GetString("STR_UI_OK");
                    GameProcess.ShowPopup(NoticeType.OK, title, msg, text1, null);
                    break;

                case TableEnum.EffectType.ADD_ITEM_SLOT:
                    GameProcess.GetGameDataManager().AddItemSlot(true);

                    msg = TableManager.GetString("STR_MSG_USE_4");
                    text1 = TableManager.GetString("STR_UI_OK");
                    GameProcess.ShowPopup(NoticeType.OK, title, msg, text1, null);
                    break;

                case TableEnum.EffectType.ADD_ITEM:
                    List<TableData_Drop> list = new List<TableData_Drop>();
                    for (int i = 0; i < _slot.Data.Data.DropList.Count; ++i)
                    {
                        TableData_Drop drop = TableManager.GetGameData(_slot.Data.Data.DropList[i]) as TableData_Drop;
                        if (drop == null) break;
                        list.Add(drop);
                    }
                    Data_Reward reward = Data_Reward.DoDrop(list);
                    GameProcess.GetGameDataManager().ExcuteReward(reward);
                    GameProcess.PlaySound(SOUND_EFFECT.COIN);
                    break;

                case TableEnum.EffectType.SUMMON:
                    AddCharacter(_slot.Data.Data.EffectValue);
                    break;

                    // 각성과 환생의 경우에는 별도의 함수에서 처리한다.
            }

            _slot.UseItem();
            MoveItemFromTempList();
        }
        catch(Exception e)
        {
            throw e;
        }
    }

    public string UseChaEnchantItem(Slot_Item _slot, Slot_Character _target)
    {
        try
        {
            if (_target == null || _target.IsEmpty)
                throw new GameException(GameException.ErrorCode.CharacterSlotIsEmpty);

            string str = _target.Enchant();
            _slot.UseItem();
            MoveItemFromTempList();
            OnChangeChaSlot();
            OnChangeCurCharacter();
            return str;
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    public string UseDeGradeItem(Slot_Item _slot, Slot_Character _target)
    {
        try
        {
            if (_target == null || _target.IsEmpty)
                throw new GameException(GameException.ErrorCode.CharacterSlotIsEmpty);

            string str = _target.DownGrade();
            _slot.UseItem();
            MoveItemFromTempList();
            OnChangeChaSlot();
            OnChangeCurCharacter();
            return str;
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    public void UseQuestItem(int _itemId, int _num)
    {
        try
        {
            for (int i = 0; i < itemSlotList.Count; ++i)
            {
                if (itemSlotList[i].IsEmpty) continue;
                if (itemSlotList[i].ItemId != _itemId) continue;
                if (_num < itemSlotList[i].Data.Num)
                {
                    itemSlotList[i].UseItem(_num);
                    _num = 0;
                }
                else
                {
                    int useNum = itemSlotList[i].Data.Num;
                    itemSlotList[i].UseItem(useNum);
                    _num -= useNum;
                }
                if(_num <= 0)
                {
                    MoveItemFromTempList();
                    break;
                }
            }
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    public int GetItemCount(int _itemId)
    {
        try
        {
            int num = 0;
            for (int i = 0; i < itemSlotList.Count; ++i)
            {
                if (itemSlotList[i].IsEmpty) continue;
                if (itemSlotList[i].ItemId != _itemId) continue;
                num += itemSlotList[i].Data.Num;
            }
            return num;
        }
        catch
        {
            return 0;
        }
    }

    public void ExcuteReward(Data_Reward _data)
    {
        int maxGold = GameProcess.GetGameConfig().MaxGold;
        int maxCoin = GameProcess.GetGameConfig().MaxCoin;
        int gold = _data.Gold;
        int coin = _data.Coin;
        if (UserData.Gold + gold > maxGold) gold = maxGold - UserData.Gold;
        if (UserData.Coin + coin > maxCoin) coin = maxCoin - UserData.Coin;
        UserData.AddGold(gold);
        UserData.AddCoin(coin);
        for (int i = 0; i < _data.ItemRewardList.Count; ++i)
        {
            TableData_Item _item = TableManager.GetGameData(_data.ItemRewardList[i].ItemId) as TableData_Item;
            if (_item != null) AddItem(_item, _data.ItemRewardList[i].Num);
        }
        OnNewItem();
    }
#endregion // Item

#region // Map
    public bool IsLockMap(TableData_MapList _data)
    {
        TableData_Map map = TableManager.GetGameData(_data.MapId) as TableData_Map;
        if (map == null) return true;
        if (listUserMap.Find(x => x.DataId == map.Id) != null) return false;

        bool isLock = false;
        for (int i = 0; i < _data.UnlockCondList.Count; ++i)
        {
            if (isLock) break;
            if (_data.UnlockCondList[i].Cond == TableEnum.UnlockType.None) break;
            switch (_data.UnlockCondList[i].Cond)
            {
                case TableEnum.UnlockType.BCrownNum:
                    if (listUserMap.FindAll(x => x.Record > 0).Count < _data.UnlockCondList[i].Value) isLock = true;
                    break;
                case TableEnum.UnlockType.SCrownNum:
                    if (listUserMap.FindAll(x => x.Record > 1).Count < _data.UnlockCondList[i].Value) isLock = true;
                    break;
                case TableEnum.UnlockType.GCrownNum:
                    if (listUserMap.FindAll(x => x.Record > 2).Count < _data.UnlockCondList[i].Value) isLock = true;
                    break;
                case TableEnum.UnlockType.BCrownMapId:
                    if (listUserMap.Find(x => x.DataId == _data.UnlockCondList[i].Value && x.Record > 0) == null) isLock = true;
                    break;
                case TableEnum.UnlockType.SCrownMapId:
                    if (listUserMap.Find(x => x.DataId == _data.UnlockCondList[i].Value && x.Record > 1) == null) isLock = true;
                    break;
                case TableEnum.UnlockType.GCrownMapId:
                    if (listUserMap.Find(x => x.DataId == _data.UnlockCondList[i].Value && x.Record > 2) == null) isLock = true;
                    break;
            }

        }

        if (!isLock) SaveMapRecord(map.Id, 0);
        return isLock;
    }

    public int GetMapRecord(int _id)
    {
        Data_UserMap map = listUserMap.Find(x => x.DataId == _id);
        return map == null ? 0 : map.Record;
    }

    public bool IsNewMap(int _id)
    {
        Data_UserMap map = listUserMap.Find(x => x.DataId == _id);
        return map == null ? false : map.IsNew;
    }

    public bool HasNewMap()
    {
        Data_UserMap map = listUserMap.Find(x => x.IsNew);
        return map == null ? false : true;
    }

    public void PlayMap(int _id)
    {
        Data_UserMap map = listUserMap.Find(x => x.DataId == _id);
        if (map != null)
        {
            map.SetNew();
            OnNewMap();
        }
    }

    public int GetMapNum(int _record)
    {
        List<Data_UserMap> list = listUserMap.FindAll(x => x.Record >= _record);
        return list == null ? 0 : list.Count;
    }

    public void SaveMapRecord(int _id, int _record)
    {
        Data_UserMap map = listUserMap.Find(x => x.DataId == _id);
        if (map == null)
        {
            listUserMap.Add(new Data_UserMap(_id, _record, true));
        }
        else if (_record > map.Record)
        {
            listUserMap.Remove(map);
            listUserMap.Add(new Data_UserMap(_id, _record, false));
        }

        lastPlayedMapId = _id;
        GameProcess.GetMissionManager().CompleteMission(_id, (TableEnum.MapClearGrade)_record);

        if(_record > 0) AddExp();

        OnChangeUserMapList();
        OnNewMap();
    }

    void AddExp()
    {
        UserData.AddExp();
        CurExp++;
        if(CurExp > CurMaxExp)
        {
            CurExp -= CurMaxExp;
            CurLevel++;
            UserData.AddExpReward(GameProcess.GetGameConfig().ExpRewardCoin);
        }
    }
#endregion // Map

#region // UserData
    public void AddCharacterSlot(bool _useItem)
    {
        try
        {
            int max = GameProcess.GetGameConfig().MaxChaSlotNum;
            int cost = _useItem ? 0 : GameProcess.GetGameConfig().CostAddChaSlot;

            if (UserData.ChaSlotNum > GameProcess.GetGameConfig().MaxChaSlotNum) throw new GameException(GameException.ErrorCode.OverMaxCharacter);
            else if (UserData.Coin < cost) throw new GameException(GameException.ErrorCode.NotEnoughCoin);

            UserData.AddCoin(-cost);
            Slot_Character slot = new Slot_Character();
            slot.ChangedEvent += OnChangeChaSlot;
            chaSlotList.Add(slot);
            UserData.AddChaSlot(1);
            OnChangeChaSlot();
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    public void AddItemSlot(bool _useItem)
    {
        try
        {
            int max = GameProcess.GetGameConfig().MaxItemSlotNum - GameProcess.GetGameConfig().AddedItemSlotNum;
            int num = GameProcess.GetGameConfig().AddedItemSlotNum;
            int cost = _useItem ? 0 : GameProcess.GetGameConfig().CostAddItemSlot;

            if (UserData.InventorySlotNum > max) throw new GameException(GameException.ErrorCode.OverMaxInventory);
            else if (UserData.Coin < cost) throw new GameException(GameException.ErrorCode.NotEnoughCoin);

            UserData.AddCoin(-cost);

            int count = itemSlotList.Count;
            for(int i=0; i < num; ++i)
            {
                Slot_Item slot = new Slot_Item(count + i);
                itemSlotList.Add(slot);
            }

            UserData.AddItemSlot(num);
            MoveItemFromTempList();

            OnChangeItemSlot();
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    public void ExchangeGold(TableData_GoodsList _data, Action _callback)
    {
        if (_data == null) throw new GameException(GameException.ErrorCode.InvalidParam);
        if (UserData.Cash < _data.Price) throw new GameException(GameException.ErrorCode.NotEnoughCash);
        int max = GameProcess.GetGameConfig().MaxGold;
        if (UserData.Gold + _data.Num > max) throw new GameException(GameException.ErrorCode.OverMaxGold);

        try
        {
            DBManager.CheckLogServer(delegate (string _tick)
            {
                if (cm.CheckData(_data))
                {
                    UserData.AddCash((-1) * _data.Price);
                    UserData.AddGold(_data.Num);

                    /* 로그 처리 */
                    string log = GetGoodsLog(LogType.USE, _data);
                    DBManager.SaveLog(log, null);
                }
                if (_callback != null) _callback();

            });
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    public void ExchangeCoin(TableData_GoodsList _data, Action _callback)
    {
        if (_data == null) throw new GameException(GameException.ErrorCode.InvalidParam);
        if (UserData.Cash < _data.Price) throw new GameException(GameException.ErrorCode.NotEnoughCash);
        int max = GameProcess.GetGameConfig().MaxCoin;
        if (UserData.Coin + _data.Num > max) throw new GameException(GameException.ErrorCode.OverMaxCoin);

        try
        {
            DBManager.CheckLogServer(delegate (string _tick)
            {
                if (cm.CheckData(_data))
                {
                    UserData.AddCash((-1) * _data.Price);
                    UserData.AddCoin(_data.Num);

                    /* 로그 처리 */
                    string log = GetGoodsLog(LogType.USE, _data);
                    DBManager.SaveLog(log, null);
                }
                if (_callback != null) _callback();
            });
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    public void Recharge(TableData_GoodsList _data)
    {
        if (_data == null) throw new GameException(GameException.ErrorCode.InvalidParam);
        if (UserData.Coin < _data.Price) throw new GameException(GameException.ErrorCode.NotEnoughCoin);
        if (UserData.Key + _data.Num > 10) throw new GameException(GameException.ErrorCode.OverMaxKey);
        UserData.AddCoin((-1) * _data.Price);
        UserData.AddKey(_data.Num);
    }

    public void BuyCash(TableData_GoodsList _data, Action _callback)
    {
        if (_data == null) throw new GameException(GameException.ErrorCode.InvalidParam);
        int max = GameProcess.GetGameConfig().MaxCash;
        if (UserData.Cash + _data.Num > max) throw new GameException(GameException.ErrorCode.OverMaxCash);
        
        try
        {
            DBManager.CheckLogServer(delegate (string _tick)
            {
                try
                {
                    GameProcess.BuyInappItem(UnityEngine.Purchasing.ProductType.Consumable, _data.ProductId, delegate ()
                    {
                        /* 구매 처리 */
                        if (_data.PurchaseLimit > 0) listIapHistory.Add(_data.ProductId);
                        UserData.AddPurcase(_data.Price_WON);
                        UserData.AddCash(_data.Num);
                        if (_callback != null) _callback();

                        /* 로그 처리 */
                        string log = GetGoodsLog(LogType.BUY, _data);
                        DBManager.SaveLog(log, null);
                    });
                }
                catch (Exception e)
                {
                    GameProcess.ShowError(new GameException(e));
                }
            });
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    public enum LogType
    {
        BUY,
        USE,
        COUPON,
    }
    private String GetGoodsLog(LogType _type, TableData_GoodsList _data)
    {
        Dictionary<string, object> hash = new Dictionary<string, object>();
        hash.Add("TYPE", _type.ToString());
        hash.Add("GUID", guid);
        hash.Add("USER_ID", userId);
        hash.Add("GOODS_ID", _data.Id);
        hash.Add("GOODS", _data.Name);
        if(_type == LogType.USE) hash.Add("PRICE", _data.Price);
        else hash.Add("PRICE", _data.Price_WON);
        return Json.Serialize(hash);
    }

    public String GetCouponLog(int _itemId, string _name, int _num)
    {
        Dictionary<string, object> hash = new Dictionary<string, object>();
        hash.Add("TYPE", LogType.COUPON);
        hash.Add("GUID", guid);
        hash.Add("USER_ID", userId);
        hash.Add("GOODS_ID", _itemId);
        hash.Add("GOODS", _name);
        hash.Add("PRICE", _num);
        return Json.Serialize(hash);
    }

    public void UseKey(int num)
    {
        if (UserData.Key < num) throw new GameException(GameException.ErrorCode.NotEnoughKey);
        UserData.AddKey(-num);
    }

    public int GetMembership()
    {
        int grade = 0;
        if (UserData != null)
        {
            int amount = UserData.PurchaseAmount;
            int[] array = GameProcess.GetGameConfig().VipGradeArray;
            for (int i = array.Length - 1; i > -1; --i)
            {
                if (amount < array[i]) continue;

                grade = Mathf.Min(4, i + 1);
                break;
            }
        }

        return grade;
    }

    public string GetUserInitial()
    {
        return UserData != null ? UserData.Initial : "AAA";
    }

    public void SetUserInitial(string _initial)
    {
        if (UserData == null) throw new GameException(GameException.ErrorCode.UserDataIsNull);
        UserData.EditInitial(_initial);
    }

    public int GetGUID()
    {
        return guid;
    }

    public int GetPurchaseCount(int _productId)
    {
        int count = 0;
        for(int i=0; i<listIapHistory.Count; ++i)
        {
            if (listIapHistory[i] == _productId) count++;
        }
        return count;
    }

    public int GetScore()
    {
        int score = 0;
        for(int i=0; i < listUserMap.Count; ++i)
        {
            switch(listUserMap[i].Record)
            {
                case 1: score += 1; break;
                case 2: score += 3; break;
                case 3: score += 5; break;
            }
        }
        return score;
    }

    string pwStrPool = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public string GetNewPw()
    {
        string newPw = "";
        int length = pwStrPool.Length;
        for (int i = 0; i < 8; ++i)
        {
            newPw += pwStrPool.Substring(UnityEngine.Random.Range(0, length),1);
        }

        return newPw;
    }

    public string GetToken()
    {
        int length = pwStrPool.Length;
        int first = UnityEngine.Random.Range(0, 9);
        int second = 8 - first;
        StringBuilder stb = new StringBuilder();
        stb.Append(pwStrPool.Substring(UnityEngine.Random.Range(0, length), 1));
        stb.Append(pwStrPool.Substring(UnityEngine.Random.Range(0, length), 1));
        stb.Append(pwStrPool.Substring(UnityEngine.Random.Range(0, length), 1));
        stb.Append(pwStrPool.Substring(UnityEngine.Random.Range(0, length), 1));
        stb.Append(first);
        stb.Append(pwStrPool.Substring(UnityEngine.Random.Range(0, length), 1));
        stb.Append(pwStrPool.Substring(UnityEngine.Random.Range(0, length), 1));
        stb.Append(pwStrPool.Substring(UnityEngine.Random.Range(0, length), 1));
        stb.Append(second);
        stb.Append(pwStrPool.Substring(UnityEngine.Random.Range(0, length), 1));
        stb.Append(pwStrPool.Substring(UnityEngine.Random.Range(0, length), 1));
        stb.Append(pwStrPool.Substring(UnityEngine.Random.Range(0, length), 1));

        return stb.ToString();
    }

    public void SendDataToServer(Action _callback)
    {
        try
        {
            ArrayList mapArray = new ArrayList();
            foreach (Data_UserMap map in listUserMap)
            {
                if(map != null) mapArray.Add(map.GetCVS());
            }
            
            string mapData = Json.Serialize(mapArray);

            DBManager.SendData(userId, mapData, delegate ()
            {
                if (_callback != null) _callback();
            });
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    public void LoadDataFromServer(Action _callback)
    {
        try
        {
            DBManager.LoadData(userId, delegate (string _mapData)
            {
                ArrayList listMap = new ArrayList();
                IList list = MiniJSON.Json.Deserialize(_mapData) as IList;
                foreach (int csv in list)
                {
                    if (csv > 0) listMap.Add(Data_UserMap.GetHashFromCSV(csv));
                }
                EncryptedPlayerPrefs.SetString(keyUserMap, Json.Serialize(listMap));

                if (_callback != null) _callback();
            });
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    public string GetEquipInfoCVSOfCurCha()
    {        
        ArrayList itemArray = new ArrayList();
        foreach(KeyValuePair<int, Slot_Item> item in CurCharacterSlot.Data.EquipSlot)
        {
            if (item.Value == null || item.Value.IsEmpty) continue;
            itemArray.Add(item.Value.Data.GetCVSForRank());
        }

        return Json.Serialize(itemArray);
    }

    public string GetChaInfoCVS()
    {
        ArrayList itemArray = new ArrayList();
        itemArray.Add(CurCharacterSlot.Data.SkinId);
        itemArray.Add(CurCharacterSlot.Data.Grade);

        return Json.Serialize(itemArray);
    }

    public string GetChaParamCVS()
    {
        ArrayList itemArray = new ArrayList();
        itemArray.Add(CurCharacterSlot.Data.SAtk);
        itemArray.Add(CurCharacterSlot.Data.LAtk);
        itemArray.Add(CurCharacterSlot.Data.ASpd);
        itemArray.Add(CurCharacterSlot.Data.Spd);
        itemArray.Add(CurCharacterSlot.Data.Def);
        itemArray.Add(CurCharacterSlot.Data.MaxHp);
        itemArray.Add(CurCharacterSlot.Data.Wbp);
        itemArray.Add(CurCharacterSlot.Data.Critical);

        return Json.Serialize(itemArray);
    }

    public int GetRewardedId()
    {
        return UserData.RewardedId;
    }

    public void GetExpReward()
    {
        int rest = GameProcess.GetGameConfig().MaxCoin - UserData.Coin;
        if (rest > 0)
        {
            UserData.UseExpReward(rest);
            Save();
        }
        else throw new GameException(GameException.ErrorCode.OverMaxCoin);
    }

    public void CheckReciept(string _id, string _reciept)
    {
        cm.CheckReciept(_id, _reciept);
    }
#endregion // UserData

#region // Mission
    public List<int> GetCompletedMissionList()
    {
        return new List<int>(listClearMission);
    }

    public void SetClearMission(int _id)
    {
        if (!listClearMission.Contains(_id)) listClearMission.Add(_id); 
    }

    public TableData_DailyReward GetDailyReward()
    {
        if(UserData.HasDailyReward())
        {
            /* 천공 격투장 입장권 보상 */
            TableData_Item _item2 = TableManager.GetGameData(36003) as TableData_Item;
            if(IsPrimium) AddItem(_item2, 20);
            else AddItem(_item2, 10);
#if UNITY_EDITOR
            Debug.Log("용자의 증표 보상 지급");
#endif

            /* 데일리 보상 리턴 */
            List<TableData_DailyReward> list = TableManager.GetDailyList();
            TableData_DailyReward reward = list.Find(x => x.Id == UserData.DailyCount);

            if (reward == null) Save();
            return reward;
        }

        return null;
    }

    public void SetLogInTime(string _timeStr)
    {
        if (_timeStr.Equals(string.Empty)) return;

        string[] t = _timeStr.Split('/');
        if(t.Length == 6)
        {
            int year = Convert.ToInt32(t[0]);
            int month = Convert.ToInt32(t[1]);
            int day = Convert.ToInt32(t[2]);
            int hour = Convert.ToInt32(t[3]);
            int minute = Convert.ToInt32(t[4]);
            int second = Convert.ToInt32(t[5]);
            logInTime = new DateTime(year, month, day, hour, minute, second);
        }
    }

    public DateTime GetLogInTime()
    {
        return logInTime;
    }
#endregion // Mission

#region // Event
    void OnChangeUserMapList()
    {
        if (ChangeUserMapListEvent != null) ChangeUserMapListEvent();
    }

    void OnChangeItemSlot()
    {
        if (AddedInventorySizeEvent != null) AddedInventorySizeEvent();
    }

    void OnChangeChaSlot()
    {
        if (ChangedChaSlotEvent != null) ChangedChaSlotEvent(this.UserData);
    }

    void OnChangeCurCharacter()
    {
        if (ChangedCurCharacterEvent != null) ChangedCurCharacterEvent();
    }

    void OnNewCharacter()
    {
        if (NewChaEvent != null) NewChaEvent();
    }

    void OnNewMap()
    {
        if (NewMapEvent != null) NewMapEvent();
    }

    void OnNewItem()
    {
        if (NewItemEvent != null) NewItemEvent();
    }
#endregion // Event
}
