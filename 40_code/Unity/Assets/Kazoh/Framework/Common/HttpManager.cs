﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class HttpManager : MonoBehaviour {

    public struct RequestStruct
    {
        public string Url;
        public WWWForm Form;
        public Action<WWW> Callback;
        public bool IsLoader;
        public float WaitTime;

        public RequestStruct(string url, Action<WWW> callback, float _waitTime, bool isLoader = false)
        {
            Url = url;
            Callback = callback;
            IsLoader = isLoader;
            Form = null;
            WaitTime = _waitTime;
        }

        public RequestStruct(string url, WWWForm form, Action<WWW> callback, float _waitTime, bool isLoader = false)
        {
            Url = url;
            Callback = callback;
            IsLoader = isLoader;
            Form = form;
            WaitTime = 30f;
        }
    }

    private Queue<RequestStruct> requests;
    private List<RequestStruct> waitList;

    public void Init()
    {
        requests = new Queue<RequestStruct>();
        waitList = new List<RequestStruct>();
    }

    public void CallRequest(string _url, bool _loader, Action<WWW> _callback)
    {
        RequestStruct request = new RequestStruct(_url, _callback, Time.time + 30f, _loader);
        requests.Enqueue(request);
        if (_loader) waitList.Add(request);
    }

    public void CallRequest(string _url, WWWForm _form, bool _loader, Action<WWW> _callback)
    {
        RequestStruct request = new RequestStruct(_url, _form, _callback, Time.time + 30f, _loader);
        requests.Enqueue(request);
        if (_loader) waitList.Add(request);
    }

    // Update is called once per frame
    void Update () {	
        if(requests != null && requests.Count > 0)
        {
            StartCoroutine(Request(requests.Dequeue()));
        }
        if(waitList.Count > 0)
        {
            RequestStruct request = waitList[0];
            waitList.RemoveAt(0);
            if (request.WaitTime < Time.time) GameProcess.ShowError(new GameException(GameException.ErrorCode.TimeOut));
            else waitList.Add(request);
        }
	}

    IEnumerator Request(RequestStruct _request)
    {
        if(_request.IsLoader) GameProcess.ShowServerLoading();

        WWW www = null;
        if (_request.Form == null) www = new WWW(_request.Url);
        else www = new WWW(_request.Url, _request.Form);
        yield return www;

        if (waitList.Contains(_request)) waitList.Remove(_request);

#if DEBUG_MODE
        Debug.Log(_request.Url);
        Debug.Log(www.text);
#endif
        if (_request.IsLoader) GameProcess.HideServerLoading();

        if (www.error == null)
        {
            if (_request.Callback != null) _request.Callback(www);
        }
        else
        {
            Debug.LogError(www.error);
            GameProcess.ShowError(new GameException(GameException.ErrorCode.UnknownServerError));
        }
    }
}
