﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ArenaManager {

    public Data_Arena CurData { get { return arenaList == null || arenaList.Count == 0 ? null : arenaList[idx]; } }
    public int Count { get { return arenaList == null ? 0 : arenaList.Count; } }
    public int Score { get; private set; }
    private List<Data_Arena> arenaList;
    private List<Data_Arena> rankList;
    //private List<Data_Arena> curList;
    private int idx;
    private Data_Arena myData;

    public ArenaManager()
    {
        arenaList = new List<Data_Arena>();
        rankList = new List<Data_Arena>();
        //curList = new List<Data_Arena>();
        idx = 0;
        Score = -1;
    }

    public void Roll(Action _callback)
    {
        try
        {
            int userId = GameProcess.GetGameDataManager().GetGUID();
            string name = GameProcess.GetGameDataManager().GetUserInitial();
            string chaInfo = GameProcess.GetGameDataManager().GetChaInfoCVS();
            string param = GameProcess.GetGameDataManager().GetChaParamCVS();
            string equip = GameProcess.GetGameDataManager().GetEquipInfoCVSOfCurCha();
            DBManager.RollArena(userId, name, chaInfo, param, equip, delegate ()
            {
                Sync(_callback);
                //if (_callback != null) _callback();
            });
        }
        catch (GameException e)
        {
            GameProcess.ShowError(e);
        }
        catch (Exception e)
        {
            GameProcess.ShowError(new GameException(e));
        }
    }

    public void Sync(Action _callback)
    {
        int count = 0;
        SyncRank(delegate ()
        {
            count++;
            if (count == 2 && _callback != null) _callback();
        });
        SyncData(delegate ()
        {
            count++;
            if (count == 2 && _callback != null) _callback();
        });
    }

    public void SyncRank(Action _callback)
    {
        try
        {
            DBManager.GetArenaRank(delegate (string _data)
            {
                rankList = Parse(_data);
                if (_callback != null) _callback();
            });
        }
        catch (GameException e)
        {
            GameProcess.ShowError(e);
        }
        catch (Exception e)
        {
            GameProcess.ShowError(new GameException(e));
        }
    }

    public void SyncData(Action _callback)
    {
        try
        {
            int userId = GameProcess.GetGameDataManager().GetGUID();
            DBManager.GetArenaData(userId, delegate (string _data)
            {
                arenaList = Parse(_data);
                idx = 0;
                if(arenaList != null)
                {
                    myData = arenaList.Find(x => x.Id == userId);
                    if (myData != null)
                    {
                        Score = myData.Record;
                        arenaList.Remove(myData);
                    }
                    arenaList.Sort((x, y) => (x.Ability.SAtk + x.Ability.LAtk).CompareTo(y.Ability.SAtk + y.Ability.LAtk));
                }
                if (_callback != null) _callback();
            });
        }
        catch (GameException e)
        {
            GameProcess.ShowError(e);
        }
        catch (Exception e)
        {
            GameProcess.ShowError(new GameException(e));
        }
    }

    public Data_Arena Next()
    {
        if (arenaList == null || Count == 0) return null;
        idx++;
        if (idx == Count) idx = 0;
        return arenaList[idx];
    }

    public Data_Arena Prev()
    {
        if (arenaList == null || Count == 0) return null;
        idx--;
        if (idx < 0) idx += Count;
        return arenaList[idx];
    }

    public List<Data_Arena> GetRankList()
    {
        List<Data_Arena> list = new List<Data_Arena>();
        list.AddRange(rankList.ToArray());
        return list;
    }

    public void Win(Action _callback)
    {
        int userId = GameProcess.GetGameDataManager().GetGUID();
        string name = GameProcess.GetGameDataManager().GetUserInitial();
        string chaInfo = GameProcess.GetGameDataManager().GetChaInfoCVS();
        string param = GameProcess.GetGameDataManager().GetChaParamCVS();
        string equip = GameProcess.GetGameDataManager().GetEquipInfoCVSOfCurCha();
        DBManager.SendWinArena(userId, name, chaInfo, param, equip, delegate (int _score)
        {
            Score = _score;
            arenaList.RemoveAt(idx);
            if (idx >= Count) idx = 0;
            if (_callback != null) _callback();
        });
    }

    public void Lose(Action _callback)
    {
        int userId = GameProcess.GetGameDataManager().GetGUID();
        string name = GameProcess.GetGameDataManager().GetUserInitial();
        string chaInfo = GameProcess.GetGameDataManager().GetChaInfoCVS();
        string param = GameProcess.GetGameDataManager().GetChaParamCVS();
        string equip = GameProcess.GetGameDataManager().GetEquipInfoCVSOfCurCha();
        DBManager.SendLoseArena(userId, name, chaInfo, param, equip, delegate (int _score)
        {
            Score = _score;
            //curList.Clear();
            if (_callback != null) _callback();
        });
    }

    private List<Data_Arena> Parse(string _data)
    {
        if (string.IsNullOrEmpty(_data)) return null;

        IList data = MiniJSON.Json.Deserialize(System.Convert.ToString(_data)) as IList;
        if (data == null) throw new GameException(GameException.ErrorCode.InvalidParam);

        List<Data_Arena> list = new List<Data_Arena>();
        foreach(IDictionary dict in data)
        {
            try
            {
                Data_Arena item = new Data_Arena(dict);
                list.Add(item);
                //Debug.Log(string.Format("PvP Rank:{1} ID:{0} Score:{2}", item.Id, item.Rank, item.Record));
            }
            catch
            {
                Debug.LogWarning("PvP 데이터가 형식에 맞지 않습니다.");
            }
        }

        return list;
    }
}
