﻿#if UNITY_ANDROID
// You must obfuscate your secrets using Window > Unity IAP > Receipt Validation Obfuscator
// before receipt validation will compile in this sample.
#define RECEIPT_VALIDATION
#endif

using UnityEngine;
using UnityEngine.Purchasing;
using System;
using System.Collections;
using System.Collections.Generic;
#if RECEIPT_VALIDATION
using UnityEngine.Purchasing.Security;
#endif

public class IAPManager : MonoBehaviour, IStoreListener
{
    private static IStoreController m_StoreController; 
    private static IExtensionProvider m_StoreExtensionProvider;
#if UNITY_IPhone && PRIMIUM
    public static string[] kProductIDConsumable = {"eafp_item_01", "eafp_item_02", "eafp_item_03", "eafp_item_04", "eafp_item_05", "eafp_item_06"};
#else
    public static string[] kProductIDConsumable = { "eaf_item_01", "eaf_item_02", "eaf_item_03", "eaf_item_04", "eaf_item_05", "eaf_item_06" };
#endif
    public static string[] kProductIDNonConsumable = {"nonconsumable_item_01"};
    public static string[] kProductIDSubscription = {"subscription_item_01"};

    private static string kProductNameAppleSubscription = "com.unity3d.subscription.new";
    private static string kProductNameGooglePlaySubscription = "com.unity3d.subscription.original";

#if RECEIPT_VALIDATION
    private CrossPlatformValidator validator;
#endif
    private Action callback;

    public void Init()
    {
        if (m_StoreController == null)
        {
            InitializePurchasing();
        }
    }

    public void InitializePurchasing()
    {
        if (IsInitialized()) return;

        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        foreach (string _id in kProductIDConsumable)
        {
            builder.AddProduct(_id, ProductType.Consumable, new IDs()
            {
                { _id, AppleAppStore.Name },
                { _id, GooglePlay.Name },
            });
        }

        //foreach (string _id in kProductIDNonConsumable)
        //{
        //    builder.AddProduct(_id, ProductType.NonConsumable);
        //}

        //foreach (string _id in kProductIDSubscription)
        //{
        //    builder.AddProduct(_id, ProductType.Subscription, new IDs(){
        //            { kProductNameAppleSubscription, AppleAppStore.Name },
        //            { kProductNameGooglePlaySubscription, GooglePlay.Name },
        //        });
        //}
                
#if RECEIPT_VALIDATION
        validator = new CrossPlatformValidator(GooglePlayTangle.Data(), AppleTangle.Data(), Application.bundleIdentifier);
#endif

        UnityPurchasing.Initialize(this, builder);
    }

    private bool IsInitialized()
    {
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    public void BuyConsumable(int _idx, Action _callback = null)
    {
        try
        {
            if (_idx <= 0 || _idx > kProductIDConsumable.Length) throw new GameException(GameException.ErrorCode.InvalidParam);
            _idx--;
            callback = _callback;
            BuyProductID(kProductIDConsumable[_idx]);
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    public void BuyNonConsumable(int _idx, Action _callback = null)
    {
        try
        {
            if (_idx <= 0 || _idx > kProductIDNonConsumable.Length) throw new GameException(GameException.ErrorCode.InvalidParam);
            callback = _callback;
            BuyProductID(kProductIDNonConsumable[_idx]);
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    public void BuySubscription(int _idx, Action _callback = null)
    {
        try
        {
            if (_idx <= 0 || _idx > kProductIDSubscription.Length) throw new GameException(GameException.ErrorCode.InvalidParam);
            callback = _callback;
            BuyProductID(kProductIDSubscription[_idx]);
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    void BuyProductID(string productId)
    {
        if (IsInitialized())
        {
            Product product = m_StoreController.products.WithID(productId);

            if (product != null && product.availableToPurchase)
            {
#if UNITY_EDITOR
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
#endif
                GameProcess.ShowServerLoading();

                m_StoreController.InitiatePurchase(product);
                StartCoroutine(HideLoader());
            }
            else
            {
                Debug.LogError("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                throw new GameException(GameException.ErrorCode.NotPurchasingProduct);
            }
        }
        else
        {
            Debug.LogError("BuyProductID FAIL. Not initialized.");
            throw new GameException(GameException.ErrorCode.NotInitializedPurchaser);
        }
    }

    IEnumerator HideLoader()
    {
        yield return new WaitForSeconds(10f);
        GameProcess.HideServerLoading();
    }
    
    public void RestorePurchases()
    {
        if (!IsInitialized())
        {
            Debug.LogError("RestorePurchases FAIL. Not initialized.");
            throw new GameException(GameException.ErrorCode.FailRestorePurchases);
        }

        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
#if UNITY_EDITOR
            Debug.Log("RestorePurchases started ...");
#endif

            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            apple.RestoreTransactions((result) => {
#if UNITY_EDITOR
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
#endif
            });
        }
        else
        {
            Debug.LogError("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
            throw new GameException(GameException.ErrorCode.FailRestorePurchases);
        }
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
#if UNITY_EDITOR || DEBUG_MODE
        Debug.Log("OnInitialized: PASS");
#endif

        m_StoreController = controller;
        m_StoreExtensionProvider = extensions;
    }

    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.LogError("OnInitializeFailed InitializationFailureReason:" + error);
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        GameProcess.HideServerLoading();

#if RECEIPT_VALIDATION
        //if (Application.platform == RuntimePlatform.Android ||
        //    Application.platform == RuntimePlatform.IPhonePlayer)
        if (Application.platform == RuntimePlatform.Android)
        {
            try
            {
                var result = validator.Validate(args.purchasedProduct.receipt);
                foreach (IPurchaseReceipt productReceipt in result)
                {
#if UNITY_EDITOR || DEBUG_MODE
                    Debug.Log(productReceipt.productID);
                    Debug.Log(productReceipt.purchaseDate);
                    Debug.Log(productReceipt.transactionID);
#endif
                    GooglePlayReceipt google = productReceipt as GooglePlayReceipt;
                    if (null != google)
                    {
#if UNITY_EDITOR || DEBUG_MODE
                        Debug.Log(google.purchaseState);
                        Debug.Log(google.purchaseToken);
#endif
                    }
                    //AppleInAppPurchaseReceipt apple = productReceipt as AppleInAppPurchaseReceipt;
                    //if (null != apple)
                    //{
                    //    Debug.Log(apple.originalTransactionIdentifier);
                    //    Debug.Log(apple.subscriptionExpirationDate);
                    //    Debug.Log(apple.cancellationDate);
                    //    Debug.Log(apple.quantity);
                    //}
                }
            }
            catch (IAPSecurityException)
            {
#if UNITY_EDITOR || DEBUG_MODE
                Debug.Log("Invalid receipt, not unlocking content");
#endif
                GameProcess.GetGameDataManager().CheckReciept(args.purchasedProduct.definition.id, args.purchasedProduct.receipt);
                return PurchaseProcessingResult.Complete;
            }
        }
#endif
        for (int i=0; i<kProductIDConsumable.Length; ++i)
        {
            if (string.Equals(args.purchasedProduct.definition.id, kProductIDConsumable[i], StringComparison.Ordinal))
            {
#if UNITY_EDITOR
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
#endif
                if (callback != null) callback();
                return PurchaseProcessingResult.Complete;
            }
        }

        for (int i = 0; i < kProductIDNonConsumable.Length; ++i)
        {
            if (string.Equals(args.purchasedProduct.definition.id, kProductIDNonConsumable[i], StringComparison.Ordinal))
            {
#if UNITY_EDITOR
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
#endif
                if (callback != null) callback();
                return PurchaseProcessingResult.Complete;
            }
        }

        for (int i = 0; i < kProductIDNonConsumable.Length; ++i)
        {
            if (string.Equals(args.purchasedProduct.definition.id, kProductIDSubscription[i], StringComparison.Ordinal))
            {
#if UNITY_EDITOR
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
#endif
                if (callback != null) callback();
                return PurchaseProcessingResult.Complete;
            }
        }

        Debug.LogError(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));

        return PurchaseProcessingResult.Complete;
    }


    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        GameProcess.HideServerLoading();

        Debug.LogError(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));        
    }
}
